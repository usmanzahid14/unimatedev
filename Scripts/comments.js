$(document).ready(function(){
	updateCommentsTable();
})

function addComment(){
	// extracting values from the form
	var userName = $('#Username').val();
	var comment = $('#Comment').val();
 
	//json conversion
	var commentNode = {
					'userName': userName,
					'comment' : comment,
	}

	var Ref = firebase.database().ref('Comments').push().key;
	firebase.database().ref('Comments/'+ Ref).update(commentNode);
  var uniName = $('#UniversityName').val('');
  var userName = $('#Username').val('');
  var comment = $('#Comment').val('');
}

function updateCommentsTable()
{
  var firebaseRef=firebase.database().ref().child('Comments');
 
  firebaseRef.orderByKey().on("value", snap => {
    //Counter
      var i=1;
      //Destory Existing table
      $('#commentTable').DataTable().clear().destroy();
      //Get Fresh copy of comments data
      snap.forEach(function(snapshot) {
     
       var key=snapshot.key;
       var tempComment=snapshot.child("comment").val();
       var tempUser=snapshot.child("userName").val();
       var tempKey=snapshot.child("key").val();
       
       //Action buttons
       var deleteButton='<a title="Delete comment" onclick="deleteComment(\'' + key + '\');" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
       
       //append data into comment table
       $('#commentsTableBody').append('<tr>'+
        '<td>'+i+'</td>'+
        '<td>'+tempUser+'</td>'+
        '<td>'+tempKey+'</td>'+
        '<td>'+tempComment+'</td>'+
        '<td>'+deleteButton+'</td>'+
        '</tr>');
       i++;
      })
      //convert comment table into datatable
      $('#commentTable').DataTable();
    });
}

function deleteComment(Key){
  firebase.database().ref('/Comments/' + Key).remove();
}