$(document).ready(function(){
getUniversitiesList(); 
getProgramsList();
updateExchangeProgramTable();
})

function getUniversitiesList(){

	//getting data to display in dropdown
	var firebaseRef=firebase.database().ref().child("Universities");
	firebaseRef.orderByKey().on("value", snap => {
		snap.forEach(function(snapshot) {	 

 		var Key=snapshot.key;
  		var uniName=snapshot.child("name").val();
  		$('#UniversityName').append('<option value='+Key+'>'+uniName+'</option>');
 		})
	});
}

function getProgramsList(){

	//getting data to display in dropdown menu
	var firebaseRef=firebase.database().ref().child("Program");
	firebaseRef.orderByKey().on("value", snap => {
		snap.forEach(function(snapshot) {	 

 		var tempKey=snapshot.key;
  		var proName=snapshot.child("programName").val();
  		$('#ProgramName').append('<option value='+tempKey+'>'+proName+'</option>');
 		})
	});
}

function addExchangePrograms(){

	//getting data from the form
	var uniID=$('#UniversityName').val();
	var uniName= $('#UniversityName option:selected').text();
	var proID=$('#ProgramName').val();
	var proName= $('#ProgramName option:selected').text();
	var duration=$('#Duration').val();
	var charges=$('#Charges').val();
	var discription=$('#Discription').val();
 

	//convert into json
	var exchangeProgramsNode = {
					'universityID': uniID,
					'universityName': uniName,
					'programID': proID,
					'programName': proName,
                    'charges': charges,
                    'duration': duration,
                    'discription': discription,
                 };

 	var keyRef = firebase.database().ref('Exchangeprograms').push().key;
	firebase.database().ref('Exchangeprograms/'+keyRef).update(exchangeProgramsNode);
	var uniID=$('#UniversityName').val('');
	var uniName= $('#UniversityName option:selected').text('');
	var proID=$('#ProgramName').val('');
	var proName= $('#ProgramName option:selected').text('');
	var duration=$('#Duration').val('');
	var charges=$('#Charges').val('');
	var discription=$('#Discription').val('');
}

function updateExchangeProgramTable()
{
	var firebaseRef=firebase.database().ref().child('Exchangeprograms');
 
 	firebaseRef.orderByKey().on("value", snap => {
 		//Counter
	  	var i=1;
	  	//Destory Existing table
	    $('#exchangeProgramTable').DataTable().clear().destroy();
	    //Get Fresh copy of exchange programs data
	  	snap.forEach(function(snapshot) {
	   
		   var key=snapshot.key;
		   var tempCharges=snapshot.child("charges").val();
		   var tempDis=snapshot.child("discription").val();
		   var tempDuration=snapshot.child("duration").val();
		   var tempPro=snapshot.child("programName").val();
		   var tempUni=snapshot.child("universityName").val();
		   
		   //Action buttons
		   var deleteButton='<a title="Delete Exchange programs" onclick="deleteExchangeProgram(\'' + key + '\');" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
		   var editButton='<a title="Edit Exchange programs" onclick="editExchangeProgram(\'' + key + '\');" data-toggle="modal" data-target="#EditExchangeProgramModal" class="m-datatable__toggle-subtable fa fa-edit" style="width: 20px; color:powderblue;" href="#"></a>';
		   
		   //append data into exchange program table
		   $('#exchangeProgramTableBody').append('<tr>'+
		    '<td>'+i+'</td>'+
		    '<td>'+tempUni+'</td>'+
		    '<td>'+tempPro+'</td>'+
		    '<td>'+tempDuration+'</td>'+
		    '<td>'+tempCharges+'</td>'+
		    '<td>'+tempDis+'</td>'+
		    '<td>'+editButton+'</td>'+
		    '<td>'+deleteButton+'</td>'+
		    '</tr>');
		   i++;
	  	})
	  	//convert Exchange program table into datatable
	    $('#exchangeProgramTable').DataTable();
    });
}

function editExchangeProgram(Key){
	firebase.database().ref('/Exchangeprograms/' + Key).once('value').then(function(snapshot){
	    var tempEditCharges=snapshot.val().charges;
	    var tempEditDis=snapshot.val().discription;
	    var tempEditDuration=snapshot.val().duration;
	    var tempEditPro=snapshot.val().programName;
	    var tempEditUni=snapshot.val().universityName;
	    
		$('#exKey').val(Key);
		$('#eUniversityName').val(tempEditUni);
		$('#eProgramName').val(tempEditPro);
		$('#eDuration').val(tempEditDuration);
		$('#eCharges').val(tempEditCharges);
		$('#eDiscription').val(tempEditDis)
	});
}

function saveExchangePrograms(){
	var editKey=$('#exKey').val();
	var editUniName=$('#eUniversityName').val();
	var editProName=$('#eProgramName').val();
	var editDuration=$('#eDuration').val();
	var editCharges=$('#eCharges').val(); 
	var editDis=$('#eDiscription').val();  
 

	//convert into json
	var editNode = {
	               'charges': editCharges,
	               'discription': editDis,
	               'duration': editDuration,
	               'programName': editProName,
	               'universityName': editUniName
	               }; 
	firebase.database().ref('Exchangeprograms/'+editKey).update(editNode);
}

function deleteExchangeProgram(Key){
	
	firebase.database().ref('/Exchangeprograms/'+ Key).remove();
}