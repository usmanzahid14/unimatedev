$(document).ready(function(){
  updateWorkshopsTable()
})

function addworkshops(downloadURL){ 
  //getting data from input form
  var title=$('#Title').val();
  var date=$('#Date').val();
  var startTime=$('#StartTime').val();
  var endTime=$('#EndTime').val();
  var discription=$('#Discription').val();
  var venue=$('#Venue').val();
  var fee=$('#Fee').val();
  var status=$('#Status').val();

  //convert into json
  var workshopNode = {
                  'title': title,
                  'date': date,
                  'startTime': startTime,
                  'endTime': endTime,
                  'discription': discription,     
                  'venue': venue,
                  'fee': fee,
                  'status': status,
                  'workshopImageUrl': downloadURL,
                 };

  var keyRef = firebase.database().ref('Workshops').push().key;
  firebase.database().ref('Workshops/'+keyRef).update(workshopNode);
  var title=$('#Title').val('');
  var date=$('#Date').val('');
  var startTime=$('#StartTime').val('');
  var endTime=$('#EndTime').val('');
  var discription=$('#Discription').val('');
  var venue=$('#Venue').val('');
  var fee=$('#Fee').val('');
  var status=$('#Status').val('');
} 

function updateWorkshopsTable()
{
  var firebaseRef=firebase.database().ref().child('Workshops');
 
  firebaseRef.orderByKey().on("value", snap => {
    //Counter
      var i=1;
      //Destory Existing table
      $('#workshopsTable').DataTable().clear().destroy();
      //Get Fresh copy of workshop data
      snap.forEach(function(snapshot) {
     
       var key=snapshot.key;
       var tempDate=snapshot.child("date").val();
       var tempDis=snapshot.child("discription").val();
       var tempEnd=snapshot.child("endTime").val();
       var tempStart=snapshot.child("startTime").val();
       var tempFee=snapshot.child("fee").val();
       var tempStatus=snapshot.child("status").val();
       var tempTitle=snapshot.child("title").val();
       var tempVenue=snapshot.child("venue").val();
        var image=snapshot.child("workshopImageUrl").val();
       //Action buttons
       var deleteButton='<a title="Delete Workshop" onclick="deleteWorkshop(\''+key+'\');" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
       var editButton='<a title="Edit Workshop" onclick="editWorkshop(\''+key+'\');" data-toggle="modal" data-target="#EditWorkshops" class="m-datatable__toggle-subtable fa fa-edit" style="width: 20px; color:powderblue;" href="#"></a>';
       
       //append data into workshops table
       $('#workshopsTableBody').append('<tr>'+
        '<td>'+i+'</td>'+
        '<td>'+tempTitle+'</td>'+
        '<td>'+tempDate+'</td>'+
        '<td>'+tempStart+'</td>'+
        '<td>'+tempEnd+'</td>'+
        '<td>'+tempDis+'</td>'+
        '<td>'+tempVenue+'</td>'+
        '<td></td>'+
        '<td>'+tempFee+'</td>'+
        '<td>'+tempStatus+'</td>'+
        '<td>'+"<img src=\""+image+" \"width=50px >"+'</td>'+
        '<td>'+editButton+'</td>'+
        '<td>'+deleteButton+'</td>'+
        '</tr>');
       i++;
      })
      //convert workshop table into datatable
      $('#workshopsTable').DataTable();
    });
}

function editWorkshop(Key){
  firebase.database().ref('/Workshops/' + Key).once('value').then(function(snapshot){
      var tempEditDate=snapshot.val().date;
      var tempEditDis=snapshot.val().discription;
      var tempEditEnd=snapshot.val().endTime;
      var tempEditFee=snapshot.val().fee;
      var tempEditStart=snapshot.val().startTime;
      var tempEditStatus=snapshot.val().status;
      var tempEditTitle=snapshot.val().title;
      var tempEditVenue=snapshot.val().venue;
      
    $('#eKey').val(Key);
    $('#eTitle').val(tempEditTitle);
    $('#eDate').val(tempEditDate);
    $('#eStartTime').val(tempEditStart);
    $('#eEndTime').val(tempEditEnd);
    $('#eDiscription').val(tempEditDis)
    $('#eVenue').val(tempEditVenue)
    $('#eFee').val(tempEditFee)
    $('#eStatus').val(tempEditStatus)
  });
}

function saveChangesWorkshops(){
  //getting data from input form
  var editKey=$('#eKey').val();
  var title=$('#eTitle').val();
  var date=$('#eDate').val();
  var startTime=$('#eStartTime').val();
  var endTime=$('#eEndTime').val();
  var discription=$('#eDiscription').val();
  var venue=$('#eVenue').val();
  var fee=$('#eFee').val();
  var status=$('#eStatus').val();

  //convert into json
  var workshopNode = {
                  'title': title,
                  'date': date,
                  'startTime': startTime,
                  'endTime': endTime,
                  'discription': discription,     
                  'venue': venue,
                  'fee': fee,
                  'status': status,
                 }; 
  firebase.database().ref('Workshops/'+editKey).update(workshopNode);
}

function deleteWorkshop(Key){
  
  firebase.database().ref('/Workshops/'+ Key).remove();
}


$(document).on('change', '#fileToUpload', function(event) {
  selectedFile = event.target.files[0];
});

var uploadImage_addWorkshop= function() {
  var filename=selectedFile.name;
  var storageRef = firebase.storage().ref();
  var uploadTask = storageRef.child('/WorkshopImages/'+filename).put(selectedFile);

  // Register three observers:
  // 1. 'state_changed' observer, called any time the state changes
  // 2. Error observer, called on failure
  // 3. Completion observer, called on successful completion
  uploadTask.on('state_changed', function(snapshot){
    // Observe state change events such as progress, pause, and resume
    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    console.log('Upload is ' + progress + '% done');
    switch (snapshot.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log('Upload is running');
        break;
    }
  }, function(error) {
    // Handle unsuccessful uploads
  }, function() {
    // Handle successful uploads on complete
    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
    uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
      addworkshops(downloadURL)
    });
  });
}