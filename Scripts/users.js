$(document).ready(function(){
	updateUsersTable();
})
 

function addblogs(downloadURL){
	//Get values from input fields
	var name=$('#Name').val();
	var email=$('#Email').val();
	var contact=$('#Contact').val();
	var pass=$('#Password').val();    
 

	//convert into json
	var userNode = {
	               'username': name,
	               'email': email,
	               'contact': contact,
	               'password': pass,
	               }; 
	var keyRef = firebase.database().ref('Users').push().key;
	firebase.database().ref('Users/'+keyRef).update(blogNode);
	
	var name=$('#Name').val('');
	var email=$('#Email').val('');
	var contact=$('#Contact').val('');
	var pass=$('#Password').val(''); 
}

function updateUsersTable()
{
	var firebaseRef=firebase.database().ref().child('Users');
 
 	firebaseRef.orderByKey().on("value", snap => {
 		//Counter
	  	var i=1;
	  	//Destory Existing table
	    $('#usersTable').DataTable().clear().destroy();
	    //Get Fresh copy of blog data
	  	snap.forEach(function(snapshot) {
	   
		   var key=snapshot.key;
		   var tempUser=snapshot.child("username").val();
		   var tempContact=snapshot.child("contact").val();
		   var tempPass=snapshot.child("password").val();
		   var tempEmail=snapshot.child("email").val();

		   //Action buttons
		   var deleteButton='<a title="Delete Blog" onclick="deleteBlog(\'' + key + '\');" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
		   
		   //append data into blog table
		   $('#usersTableBody').append('<tr>'+
		    '<td>'+i+'</td>'+
		    '<td>'+tempUser+'</td>'+
		    '<td>'+tempEmail+'</td>'+
		    '<td>'+tempContact+'</td>'+
		    '<td>'+tempPass+'</td>'+
	        '<td>'+deleteButton+'</td>'+
		    '</tr>');
		   i++;
	  	})
	  	//convert blog table into datatable
	    $('#usersTable').DataTable();
    });
}

function deleteBlog(Key){
	
	firebase.database().ref('/Blog/'+ Key).remove();
}