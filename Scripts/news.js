$(document).ready(function(){
  updateNewsTable()
})

function addNews(downloadURL){ 
  //getting data from input form
  var title=$('#Title').val();
  var date=$('#Lastdate').val();
  var discription=$('#Discription').val();
  var postTime=$('#postingTime').val();
  var status=$('#Status').val();

  //convert into json
  var newsNode = {
                  'title': title,
                  'lastdate': date,
                  'discription': discription,     
                  'postingTime': postTime,
                  'status': status,
                  'newsImageUrl': downloadURL,
                 };

  var keyRef = firebase.database().ref('News').push().key;
  firebase.database().ref('News/'+keyRef).update(newsNode);
  var title=$('#Title').val('');
  var date=$('#Lastdate').val('');
  var discription=$('#Discription').val('');
  var postTime=$('#postingTime').val('');
  var status=$('#Status').val('');
} 

function updateNewsTable()
{
  var firebaseRef=firebase.database().ref().child('News');
 
  firebaseRef.orderByKey().on("value", snap => {
    //Counter
      var i=1;
      //Destory Existing table
      $('#newsTable').DataTable().clear().destroy();
      //Get Fresh copy of workshop data
      snap.forEach(function(snapshot) {
     
       var key=snapshot.key;
       var tempDate=snapshot.child("lastdate").val();
       var tempDis=snapshot.child("discription").val();
       var tempPostingDate=snapshot.child("postingTime").val();
       var tempStatus=snapshot.child("status").val();
       var tempTitle=snapshot.child("title").val();
       var image=snapshot.child("newsImageUrl").val();
       //Action buttons
       var deleteButton='<a title="Delete News" onclick="deleteNews(\''+key+'\');" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
       var editButton='<a title="Edit News" onclick="editNews(\''+key+'\');" data-toggle="modal" data-target="#EditNews" class="m-datatable__toggle-subtable fa fa-edit" style="width: 20px; color:powderblue;" href="#"></a>';
       
       //append data into workshops table
       $('#newsTableBody').append('<tr>'+
        '<td>'+i+'</td>'+
        '<td>'+tempTitle+'</td>'+
        '<td>'+tempDate+'</td>'+
        '<td>'+tempDis+'</td>'+
        '<td>'+tempPostingDate+'</td>'+
        '<td>'+tempStatus+'</td>'+
        '<td>'+"<img src=\""+image+" \"width=50px >"+'</td>'+
        '<td>'+editButton+'</td>'+
        '<td>'+deleteButton+'</td>'+
        '</tr>');
       i++;
      })
      //convert workshop table into datatable
      $('#newsTable').DataTable();
    });
}

function editNews(Key){
  firebase.database().ref('/News/' + Key).once('value').then(function(snapshot){
      var tempEditDate=snapshot.val().lastdate;
      var tempEditDis=snapshot.val().discription;
      var tempEditPostTime=snapshot.val().postingTime;
      var tempEditStatus=snapshot.val().status;
      var tempEditTitle=snapshot.val().title;
      var tempEditImage=snapshot.val().newsImageUrl;
      
    $('#eKey').val(Key);
    $('#eTitle').val(tempEditTitle);
    $('#eLastdate').val(tempEditDate);
    $('#eDiscription').val(tempEditDis)
    $('#epostingNews').val(tempEditPostTime)
    $('#eStatus').val(tempEditStatus)
  });
}

function saveChangesNews(){
  //getting data from input form
  var editKey=$('#eKey').val();
  var title=$('#eTitle').val();
  var date=$('#eLastdate').val();
  var discription=$('#eDiscription').val();
  var postTime=$('#epostingNews').val();
  var status=$('#eStatus').val();

  //convert into json
  var newsNode = {
                  'title': title,
                  'lastdate': date,
                  'discription': discription,     
                  'postingTime': postTime,
                  'status': status,
                 }; 
  firebase.database().ref('News/'+editKey).update(newsNode);
}

function deleteNews(Key){
  
  firebase.database().ref('/News/'+ Key).remove();
}


$(document).on('change', '#fileToUpload', function(event) {
  selectedFile = event.target.files[0];
});

var uploadImage_addnews= function() {
  var filename=selectedFile.name;
  var storageRef = firebase.storage().ref();
  var uploadTask = storageRef.child('/NewsImages/'+filename).put(selectedFile);

  // Register three observers:
  // 1. 'state_changed' observer, called any time the state changes
  // 2. Error observer, called on failure
  // 3. Completion observer, called on successful completion
  uploadTask.on('state_changed', function(snapshot){
    // Observe state change events such as progress, pause, and resume
    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    console.log('Upload is ' + progress + '% done');
    switch (snapshot.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log('Upload is running');
        break;
    }
  }, function(error) {
    // Handle unsuccessful uploads
  }, function() {
    // Handle successful uploads on complete
    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
    uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
      addNews(downloadURL)
    });
  });
}