$(document).ready(function(){
	updateRatingTable();
	getUniversities();
})

function getUniversities(){

	$('#UniversityName').html('');
	//getting data from DB to display in drop down menu
	var firebaseRef=firebase.database().ref().child("Universities");
	firebaseRef.orderByKey().on("value", snap => {
	
		snap.forEach(function(snapshot) {		   
		   var Key=snapshot.key;
		   var uniName=snapshot.child("name").val();
		   $('#UniversityName').append('<option value='+Key+'>'+uniName+'</option>');
	  	})
	});
}

function addRating(){
	//Get values from input fields
	var uId= $('#UniversityName option:selected').val();
	var uniName= $('#UniversityName option:selected').text();
	var rating=$('#Rating').val();

	//convert into json
	var ratingNode = {
                    'universityName': uniName,
                    'UID': uId,
                    'rating': rating,
                 }
  	var keyRef = firebase.database().ref('Review').push().key;
	firebase.database().ref('Review/'+keyRef).update(ratingNode);
	var rating=$('#Rating').val('');
}

function updateRatingTable()
{
	var firebaseRef=firebase.database().ref().child('Review');
 
 	firebaseRef.orderByKey().on("value", snap => {
 		//Counter
	  	var i=1;
	  	//Destory Existing table
	    $('#reviewsTable').DataTable().clear().destroy();
	    //Get Fresh copy of reviews data
	  	snap.forEach(function(snapshot) {
	   
		   var key=snapshot.key;
		   var tempRate=snapshot.child("rating").val();
		   var tempUni=snapshot.child("universityName").val();
		   var tempUniID=snapshot.child("UID").val();
		   
		   //Action buttons
		   var deleteButton='<a title="Delete Review" onclick="deleteReview(\'' + key + '\')" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
		
		   //append data into ratings table
		   $('#reviewsTableBody').append('<tr>'+
		    '<td>'+i+'</td>'+
		    '<td>'+tempUni+'</td>'+
		    '<td>'+tempRate+'</td>'+
		    '<td>'+deleteButton+'</td>'+
		    '</tr>'
		    );
		   i++;
	  	})
	  	//convert ratings table into datatable
	    $('#reviewsTable').DataTable();
    });
}

function deleteReview(Key){
	firebase.database().ref('/Review/' + Key).remove();
}