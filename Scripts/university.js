var selectedFile;

$(document).ready(function(){
updateUniversityTable()
})
function addUniversity(downloadURL)
{
//Get values from input fields
var uniName=$('#UniversityName').val();
var email=$('#Email').val();
var phone=$('#Phone').val();
var rank=$('#Rank').val();
var sector=$('#Sector').val();
var discrption=$('#Discrption').val();
var trainingInstitute=$('#TrainingInstitute').val();	
var location=$('#Location').val();
var matricPercentage=$('#MatricPercentage').val();
var interPercentage=$('#InterPercentage').val();
var entryTestPercentage=$('#EntryTestPercentage').val();
//convert into json 
var universityNode = {
                    'discription': discrption,
                    'email': email,
                    'location': location,
                    'name': uniName,
                    'rank': rank,
                    'phone': phone,
                    'sector':sector,
                    'isEntryTestInstitution': trainingInstitute,
                    'universityImageUrl': downloadURL,
                    'matricPercentage':matricPercentage,
                    'interPercentage':interPercentage,
                    'entryTestPercentage':entryTestPercentage,

                 }

  var keyRef = firebase.database().ref('Universities').push().key;
firebase.database().ref('Universities/'+keyRef).update(universityNode);	

var uniName=$('#UniversityName').val('');
var email=$('#Email').val('');
var phone=$('#Phone').val('');
var rank=$('#Rank').val('');
var sector=$('#Sector').val('');
var discrption=$('#Discrption').val('');
var trainingInstitute=$('#TrainingInstitute').val('');	
var location=$('#Location').val('');
var matricPercentage=$('#MatricPercentage').val('');
var interPercentage=$('#InterPercentage').val('');
var entryTestPercentage=$('#EntryTestPercentage').val('');

}
function updateUniversityTable()
{
var firebaseRef=firebase.database().ref().child('Universities');
 
 	firebaseRef.orderByKey().on("value", snap => {
 	//Counter
 	var i=1;
 	//Destory Existing table
   $('#universityTable').DataTable().clear().destroy();
   //Get Fresh copy of universities data
 	snap.forEach(function(snapshot) {
  
  var key=snapshot.key;
  var des=snapshot.child("discription").val();
  var email=snapshot.child("email").val();
  var rank=snapshot.child("rank").val();
  var name=snapshot.child("name").val();
  var phone=snapshot.child("phone").val();
  var location=snapshot.child("location").val();
  var sector=snapshot.child("sector").val();
  var image=snapshot.child("universityImageUrl").val();
  var isEntryTestInstitution=snapshot.child("isEntryTestInstitution").val();
  var matricPercentage=snapshot.child("matricPercentage").val();
  var interPercentage=snapshot.child("interPercentage").val();
  var entryTestPercentage=snapshot.child("entryTestPercentage").val();
  //Action buttons
  var deleteButton='<a title="Delete university" onclick="deleteUniversity(\'' + key + '\');" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
  var editButton='<a title="Edit university" onclick="editUniversity(\'' + key + '\');" data-toggle="modal" data-target="#EditUniversity" class="m-datatable__toggle-subtable fa fa-edit" style="width: 20px; color:powderblue;" href="#"></a>';
  
  //append data into university table
  $('#tableBody').append('<tr>'+
   '<td>'+i+'</td>'+
   '<td>'+name+'</td>'+
   '<td>'+email+'</td>'+
   '<td>'+phone+'</td>'+
   '<td>'+rank+'</td>'+
   '<td>'+location+'</td>'+
   '<td>'+sector+'</td>'+
   '<td>'+des+'</td>'+
   '<td>'+isEntryTestInstitution+'</td>'+
   '<td></td>'+
   '<td>'+matricPercentage+'</td>'+'<td></td>'+
   '<td>'+interPercentage+'</td>'+'<td></td>'+
   '<td>'+entryTestPercentage+'</td>'+'<td></td>'+
   '<td>'+"<img src=\""+image+" \"width=50px >"+'</td>'+
   '<td>'+editButton+'</td>'+
   '<td>'+deleteButton+'</td>'+
   '</tr>');
  i++;
 	})
 	//covert university table into datatable
   $('#universityTable').DataTable();
    });
}

function editUniversity(Key){
firebase.database().ref('/Universities/' + Key).once('value').then(function(snapshot){
   var tempEditDis=snapshot.val().discription;
   var tempEditEmail=snapshot.val().email;
   var tempEditInstitue=snapshot.val().isEntryTestInstitution;
   var tempEditLocation=snapshot.val().location;
   var tempEditUni=snapshot.val().name;
   var tempEditPhone=snapshot.val().phone;
   var tempEditRank=snapshot.val().rank;
   var tempEditSector=snapshot.val().sector;
   var tempMatricPercentage=snapshot.val().matricPercentage;
   var tempInterPercentage=snapshot.val().interPercentage;
   var tempEntryTestPercentage=snapshot.val().entryTestPercentage;
    
    $('#eKey').val(Key);
    $('#eUniversityName').val(tempEditUni);
    $('#eEmail').val(tempEditEmail);
    $('#ePhone').val(tempEditPhone);
    $('#eRank').val(tempEditRank);
    $('#eSector').val(tempEditSector);
    $('#eDiscrption').val(tempEditDis);
    $('#eTrainingInstitute').val(tempEditInstitue);
    $('#eLocation').val(tempEditLocation);
    $('#eMatricPercentage').val(tempMatricPercentage);
    $('#eInterPercentage').val(tempInterPercentage);
    $('#eEntryTestPercentage').val(tempEntryTestPercentage);
});
}

function saveChangesUniversity(){
//Get values from input fields
  var tempKey=$('#eKey').val();
  var uniName=$('#eUniversityName').val();
  var email=$('#eEmail').val();
  var phone=$('#ePhone').val();
  var rank=$('#eRank').val();
  var sector=$('#eSector').val();
  var discrption=$('#eDiscrption').val();
  var trainingInstitute=$('#eTrainingInstitute').val();	
  var location=$('#eLocation').val();
  var matricPercentage=$('#eMatricPercentage').val();
  var interPercentage=$('#eInterPercentage').val();
  var entryTestPercentage=$('#eEntryTestPercentage').val();

  //convert into json 
  var universityNode = {
                      'discription': discrption,
                      'email': email,
                      'location': location,
                      'name': uniName,
                      'rank': rank,
                      'phone': phone,
                      'sector':sector,
                      'isEntryTestInstitution': trainingInstitute,
                      'matricPercentage':matricPercentage,
                      'interPercentage':interPercentage,
                      'entryTestPercentage':entryTestPercentage,

                   }
  firebase.database().ref('Universities/'+tempKey).update(universityNode);
}

function deleteUniversity(Key){

firebase.database().ref('/Universities/'+ Key).remove();
}


$(document).on('change', '#fileToUpload', function(event) {
    selectedFile = event.target.files[0];
  });

  var uploadImage_addUni= function() {
  var filename=selectedFile.name;
  var storageRef = firebase.storage().ref();
  var uploadTask = storageRef.child('/UniversityImages/'+filename).put(selectedFile);

  uploadTask.on('state_changed', function(snapshot){
   
   var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
   console.log('Upload is ' + progress + '% done');
   switch (snapshot.state) {
     case firebase.storage.TaskState.PAUSED: // or 'paused'
       console.log('Upload is paused');
       break;
     case firebase.storage.TaskState.RUNNING: // or 'running'
       console.log('Upload is running');
       break;
   }
  }, function(error) {
   // Handle unsuccessful uploads
  }, function() {
   // Handle successful uploads on complete
   // For instance, get the download URL: https://firebasestorage.googleapis.com/...
   uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
     addUniversity(downloadURL)
   });
  });
}