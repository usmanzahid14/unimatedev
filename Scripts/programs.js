$(document).ready(function(){
	getUniversitiesList();
	updateProgramTable();
})  

function getUniversitiesList(){

	$('#UniversityName').html('');
	//getting data from DB to display in drop down menu
	var firebaseRef=firebase.database().ref().child("Universities");
	firebaseRef.orderByKey().on("value", snap => {
	
		snap.forEach(function(snapshot) {		   
		   var Key=snapshot.key;
		   var uniName=snapshot.child("name").val();
		   $('#UniversityName').append('<option value='+Key+'>'+uniName+'</option>');
	  	})
	});
}

function addProgram(){
	//Get values from input fields
	var uId= $('#UniversityName option:selected').val();
	var uniName= $('#UniversityName option:selected').text();
	var programName= $('#ProgramName').val();
	var programCatogery= $('#catogery').val();
	var duration=$('#Duration').val();
	var fee=$('#Fee').val();
	var feeDuration=$('#FeeDuration').val();
	var merit=$('#Merit').val();
	var lastYear=$('#LastYearMerit').val();
	var scholarship=$('#Scholarship').val();
	//convert into json
	var programNode = {
                    'programName': programName,
                    'Catogery': programCatogery,
                    'UID' : uId,
                    'uName': uniName,
                    'duration': duration,
                    'fee': fee,
                    'feeDuration': feeDuration,
                    'openMerit': merit,
                    'lastYearMerit': lastYear,
                    'scholarship':scholarship,
                 }
  	var keyRef = firebase.database().ref('Program').push().key;
	firebase.database().ref('Program/'+keyRef).update(programNode);
	
	var programName= $('#ProgramName').val('');
	var duration=$('#Duration').val('');
	var fee=$('#Fee').val('');
	var lastYear=$('#LastYearMerit').val('');
	
}

function updateProgramTable()
{
	var firebaseRef=firebase.database().ref().child('Program');
 
 	firebaseRef.orderByKey().on("value", snap => {
 		//Counter
	  	var i=1;
	  	//Destory Existing table
	    $('#programTable').DataTable().clear().destroy();
	    //Get Fresh copy of program data
	  	snap.forEach(function(snapshot) {
	   
		   var key=snapshot.key;
		   var tempProgramName=snapshot.child("programName").val();
		   var tempUniversityName=snapshot.child("uName").val();
		   var tempDuration=snapshot.child("duration").val();
		   var tempFee=snapshot.child("fee").val();
		   var tempFeeDuration=snapshot.child("feeDuration").val();
		   var tempOpenMerit=snapshot.child("openMerit").val();
		   var tempLastYear=snapshot.child("lastYearMerit").val();
		   var tempScholarship=snapshot.child("scholarship").val();
		   var tempCatogery=snapshot.child("Catogery").val();
		   
		   //Action buttons
		   var deleteButton='<a title="Delete Program" onclick="deleteProgram(\'' + key + '\')" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
		   var editButton='<a title="Edit Program" onclick="editProgram(\'' + key + '\')" data-toggle="modal" data-target="#EditPrograms" class="m-datatable__toggle-subtable fa fa-edit" style="width: 20px; color:powderblue;" href="#"></a>';
		   
		   //append data into program table
		   $('#programTableBody').append('<tr>'+
		    '<td>'+i+'</td>'+
		    '<td>'+tempUniversityName+'</td>'+
		    '<td>'+tempProgramName+'</td>'+
		    '<td>'+tempCatogery+'</td>'+
		    '<td>'+tempDuration+'</td>'+
		    '<td>'+tempFee+'</td>'+
		    '<td>'+tempFeeDuration+'</td>'+
		    '<td>'+tempOpenMerit+'</td>'+
		    '<td>'+tempScholarship+'</td>'+
		    '<td>'+tempLastYear+'</td>'+
		    '<td>'+editButton+'</td>'+
		    '<td>'+deleteButton+'</td>'+
		    '</tr>');
		   i++;
	  	})
	  	//convert program table into datatable
	    $('#programTable').DataTable();
    });
}

function editProgram(Key){
	firebase.database().ref('/Program/' + Key).once('value').then(function(snapshot){
	    var tempEditLastMerit=snapshot.val().lastYearMerit;
	    var tempEditFee=snapshot.val().fee;
	    var tempEditDuration=snapshot.val().duration;
	    var tempEditFeeDuration=snapshot.val().feeDuration;
	    var tempEditMerit=snapshot.val().openMerit;
	    var tempEditPro=snapshot.val().programName;
	    var tempEditScholarship=snapshot.val().scholarship;
	    var tempEditUni=snapshot.val().uName;
	    var tempEditCat=snapshot.val().Catogery;
	    
		$('#exKey').val(Key);
		$('#eUniName').val(tempEditUni);
		$('#eProgramName').val(tempEditPro);
		$('#eDuration').val(tempEditDuration);
		$('#eFee').val(tempEditFee);
		$('#eFeeDuration').val(tempEditFeeDuration)
		$('#eMerit').val(tempEditMerit)
		$('#eScholarship').val(tempEditScholarship)
		$('#eLastYearMerit').val(tempEditLastMerit)
		$('#eCatogery').val(tempEditCat)
	});
}

function saveProgramChanges(){
	var editKey=$('#exKey').val();
	var editUniName=$('#eUniName').val();
	var editProName=$('#eProgramName').val();
	var editDuration=$('#eDuration').val();
	var editFee=$('#eFee').val(); 
	var editFeeDuration=$('#eFeeDuration').val();  
	var editMerit=$('#eMerit').val();  
	var editScholarship=$('#eScholarship').val();  
	var editLast=$('#eLastYearMerit').val();
	var editCat=$('#eCatogery').val();
 
	//convert into json
	var editNode = {
	                'programName': editProName,
                    'Catogery': editCat,
                    'UID' : editKey,
                    'uName': editUniName,
                    'duration': editDuration,
                    'fee': editFee,
                    'feeDuration': editFeeDuration,
                    'openMerit': editMerit,
                    'lastYearMerit': editLast,
                    'scholarship':editScholarship,                    
	               }; 
	firebase.database().ref('Program/'+editKey).update(editNode);
}

function deleteProgram(Key){
	
	firebase.database().ref('/Program/'+ Key).remove();
}