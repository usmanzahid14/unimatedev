$(document).ready(function(){
	updateScholarshipsTable();
  getUniversitiesList();
})

function getUniversitiesList(){

  $('#UniversityName').html('');
  //getting data from DB to display in drop down menu
  var firebaseRef=firebase.database().ref().child("Universities");
  firebaseRef.orderByKey().on("value", snap => {
  
    snap.forEach(function(snapshot) {      
       var Key=snapshot.key;
       var uniName=snapshot.child("name").val();
       $('#UniversityName').append('<option value='+Key+'>'+uniName+'</option>');
      })
  });
}

function addScholarships(){
	//Get values from input fields
	var uId= $('#UniversityName option:selected').val();
  var uniName= $('#UniversityName option:selected').text();
	var type= $('#Type').val();
	var discription= $('#Discription').val();


	//convert into json
	var scholarshipsNode = {
					            'UID' : uId,
                      'uName': uniName,
                      'type':type, 
                      'discription' :discription, 
                 }
    var keyRef = firebase.database().ref('Scholarships').push().key;
	firebase.database().ref('Scholarships/'+keyRef).update(scholarshipsNode);

  var discription= $('#Discription').val('');
}

function updateScholarshipsTable()
{
  var firebaseRef=firebase.database().ref().child('Scholarships');
 
  firebaseRef.orderByKey().on("value", snap => {
    //Counter
      var i=1;
      //Destory Existing table
      $('#scholarshipsTable').DataTable().clear().destroy();
      //Get Fresh copy of scholarships data
      snap.forEach(function(snapshot) {
     
         var key=snapshot.key;
         var tempSponser=snapshot.child("uName").val();
         var tempDis=snapshot.child("discription").val();
         var tempType=snapshot.child("type").val();
         
         //Action buttons
         var deleteButton='<a title="Delete Scholarship" onclick="deleteScholarship(\''+key+'\');" class="m-datatable__toggle-subtable fa fa-trash" style="width: 20px; color:powderblue;" href="#"></a>';
         var editButton='<a title="Edit scholarship" onclick="editScholarship(\''+key+'\');" data-toggle="modal" data-target="#EditScholarships" class="m-datatable__toggle-subtable fa fa-edit" style="width: 20px; color:powderblue;" href="#"></a>';
         
         //append data into scholarship table
         $('#scholarshipsTableBody').append('<tr>'+
          '<td>'+i+'</td>'+
          '<td>'+tempSponser+'</td>'+
          '<td>'+tempType+'</td>'+
          '<td>'+tempDis+'</td>'+
          '<td>'+editButton+'</td>'+
          '<td>'+deleteButton+'</td>'+
          '</tr>');
         i++;
        })
        //convert scholarship table into datatable
        $('#scholarshipsTable').DataTable();
    });
}
function editScholarship(Key){
  firebase.database().ref('/Scholarships/' + Key).once('value').then(function(snapshot){
      var tempEditDis=snapshot.val().discription;
      var tempEditUni=snapshot.val().uName;
      var tempEditType=snapshot.val().type;
      
    $('#exKey').val(Key);
    $('#eUniName').val(tempEditUni);
    $('#eType').val(tempEditType);
    $('#eDiscription').val(tempEditDis);
  });
}

function saveChangesScholarship(){
  var editKey=$('#eKey').val();
  var editSponserName=$('#eUniName').val();
  var editType=$('#eType').val();
  var editDis=$('#eDiscription').val(); 

  //convert into json
  var editNode = {
                  'UID' : editKey,
                  'uName': editSponserName,
                  'type':editType, 
                  'discription' :editDis,
                 }; 
  firebase.database().ref('/Scholarships/'+editKey).update(editNode);
}

function deleteScholarship(Key){
  
  firebase.database().ref('/Scholarships/'+ Key).remove();
}