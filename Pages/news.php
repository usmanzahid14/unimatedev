<!DOCTYPE html> 
<html lang="en"> 
		<!-- Include Header Here-->
		<?php include '../Includes/header.php';?>
		<!--End-->
	<body class="">
		<div class="wrapper ">
			<!-- Include SideBar Here-->
			<?php include '../Includes/sideBar.php';?>
			<!--End-->
			
			<div class="main-panel" style="background-image: linear-gradient(#2b7a78, #def2f1);">
				<!-- Include navigation top header here-->
				<?php include '../Includes/topheader.php';?>
				<div class="content">
			        <div class="container-fluid">
			    		<!-- Add Data Here-->
			    		 <div class="row">
				            <div class="col-md-12">
				              <div class="card">
				                <div class="card-header card-header-info card-header-icon">
				                  <div class="card-icon">
				                    <i class="material-icons" style="font-size: 30px">event</i>
				                  </div>
				                  <h4 class="card-title">List of News</h4>
				                </div>
				                <div class="card-body">
				                  <div class="toolbar">
				                    <div class="text-right">
										<button class="btn btn-info" type="button"  data-toggle="modal" data-target="#AddEditNews">Add new</button>
									</div>
				                  </div>
				                  <div class="material-datatables">
				                  	<div class="table-responsive">
					                  	<table width="100%" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"  role="grid" aria-describedby="datatables_info" style="width: 100%;" cellspacing="0" id="newsTable">
					                      	<thead>
					                      		<tr class="bg-info text-white">
					                      			<th>Sr No.</th>
							        				<th>Title</th>
							        				<th>Last Date</th>     				
							        				<th>Discription</th>
							        				<th>Posting Date</th>
							        				<th>Status</th>
							        				<th>Images</th>
							        				<th>Edit</th>
							        				<th>Delete</th>
							      				</tr>
					                      	</thead>
					                      	<tbody id="newsTableBody">
					                        </tbody>
					                    </table>	
				                  	</div>
				                  </div>
				                </div>
				                <!-- end content-->
				              </div>
				              <!--  end card  -->
				            </div>
				            <!-- end col-md-12 -->
				          </div>
		      		<!-- end row -->
			    	</div>
     			</div>
			<?php include '../Includes/footer.php';?>	
			</div>
			<!--  Add New Modal-->
				<div class="modal fade" id="AddEditNews" tabindex="-1" role="">
				    <div class="modal-dialog AddEditNews" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Add News</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Title"><b>Title</b></label>	
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Title" id="Title" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Date"><b>Date</b></label> 	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Date" id="Lastdate" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Discription"><b>Discription</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Discription" id="Discription" class="form-control" required><br>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Venue"><b>Posting Time</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Venue" id="postingTime" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Status"><b>Status</b></label>
											    	</div>
											  		<div class="col-md-10">
										    			<select class="form-control" id="Status">
										    				<option value="True">True</option>
										    				<option value="False">False</option>
										    			</select>
											  		</div>
											  	</div>

											  	</div>
                                                   <div class="row">
											  		 <div class="col-md-2">
											  		 	<label for="Status"><b>Image</b></label>
											    	</div>
											    	<div class="col-md-10">
										    			<form action="upload.php" method="post" enctype="multipart/form-data">
												       <input class="form-control" type="file" name="fileToUpload" id="fileToUpload"></form>
											  		</div>
                                                    </div>
											  	<div class="row">
											  		<div class="col-md-5">
											    	</div>
											  		<div class="col-md-7">
											  			<button class="btn btn-info" onclick="uploadImage_addnews();" data-dismiss="modal">Save</button>
										    			<button class="btn btn-info" data-dismiss="modal">Cancel</button>
											  		</div>
											  	</div>
											</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->

				<!--  Edit Modal-->
				<div class="modal fade" id="EditNews" tabindex="-1" role="">
				    <div class="modal-dialog EditNews" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Edit News</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
											  	<div class="row">
											  		<input type="hidden" id="eKey">
											  		<div class="col-md-2">
												    	<label for="Title"><b>Title</b></label>	
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Title" id="eTitle" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Date"><b>Date</b></label> 	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Date" id="eLastdate" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Discription"><b>Discription</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Discription" id="eDiscription" class="form-control" required><br>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Venue"><b>Posting date</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Posting Date" id="epostingNews" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Status"><b>Status</b></label>
											    	</div>
											  		<div class="col-md-10">
										    			<select class="form-control" id="eStatus">
										    				<option value="True">True</option>
										    				<option value="False">False</option>
										    			</select>
											  		</div>
											  	</div>
											  	</div>
                                                   <div class="row">
											  		 <div class="col-md-2">
											  		 	<label for="Status"><b>Image</b></label>
											    	</div>
											    	<div class="col-md-10">
										    			<form action="upload.php" method="post" enctype="multipart/form-data">
												       <input class="form-control" type="file" name="fileToUpload" id="fileToUpload"></form>
											  		</div>
                                                    </div>
											  	<div class="row">
											  		<div class="col-md-5">
											    	</div>
											  		<div class="col-md-7">
											  			<button class="btn btn-info" onclick="saveChangesNews();" data-dismiss="modal">Save</button>
										    			<button class="btn btn-info" data-dismiss="modal">Cancel</button>
											  		</div>
											  	</div>
											</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->
		</div>
		<script src="../Scripts/news.js"></script>
	</body>
</html>
