<!DOCTYPE html> 
<html lang="en"> 
		<!-- Include Header Here-->
		<?php include '../Includes/header.php';?>
		<!--End-->
	<body class="">
		<div class="wrapper ">
			<!-- Include SideBar Here-->
			<?php include '../Includes/sideBar.php';?>
			<!--End-->
			
			<div class="main-panel" style="background-image: linear-gradient(#2b7a78, #def2f1);">
				<!-- Include navigation top header here-->
				<?php include '../Includes/topheader.php';?>
				<div class="content">
			        <div class="container-fluid">
			    		<!-- Add Data Here-->
			    		 <div class="row">
				            <div class="col-md-12">
				              <div class="card">
				                <div class="card-header card-header-info card-header-icon">
				                  <div class="card-icon">
				                    <i class="material-icons" style="font-size: 30px">event</i>
				                  </div>
				                  <h4 class="card-title">List of Workshops/Events</h4>
				                </div>
				                <div class="card-body">
				                  <div class="toolbar">
				                    <div class="text-right">
										<button class="btn btn-info" type="button"  data-toggle="modal" data-target="#AddEditWorkshops">Add new</button>
									</div>
				                  </div>
				                  <div class="material-datatables">
				                  	<div class="table-responsive">
					                  	<table width="100%" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"  role="grid" aria-describedby="datatables_info" style="width: 100%;" cellspacing="0" id="workshopsTable">
					                      	<thead>
					                      		<tr class="bg-info text-white">
					                      			<th>Sr No.</th>
							        				<th>Title</th>
							        				<th>Date</th>
							        				<th>Start Time</th>
							        				<th>End Time</th>     				
							        				<th>Discription</th>
							        				<th>Venue<th>
							        				<th>Fee</th>
							        				<th>Status</th>
							        				<th>Images</th>
							        				<th>Edit</th>
							        				<th>Delete</th>
							      				</tr>
					                      	</thead>
					                      	<tbody id="workshopsTableBody">
					                        </tbody>
					                    </table>	
				                  	</div>
				                  </div>
				                </div>
				                <!-- end content-->
				              </div>
				              <!--  end card  -->
				            </div>
				            <!-- end col-md-12 -->
				          </div>
		      		<!-- end row -->
			    	</div>
     			</div>
			<?php include '../Includes/footer.php';?>	
			</div>
			<!--  Add New Modal-->
				<div class="modal fade" id="AddEditWorkshops" tabindex="-1" role="">
				    <div class="modal-dialog AddEditWorkshops" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Add Workshops/Events</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Title"><b>Title</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Title" id="Title" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Date"><b>Date</b></label> 	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Date" id="Date" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Start Time"><b>Start Time</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Start Time" id="StartTime" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="End Time"><b>End Time</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter End Time" id="EndTime" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Discription"><b>Discription</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Discription" id="Discription" class="form-control" required><br>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Venue"><b>Venue</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Venue" id="Venue" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Fee"><b>Workshop Fee</b></label>
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Workshop Fee" id="Fee" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Status"><b>Status</b></label>
											    	</div>
											  		<div class="col-md-10">
										    			<select class="form-control" id="Status">
										    				<option value="True">True</option>
										    				<option value="False">False</option>
										    			</select>
											  		</div>
											  	</div>

											  	</div>
                                                   <div class="row">
											  		 <div class="col-md-2">
											  		 	<label for="Status"><b>Image</b></label>
											    	</div>
											    	<div class="col-md-10">
										    			<form action="upload.php" method="post" enctype="multipart/form-data">
												       <input class="form-control" type="file" name="fileToUpload" id="fileToUpload"></form>
											  		</div>
                                                    </div>
											  	<div class="row">
											  		<div class="col-md-5">
											    	</div>
											  		<div class="col-md-7">
											  			<button class="btn btn-info" onclick="uploadImage_addWorkshop();" data-dismiss="modal">Save</button>
										    			<button class="btn btn-info" data-dismiss="modal">Cancel</button>
											  		</div>
											  	</div>
											</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->

				<!--  Edit Modal-->
				<div class="modal fade" id="EditWorkshops" tabindex="-1" role="">
				    <div class="modal-dialog EditWorkshops" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Edit Workshops/Events</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
											  	<div class="row">
											  		<input type="hidden" id="eKey">
											  		<div class="col-md-2">
												    	<label for="Title"><b>Title</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eTitle" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Date"><b>Date</b></label> 	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eDate" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Start Time"><b>Start Time</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eStartTime" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="End Time"><b>End Time</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eEndTime" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Discription"><b>Discription</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eDiscription" class="form-control" required><br>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Venue"><b>Venue</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eVenue" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Fee"><b>Workshop Fee</b></label>
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eFee" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Status"><b>Status</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<select class="form-control" id="eStatus">
										    				<option value="True">True</option>
										    				<option value="False">False</option>
										    			</select><br>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-4">
											    	</div>
											  		<div class="col-md-8">
											  			<button class="btn btn-info" onclick="saveChangesWorkshops();" data-dismiss="modal">Save Changes</button>
										    			<button class="btn btn-info" data-dismiss="modal">Discard</button>
											  		</div>
											  	</div>
											</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->
		</div>
		<script src="../Scripts/workshop.js"></script>
	</body>
</html>
