<!DOCTYPE html>
<html lang="en">
		<!-- Include Header Here-->
		<?php include '../Includes/header.php';?>
		<!--End-->
	<body class="">
		<div class="wrapper ">
			<!-- Include SideBar Here-->
			<?php include '../Includes/sideBar.php';?>
			<!--End-->
			
			<div class="main-panel" style="background-image: linear-gradient(#2b7a78, #def2f1);">
				<!-- Include navigation top header here-->
				<?php include '../Includes/topheader.php';?>
				<div class="content">
			        <div class="container-fluid">
			    		<!-- Add Data Here-->
			    		 <div class="row">
				            <div class="col-md-12">
				              <div class="card">
				                <div class="card-header card-header-info card-header-icon">
				                  <div class="card-icon">
				                    <i class="material-icons" style="font-size: 30px">explicit</i>
				                  </div>
				                  <h4 class="card-title">List of Exchange Programs</h4>
				                </div>
				                <div class="card-body">
				                  <div class="toolbar">
				                    <div class="text-right">
										<button class="btn btn-info" type="button"  data-toggle="modal" data-target="#AddEditExchangeProgram">Add new</button>
									</div>
				                  </div>
				                  <div class="material-datatables">
				                  	<div class="table-responsive">
					                  	<table width="100%" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"  role="grid" aria-describedby="datatables_info" style="width: 100%;" cellspacing="0" id="exchangeProgramTable">
					                      	<thead>
					                      		<tr class="bg-info text-white">
					                      			<th>Sr No.</th>
							        				<th>University Name </th>
							        				<th>Program Name</th>
							        				<th>Duration</th>
							        				<th>Charges</th>
							        				<th>Discription</th>
							        				<th>Edit</th>
							        				<th>Delete</th>  				
							      				</tr>
					                      	</thead>
					                      	<tbody id="exchangeProgramTableBody">
					                        </tbody>
					                    </table>	
				                  	</div>
				                  </div>
				                </div>
				                <!-- end content-->
				              </div>
				              <!--  end card  -->
				            </div>
				            <!-- end col-md-12 -->
				          </div>
		      		<!-- end row -->
			    	</div>
     			</div>
			<?php include '../Includes/footer.php';?>	
			</div>
			<!--  Add New Modal-->
				<div class="modal fade" id="AddEditExchangeProgram" tabindex="-1" role="">
				    <div class="modal-dialog AddEditExchangeProgram" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Add Exchange Programs</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
										  		<div class="row">
											    	<div class="col-md-2">
												    	<label for="UniversityName"><b>University Name</b></label>	   
											    	</div>
												    <div class="col-md-10">
												      	<select name="UniversityName" class="form-control" id="UniversityName">
					                					</select>
												    </div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="ProgramName"><b>Program Name</b></label>  		
											    	</div>
											  		<div class="col-md-10">
										    			<select name="ProgramName" class="form-control" id="ProgramName">
					                					</select>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Duration"><b>Duration</b></label> 	   		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Duration" id="Duration" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Charges"><b>Charges</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Charges" id="Charges" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="Discription"><b>Discription</b></label>	   		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" placeholder="Enter Discription" id="Discription" class="form-control" required><br>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-5">
											    	</div>
											  		<div class="col-md-7">
											  			<button class="btn btn-info" onclick="addExchangePrograms();" data-dismiss="modal">Save</button>
										    			<button class="btn btn-info" data-dismiss="modal">Cancel</button>
											  		</div>
											  	</div>
											</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->

				<!--  Edit Modal-->
				<div class="modal fade" id="EditExchangeProgramModal" tabindex="-1" role="">
				    <div class="modal-dialog EditExchangeProgramModal" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Edit Exchange Programs</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
										  		<div class="row">
										  			<input type="hidden" id="exKey">
											    	<div class="col-md-2">
												    	<label for="eUniversityName"><b>University Name</b></label>	   
											    	</div>
												    <div class="col-md-10">
												      	<input type="text" id="eUniversityName" class="form-control" required readonly>
												    </div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="eProgramName"><b>Program Name</b></label>  		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eProgramName" class="form-control" required readonly>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="eDuration"><b>Duration</b></label> 	   		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text"  id="eDuration" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="eCharges"><b>Charges</b></label>	    		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" p id="eCharges" class="form-control" required>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-2">
												    	<label for="eDiscription"><b>Discription</b></label>	   		
											    	</div>
											  		<div class="col-md-10">
										    			<input type="text" id="eDiscription" class="form-control" required><br>
											  		</div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-4">
											    	</div>
											  		<div class="col-md-8">
											  			<button class="btn btn-info" onclick="saveExchangePrograms();" data-dismiss="modal">Save Changes</button>
										    			<button class="btn btn-info" data-dismiss="modal">Discard</button>
											  		</div>
											  	</div>
											</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->
		</div>
		<script src="../Scripts/exchangeprograms.js"></script>
	</body>
</html>
