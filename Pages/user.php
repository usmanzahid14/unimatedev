<!DOCTYPE html>
<html lang="en"> 
    <!-- Include Header Here-->
    <?php include '../Includes/header.php';?>
    <!--End-->
  <body class="">
    <div class="wrapper ">
      <!-- Include SideBar Here-->
      <?php include '../Includes/sideBar.php';?>
      <!--End-->
      
      <div class="main-panel" style="background-image: linear-gradient(#2b7a78, #def2f1);">
        <!-- Include navigation top header here-->
        <?php include '../Includes/topheader.php';?>
        <div class="content">
              <div class="container-fluid">
              <!-- Add Data Here-->             
                  <div class="row">
                        <div class="col-md-12">
                          <div class="card">
                        <div class="card-header card-header-info card-header-icon">
                          <div class="card-icon">
                            <i class="fa fa-user" style="font-size: 30px"></i>
                          </div>
                          <h4 class="card-title">List of Users</h4>
                        </div>
                        <div class="card-body">
                          <div class="toolbar">
                            <div class="text-right" >
                                  <button class="btn btn-info" type="button"  data-toggle="modal" data-target="#AddEditUsers">Add new</button>
                            </div>
                          </div>
                          <div class="material-datatables">
                            <div class="table-responsive">
                              <table width="100%" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"  role="grid" aria-describedby="datatables_info" style="width: 100%;" cellspacing="0" id="usersTable">
                                        <thead>
                                                <tr class="bg-info text-white">
                                                  <th>Sr No.</th>
                                                  <th>Username</th>
                                                  <th>E-mail</th>
                                                  <th>Contact</th>
                                                  <th>Password</th>
                                                  <th>Delete</th>
                                                </tr>
                                                <tbody id="usersTableBody"></tbody>
                                        </thead>
                              </table>  
                            </div>
                          </div>
                        </div>
                        <!-- end content-->
                          </div>
                          <!--  end card  -->
                        </div>
                        <!-- end col-md-12 -->
                  </div>
              <!-- end row -->
            </div>
        </div>
      <?php include '../Includes/footer.php';?> 
      </div>
      <!--  Add New Modal-->
        <div class="modal fade" id="AddEditUsers" tabindex="-1" role="">
            <div class="modal-dialog Users" role="document">
                <div class="modal-content">
                    <div class="card card-signup card-plain">
                        <div class="modal-header card-header-info">
                          <h4 class="card-title">Add Blogs</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="material-icons">clear</i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card ">
                                <div class="card-body ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-2">
                                              <label for="Name"><b>Name</b></label>   
                                            </div>
                                            <div class="col-md-10">
                                              <input type="text" placeholder="Enter Name" id="Name" class="form-control"  required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                              <label for="Email"><b>Email</b></label>      
                                            </div>
                                            <div class="col-md-10">
                                              <input type="text" placeholder="Enter Email" id="Email" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                              <label for="Contact"><b>Contact</b></label>           
                                            </div>
                                            <div class="col-md-10">
                                              <input type="text" placeholder="Enter Contact" id="Contact" class="form-control"  required> <br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                              <label for="Password"><b>Password</b></label> 
                                            </div>
                                            <div class="col-md-10">
                                              <input type="text" placeholder="Enter Password" id="Password" class="form-control"  required> <br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class ="col-md-5">
                                            </div>
                                            <button class="btn btn-info" type="button" onclick="addUser();" data-dismiss="modal">  Save </button>
                                            <button class="btn btn-info" data-dismiss="modal"> Cancel</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Modal-->

        <script src="../Scripts/users.js"></script>
  </body>
</html>