<!DOCTYPE html>
<html lang="en">
		<!-- Include Header Here-->
		<?php include '../Includes/header.php';?>
		<!--End-->
	<body class="">
		<div class="wrapper ">
			<!-- Include SideBar Here-->
			<?php include '../Includes/sideBar.php';?>
			<!--End-->
			
			<div class="main-panel" style="background-image: linear-gradient(#2b7a78, #def2f1);">
				<!-- Include navigation top header here-->
				<?php include '../Includes/topheader.php';?>
				<div class="content">
			        <div class="container-fluid">
			    		<!-- Add Data Here-->
			    		 <div class="row">
				            <div class="col-md-12">
				              <div class="card">
				                <div class="card-header card-header-info card-header-icon">
				                  <div class="card-icon">
				                    <i class="material-icons" style="font-size: 30px">star_rate</i>
				                  </div>
				                  <h4 class="card-title">List of Reviews</h4>
				                </div>
				                <div class="card-body">
				                  <div class="toolbar">
				                    <div class="text-right" >
						                <button class="btn btn-info" type="button"  data-toggle="modal" data-target="#AddEditReviews">Add new</button>
						            </div>
				                  </div>
				                  <div class="material-datatables">
				                  	<div class="table-responsive">
					                  	<table width="100%" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"  role="grid" aria-describedby="datatables_info" style="width: 100%;" cellspacing="0" id="reviewsTable">
					                      	<thead>
					                      		<tr class="bg-info text-white">
					                      			<th>Sr No.</th>
							        				<th>University Name </th>
							        				<th>Rating</th>
							        				<th>Delete</th>
							      				</tr>
					                      	</thead>
					                      	<tbody id="reviewsTableBody">
					                        </tbody>
					                    </table>	
				                  	</div>
				                  </div>
				                </div>
				                <!-- end content-->
				              </div>
				              <!--  end card  -->
				            </div>
				            <!-- end col-md-12 -->
				          </div>
		      		<!-- end row -->
			    	</div> 
     			</div>
			<?php include '../Includes/footer.php';?>	
			</div>
		</div>
		<!-- Add New Modal-->
				<div class="modal fade" id="AddEditReviews" tabindex="-1" role="">
				    <div class="modal-dialog AddEditReviews" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Add Reviews</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="University Name"><b>University Name</b></label>         
					                </div>
					                <div class="col-md-9">
					                	<select name="UniversityName" class="form-control" id="UniversityName">
					                	</select>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Rating"><b>Rating</b></label>           
					                </div>
					                <div class="col-md-9">
					                      <input type="text" placeholder="Enter Rating" id="Rating" class="form-control"  required>
					                </div>
					            </div>
                				<div class="row">
                 					<div class ="col-md-5">
               						</div>
               						<div class="col-md-7">
                						<button class="btn btn-info" type="button" onclick="addRating();" data-dismiss="modal"> Save </button>
                  						<button class="btn btn-info" data-dismiss="modal"> Cancel</button>
               						</div>

               					</div>
              				</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->
		</div>
		<script src="../Scripts/ratings.js"></script>
	</body>
</html>
