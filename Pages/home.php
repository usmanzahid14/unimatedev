<!DOCTYPE html>
<html lang="en">
<head>
  <title>Unimate</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      background-color: #3aafa9;
        margin-bottom: 0;
       border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #def2f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #3aafa9;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header"  >
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">  
        </button>  
        <div class="logo" >
            <img src="../Assets/img/logo3.png" width="150px" style="margin-left: 53px; margin-top: -20px">        
        </div>
      </div>
      <div class="collapse navbar-collapse text-center" id="myNavbar">
        <ul class="nav navbar-nav">
          <br><br><br>
          <li>
            <a  style="color: #17252a" href="../Pages/user.php"> Users </a>
          </li>
          <li>
            <a   style="color: #17252a" href="../Pages/university.php"> Universities</a>
          </li>
          <li>
            <a  style="color: #17252a" href="../Pages/programs.php"> Programs </a>
          </li>
          <li>
            <a  style="color: #17252a" href="../Pages/blogs.php">Blogs</a>
          </li>
          <li>
            <a  style="color: #17252a" href="../Pages/comments.php">Comments</a>
          </li>
          <li>
            <a  style="color: #17252a" href="../Pages/exchangeprograms.php">Exchange program</a>
          </li>
          <li>
            <a style="color: #17252a" href="../Pages/scholarships.php">Scholarships</a>
          </li>  
         <li>
            <a style="color: #17252a" href="../Pages/reviews.php">Review</a>
         </li>  
         <li>
            <a style="color: #17252a" href="../Pages/workshops.php">Workshops</a>
         </li>  
        </ul>    
      </div>
    </div>
  </nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <div class="well">
      <br><br>
        <div class="logo"  >
          <img src="../Assets/img/pic1.jpg" width="150px" style="margin-left: 10px; margin-top: -20px">        
        </div><br><br><br><br>
        <div class="logo">
          <img src="../Assets/img/pic2.jpg" width="150px" style="margin-left: 10px; margin-top: -20px">        
        </div>
      </div>
    </div>
    <div class="col-sm-8 text-left"> 
      <h1>Welcome To Admin Portal</h1>
      <p>It is a web site which consist of information about different universities. Students can get information about several universities, they can apply and can get admission through this. The main purpose of this website is to help widening the horizon of new students and acts as a bridge between the students who just passed out from Intermediate Examination and want to get admissions in universities. They will get relatable information about the universities.</p>
      <hr>
      <div class="alert alert-warning">
        <strong>Warning!</strong> You are not allowed without admin permission.
      </div><br>
      <div class="text-center" >
        <img src="../Assets/img/UNIMATE-4.png" width="250px" height="250px">
      </div>
    </div>
    <div class="col-sm-2 sidenav"><br><br><br>
      <div class="well" style="background: #3aafa9"  >
        <h3 style="color: #def2f1">Objective</h3>
        <p style="color: #def2f1"> The main purpose of this website is to help widening the horizon of new students and acts as a bridge between the students who just passed out from Intermediate Examination and want to get admissions in universities.      </p>
        </div>     
    </div>
  </div><br><br>

  <footer class="container-fluid text-center">
    <h3>Contact: unimate18@gmail.com</h3>
  </footer>
  <div class="modal fade" id="Login" tabindex="-1" role="">
    <div class="modal-dialog Login" role="document">
      <div class="modal-content">
        <div class="card card-signup card-plain">
          <div class="modal-header card-header-info">
            <h4 class="card-title">Login</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  <i class="material-icons">x</i>
              </button>
          </div>
          <div class="modal-body">
            <div class="card ">
              <div class="card-body ">
                <div class="container">            
                  <div class="form-group">
                    <label for="pwd">Password:
                    </label>
                    <input type="password" id="pwd" class="form-control">
                  </div>
                </div>
                <div class="row">
                  <div class ="col-md-6">
                  </div>
                  <div class="col-md-6">
                    <button class="btn btn-info" type="button"> Login 
                    </button>
                  </div>   
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>