<!DOCTYPE html>
<html lang="en">
		<!-- Include Header Here-->
		<?php include '../Includes/header.php';?>
		<!--End-->
	<body class="">
		<div class="wrapper ">
			<!-- Include SideBar Here-->
			<?php include '../Includes/sideBar.php';?>
			<!--End-->
			
			<div class="main-panel" style="background-image: linear-gradient(#2b7a78, #def2f1);">
				<!-- Include navigation top header here-->
				<?php include '../Includes/topheader.php';?>
				<div class="content">
			        <div class="container-fluid">
			    		<!-- Add Data Here-->
			    		 <div class="row">
				            <div class="col-md-12">
				              <div class="card">
				                <div class="card-header card-header-info card-header-icon">
				                  <div class="card-icon">
				                    <i class="fa fa-graduation-cap" style="font-size: 30px"></i>
				                  </div>
				                  <h4 class="card-title">List of Programs</h4>
				                </div>
				                <div class="card-body">
				                  <div class="toolbar">
				                    <div class="text-right" >
						                 <button class="btn btn-info" type="button"  data-toggle="modal" data-target="#AddEditPrograms">Add new</button>
						            </div>
				                  </div>
				                  <div class="material-datatables">
				                  	<div class="table-responsive">
					                  	<table width="100%" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"  role="grid" aria-describedby="datatables_info" style="width: 100%;" cellspacing="0" id="programTable">
					                      	<thead>
	                      					<tr class="bg-info text-white">
	                      						<th>Sr No.</th>
						                        <th>University Name </th>
						                        <th>Program Name</th>
						                        <th>Program Catogery</th>
						                        <th>Duration</th>
						                        <th>Fee</th>
						                        <th>Fee Duration</th>
						                        <th>Merit</th>
						                        <th>Scholarship</th>
						                        <th>Last Year Merit</th>
						                        <th>Edit</th>
						                        <th>Delete</th>
						                    </tr>
	                    					</thead>
					                      	<tbody id="programTableBody">
					                        </tbody>
					                    </table>	
				                  	</div>
				                  </div>
				                </div>
				                <!-- end content-->
				              </div>
				              <!--  end card  -->
				            </div>
				            <!-- end col-md-12 -->
				          </div>
		      		<!-- end row -->
			    	</div>
     			</div>
			<?php include '../Includes/footer.php';?>	
			</div>
			<!--  Add New Modal-->
				<div class="modal fade" id="AddEditPrograms" tabindex="-1" role="">
				    <div class="modal-dialog AddEditPrograms" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Add Programs</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="University Name"><b>University Name</b></label>         
					                </div>
					                <div class="col-md-9">
					                	<select name="UniversityName" class="form-control" id="UniversityName">
					                	</select>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="pName"><b>Program Name</b></label>
					                </div>
					               <div class="col-md-9">
					                      <input type="text" placeholder="Enter Program Name" id="ProgramName" class="form-control"  required> 
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Program Catogery"><b>Program Catogery</b></label>   
					                </div>
					            	<div class="col-md-9">
					            		<select class="form-control" id="catogery">
 										 <option value="Bacholers">Bacholers/Undergraduate (BS)</option>
 										 <option value="Masters">Masters (MS)</option>
										</select>
					            	</div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Duration"><b>Duration</b></label>
					                </div>
					               <div class="col-md-9">
					                      <input type="text" placeholder="Enter Duration" id="Duration" class="form-control"  required>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Fee"><b>Fee</b></label>           
					                </div>
					                <div class="col-md-9">
					                      <input type="text" placeholder="Enter Fee" id="Fee" class="form-control"  required>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Fee Duration"><b>Fee Duration</b></label>   
					                </div>
					            	<div class="col-md-9">
					            		<select class="form-control" id="FeeDuration">
 										 <option value="Monthly">Monthly</option>
 										 <option value="Quarterly">Quarterly</option>
 										 <option value="Semester Wise">Semester Wise</option>
										</select>
					            	</div>
					            </div>
					            <div class="row">
					            	<div class="col-md-3">
					                      <label for="Merit"><b>Merit</b></label>      
					                </div>
					            	<div class="col-md-9">
					                      <select class="form-control" id="Merit">
					                      	<option value="Yes">Yes</option>
					                      	<option value="No">No</option>
					                      </select>
					            	</div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Scholarship"><b>Scholarship</b></label>         
					                </div>
					                <div class="col-md-9">
					                      <select class="form-control" id="Scholarship">
					                      	<option value="Yes">Yes</option>
					                      	<option value="No">No</option>
					                      </select>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="LastYearMerit"><b>Last Year Merit</b></label>
					                </div>
					                <div class="col-md-9">
					                      <input type="text" placeholder="Enter Last Year Merit" id="LastYearMerit" 
					                      class="form-control"  required>
					                </div>
					            </div>
                				<div class="row">
                 					<div class ="col-md-5">
               						</div>
               						<div class="col-md-7">
                						<button class="btn btn-info" type="button" onclick="addProgram();" data-dismiss="modal">  Save </button>
                  						<button class="btn btn-info" data-dismiss="modal"> Cancel</button>
               						</div>

               					</div>
              				</div> 
						                </div>
						            </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<!-- end:: Modal-->

				<!--  Edit Modal-->
				<div class="modal fade" id="EditPrograms" tabindex="-1" role="">
				    <div class="modal-dialog EditPrograms" role="document">
				        <div class="modal-content">
				            <div class="card card-signup card-plain">
				                <div class="modal-header card-header-info">
				                	<h3 class="card-title">Edit Programs</h3>
				                   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				                      <i class="material-icons">clear</i>
				                    </button>
								</div>
				                <div class="modal-body">
				                	<div class="card ">
						                <div class="card-body ">
						                  	<div class="container">
					            <div class="row">
					            	<input type="hidden"  id="exKey">
					                <div class="col-md-3">
					                      <label for="University Name"><b>University Name</b></label>         
					                </div>
					                <div class="col-md-9">
					                	<input type="text" id="eUniName" class="form-control"  required readonly>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="pName"><b>Program Name</b></label>
					                </div>
					               <div class="col-md-9">
					                      <input type="text" id="eProgramName" class="form-control"  required> 
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Program Catogery"><b>Program Catogery</b></label>   
					                </div>
					            	<div class="col-md-9">
					            		<select class="form-control" id="eCatogery">
 										 <option value="Bacholers">Bacholers/Undergraduate (BS)</option>
 										 <option value="Masters">Masters (MS)</option>
										</select>
					            	</div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Duration"><b>Duration</b></label>
					                </div>
					               <div class="col-md-9">
					                      <input type="text" id="eDuration" class="form-control"  required>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Fee"><b>Fee</b></label>           
					                </div>
					                <div class="col-md-9">
					                      <input type="text" id="eFee" class="form-control"  required>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Fee Duration"><b>Fee Duration</b></label>         
					                </div>
					            	<div class="col-md-9">
					            		<select class="form-control" id="eFeeDuration">
 										 <option value="Monthly">Monthly</option>
 										 <option value="Quarterly">Quarterly</option>
 										 <option value="Semester Wise">Semester Wise</option>
										</select>
					            	</div>
					            </div>
					            <div class="row">
					            	<div class="col-md-3">
					                      <label for="Merit"><b>Merit</b></label>      
					                </div>
					            	<div class="col-md-9">
					                      <select class="form-control" id="eMerit">
					                      	<option value="Yes">Yes</option>
					                      	<option value="No">No</option>
					                      </select>
					            	</div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="Scholarship"><b>Scholarship</b></label>         
					                </div>
					                <div class="col-md-9">
					                      <select class="form-control" id="eScholarship">
					                      	<option value="Yes">Yes</option>
					                      	<option value="No">No</option>
					                      </select>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                      <label for="LastYearMerit"><b>Last Year Merit</b></label>
					                </div>
					                <div class="col-md-9">
					                      <input type="text" placeholder="Enter Last Year Merit" id="eLastYearMerit" 
					                      class="form-control"  required>
					                </div>
					            </div>
                				<div class="row">
                 					<div class ="col-md-4">
               						</div>
               						<div class="col-md-8">
                						<button class="btn btn-info" type="button" onclick="saveProgramChanges();" data-dismiss="modal">  Save Changes</button>
                  						<button class="btn btn-info" data-dismiss="modal">Discard</button>
               						</div>
               					</div>
              				</div> 
						 </div>
					</div>
				 </div>
			</div>
		</div>
	</div>
</div>
<!-- end:: Modal-->
		</div>
		<script src="../Scripts/programs.js"></script>
	</body>
</html>
