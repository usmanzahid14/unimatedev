<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en"> 
<!-- Include Header Here-->
<?php include '../Includes/header.php';?>
<!--End-->
<body class="">
<div class="wrapper ">
<!-- Include SideBar Here-->
<?php include '../Includes/sideBar.php';?>
<!--End-->

<div class="main-panel" style="background-image: linear-gradient(#2b7a78, #def2f1);">
<!-- Include navigation top header here-->
<?php include '../Includes/topheader.php';?>
<div class="content">
       <div class="container-fluid">
   	<!-- Add Data Here-->
   	<div class="row">
           <div class="col-md-12">
             <div class="card">
               <div class="card-header card-header-info card-header-icon">
                 <div class="card-icon">
                   <i class="fa fa-university" style="font-size: 30px"></i>
                 </div>
                 <h4 class="card-title">List of Universities</h4>
               </div>
               <div class="card-body">
                 <div class="toolbar">
                   <button class="btn btn-info" type="button"  data-toggle="modal" data-target="#AddEditUniversities" style="    float: right;">Add new</button>
                 </div>
                 <div class="table-responsive">
                 	<table width="100%" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"  role="grid" aria-describedby="datatables_info" style="width: 100%;" cellspacing="0" id="universityTable">
                     	<thead>
                     	<tr class="bg-info text-white">
                         	<th>Sr No. </th>
                          <th>University Name </th>
                         	<th>Email</th>
                         	<th>Phone</th>
                         	<th>Rank</th>
                         	<th>Location</th>
                         	<th>Sector</th>
                         	<th>Discrption</th>
                         	<th>Training Institute<th>
                         	<th>Matric Percentage<th>
                         	<th>Inter Percentage<th>
                         	<th>EntryTest Percentage<th>	
                         	<th>Image</th>	
                         	<th>Edit</th>
                         	<th>Delete</th>
   	                </tr>
                     	</thead>
                     	<tbody id="tableBody">
                       </tbody>
                   </table>	
                 	</div>
               </div>
               <!-- end content-->
             </div>
             <!--  end card  -->
           </div>
           <!-- end col-md-12 -->
         </div>
     	<!-- end row -->
   	</div>
     	</div>
<?php include '../Includes/footer.php';?>	
</div>
<!--  Add New Modal-->
<div class="modal fade" id="AddEditUniversities" tabindex="-1" role="">
   <div class="modal-dialog AddEditUniversities" role="document">
       <div class="modal-content">
           <div class="card card-signup card-plain">
               <div class="modal-header card-header-info">
               	<h3 class="card-title">Add university</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                     <i class="material-icons">clear</i>
                   </button>
               </div>
               <div class="modal-body">
               	<div class="card ">
               <div class="card-body ">
                 	<div class="container">
                     	<div class="row">
                       	<div class="col-md-2">
                       	<label for="UniversityName"><b>University Name</b></label>	   	
                       	</div>
                       <div class="col-md-10">
                         <input type="text" placeholder="Enter University Name" id="UniversityName" class="form-control" required>
                       </div>
                     	</div>
                     	<div class="row">
                     	<div class="col-md-2">
                       	<label for="Email"><b>Email</b></label>	   	
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter Email" id="Email" class="form-control" required>
                     	</div>
                     	</div>
                     	<div class="row">
                     	<div class="col-md-2">
                       	<label for="Phone"><b>Phone</b></label>    	
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter Phone" id="Phone" class="form-control" required>
                     	</div>
                     	</div>
                     	<div class="row">
                     	<div class="col-md-2">
                       	<label for="Rank"><b>Rank</b></label>	   	
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter Rank" id="Rank" class="form-control" required>
                     	</div>
                     	</div>
                     	<div class="row">
                     	<div class="col-md-2">
                       	<label for="Sector"><b>Sector</b></label>	   	
                       	</div>
                     	<div class="col-md-10">
                       	<select class="form-control" id="Sector">
                           	<option value="Government">Government</option>
                           	<option value="Private">Private</option>
                           	<option value="Semi-Government">Semi-Government</option>
                           	<option value="Fedral">Fedral</option>
                        </select>
                     	</div>
                     	</div>
                     	<div class="row">
                     	<div class="col-md-2">
                       	<label for="Discrption"><b>Discrption</b></label>	   	
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter Discrption" id="Discrption" class="form-control" required><br>
                     	</div>
                     	</div>
                     	<div class="row">
                     	<div class="col-md-2">
                       	<label for="Training Institute"><b>Training Institute</b></label>
                       	</div>
                     	<div class="col-md-10">
                       	<select class="form-control" id="TrainingInstitute">
                           	<option value="Yes">Yes</option>
                           	<option value="No">No</option>
                        </select>
                     	</div>
                     	</div>
                     	<div class="row">
                     	<div class="col-md-2">
                       	<label for="Location"><b>Location</b></label>
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter Location" id="Location" class="form-control" required>
                     	</div>
                     	<br>
                     	</div>
                     	<div class="row">
                     	  <div class="col-md-2">
                       	<label for="MatricPercentage"><b>Matric Percentage</b></label>
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter Matric Percentage" id="MatricPercentage" class="form-control" required>
                     	</div>
                     	</div>
                     	<div class="row">
                     	  <div class="col-md-2">
                       	<label for="InterPercentage"><b>Inter Percentage</b></label>
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter Inter Percentage" id="InterPercentage" class="form-control" required>
                     	</div>
                     	</div>
                     	<div class="row">
                     	  <div class="col-md-2">
                       	<label for="EntryTestPercentage"><b>EntryTest Percentage</b></label>
                       	</div>
                     	<div class="col-md-10">
                       	<input type="text" placeholder="Enter EntryTest Percentage" id="EntryTestPercentage" class="form-control" required>
                     	</div>
                     	</div>
                     	<br>
                      <div class="row">
                        <div class="col-md-2">
                       	  <label for="Image"><b>Image</b></label>
                       	</div>
                     	  <form action="upload.php" method="post" enctype="multipart/form-data">
                          <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">      
                        </form>
                      </div>
                     	<div class="row">
                     	<div class="col-md-5">
                       	</div>
                     	<div class="col-md-7">
                     	<button class="btn btn-info" onclick="uploadImage_addUni();" data-dismiss="modal">Save</button>
                       	<button class="btn btn-info" data-dismiss="modal">Cancel</button>
                     	</div>
                     	</div>
                    </div> 
                  </div>
                </div>
               </div>
           </div>
       </div>
   </div>
</div>
<!-- end:: Modal-->

<!--  Edit Modal-->
<div class="modal fade" id="EditUniversity" tabindex="-1" role="">
   <div class="modal-dialog EditUniversity" role="document">
       <div class="modal-content">
           <div class="card card-signup card-plain">
               <div class="modal-header card-header-info">
               	<h3 class="card-title">Edit university</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                     <i class="material-icons">clear</i>
                   </button>
</div>
               <div class="modal-body">
               	<div class="card ">
               <div class="card-body ">
                 	<div class="container">
 	<div class="row">
 	<input type="hidden" id="eKey">
   	<div class="col-md-2">
   	<label for="UniversityName"><b>University Name</b></label>	
   	</div>
   <div class="col-md-10">
     <input type="text" id="eUniversityName" class="form-control" required>
   </div>
 	</div>
 	<div class="row">
 	<div class="col-md-2">
   	<label for="Email"><b>Email</b></label>	   	
   	</div>
 	<div class="col-md-10">
   	<input type="text" id="eEmail" class="form-control" required>
 	</div>
 	</div>
 	<div class="row">
 	<div class="col-md-2">
   	<label for="Phone"><b>Phone</b></label>    	
   	</div>
 	<div class="col-md-10">
   	<input type="text" id="ePhone" class="form-control" required>
 	</div>
 	</div>
 	<div class="row">
 	<div class="col-md-2">
   	<label for="Rank"><b>Rank</b></label>	   	
   	</div>
 	<div class="col-md-10">
   	<input type="text" id="eRank" class="form-control" required>
 	</div>
 	</div>
 	<div class="row">
 	<div class="col-md-2">
   	<label for="Sector"><b>Sector</b></label>	   	
   	</div>
 	<div class="col-md-10">
   	<select class="form-control" id="eSector">
                     	<option value="Government">Government</option>
                     	<option value="Private">Private</option>
                     	<option value="Semi-Government">Semi-Government</option>
                     	<option value="Fedral">Fedral</option>
                     	</select>
 	</div>
 	</div>
 	<div class="row">
 	<div class="col-md-2">
   	<label for="Discrption"><b>Discrption</b></label>	   	
   	</div>
 	<div class="col-md-10">
   	<input type="text" id="eDiscrption" class="form-control" required><br>
 	</div>
 	</div>
 	<div class="row">
 	<div class="col-md-2">
   	<label for="Training Institute"><b>Training Institute</b></label>
   	</div>
 	<div class="col-md-10">
   	<select class="form-control" id="eTrainingInstitute">
                     	<option value="Yes">Yes</option>
                     	<option value="No">No</option>
                     	</select>
 	</div>
 	</div>
 	<div class="row">
 	<div class="col-md-2">
   	<label for="Location"><b>Location</b></label>
   	</div>
 	<div class="col-md-10">
   	<input type="text" id="eLocation" class="form-control" required><br>
 	</div>
 	</div>
                                                   
                                                   <div class="row">
 	  <div class="col-md-2">
   	<label for="MatricPercentage"><b>Matric Percentage</b></label>
   	</div>
 	<div class="col-md-10">
   	<input type="text" placeholder="Enter Matric Percentage" id="eMatricPercentage" class="form-control" required>

 	</div>
 	</div>
 	<div class="row">
 	  <div class="col-md-2">
   	<label for="InterPercentage"><b>Inter Percentage</b></label>
   	</div>
 	<div class="col-md-10">
   	<input type="text" placeholder="Enter Inter Percentage" id="eInterPercentage" class="form-control" required>

 	</div>
 	</div>
 	<div class="row">
 	  <div class="col-md-2">
   	<label for="EntryTestPercentage"><b>EntryTest Percentage</b></label>
   	</div>
 	<div class="col-md-10">
   	<input type="text" placeholder="Enter EntryTest Percentage" id="eEntryTestPercentage" class="form-control" required>
 	</div>
 	</div>





 	<div class="row">
 	<div class="col-md-4">
   	</div>
 	<div class="col-md-8">
 	<button class="btn btn-info" onclick="saveChangesUniversity();" data-dismiss="modal">Save Changes</button>
   	<button class="btn btn-info" data-dismiss="modal">Discard</button>
 	</div>
 	</div>
</div> 
               </div>
           </div>
               </div>
           </div>
       </div>
   </div>
</div>
<!-- end:: Modal-->
</div>
<script src="../Scripts/university.js"></script>
</body>
</html>