<div class="sidebar" data-color="azure" data-background-color="white" data-image="../Assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
          <img src="../Assets/img/logo3.png" width="150px" style="margin-left: 53px; margin-top: -20px">        
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active">
            <a class="nav-link" href="../Pages/home.php">
              <i class="material-icons">house</i>
              <p> Home </p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="../Pages/user.php">
              <i class="fa fa-user"></i>
              <p> Users
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="../Pages/university.php">
              <i class="fa fa-university"></i>
              <p> Universities
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/programs.php">
              <i class="fa fa-graduation-cap"></i>
              <p> Programs/Degrees </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/blogs.php">
              <i class="fa fa-edit"></i>
              <p> Blogs </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/reviews.php">
              <i class="material-icons">star_rate</i>
              <p> Reviews </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/comments.php">
              <i class="fa fa-comments"></i>
              <p> Comments </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/scholarships.php">
              <i class="fa">$</i>
              <p> Scholarships </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/workshops.php">
              <i class="material-icons">event</i>
              <p> Workshops/Events </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/news.php">
              <i class="material-icons">announcement</i>
              <p> News </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../Pages/exchangeprograms.php">
              <i class="material-icons">explicit</i>
              <p> Exchange Programs </p>
            </a>
          </li>
        </ul>
      </div>
    </div>