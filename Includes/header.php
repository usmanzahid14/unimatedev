
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    UniMate
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="../Assets/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../Assets/css/material-dashboard.minf066.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../Assets/demo/demo.css" rel="stylesheet" />
  
  <!-- End Google Tag Manager -->
  <!-- Jquery -->
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <!-- The core Firebase JS SDK is always required and must be listed first -->
  <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase.js"></script>
  
  
  <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-database.js"></script>
  

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#config-web-app -->

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#config-web-app -->

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCd8Ex_O5lRKxTBae1BhaQBVI-4CeGTlgA",
    authDomain: "unimate-d9901.firebaseapp.com",
    databaseURL: "https://unimate-d9901.firebaseio.com",
    projectId: "unimate-d9901",
    storageBucket: "unimate-d9901.appspot.com",
    messagingSenderId: "336982062095",
    appId: "1:336982062095:web:e3220eb439b1c4f7"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>

</head>
