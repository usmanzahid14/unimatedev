<?php 
if (isset($_GET['userID'])) {
  $userID=$_GET['userID'];
}
?>

<!-- Page Header-->
      <header class="page-head">
        <!-- RD Navbar Transparent-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-default" data-md-device-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-stick-up-offset="210" data-xl-stick-up-offset="85" data-lg-auto-height="true" data-md-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Right Side Toggle-->
                <button class="rd-navbar-top-panel-toggle d-lg-none" data-rd-navbar-toggle=".rd-navbar-top-panel"><span></span></button>
                <div class="rd-navbar-top-panel">
                  <div class="rd-navbar-top-panel-left-part">
                    <ul class="list-unstyled">
                      <li>
                        <div class="unit flex-row align-items-center unit-spacing-xs">
                          <div class="unit-left"><span class="icon mdi mdi-phone text-middle"></span></div>
                          <div class="unit-body">0000-0000000
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="unit flex-row align-items-center unit-spacing-xs">
                          <div class="unit-left"><span class="icon mdi mdi-map-marker text-middle"></span></div>
                          <div class="unit-body">C-II Johar Town, Lahore, Pakistan.</div>
                        </div>
                      </li>
                      <li>
                        <div class="unit flex-row align-items-center unit-spacing-xs">
                          <div class="unit-left"><span class="icon mdi mdi-email-open text-middle"></span></div>
                          <div class="unit-body">unimate18@gmail.com</div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="rd-navbar-top-panel-right-part">
                    <div class="rd-navbar-top-panel-left-part">
                      <div class="unit flex-row align-items-center unit-spacing-xs">
                        <div class="unit-left"><span class="icon mdi mdi-login text-middle"></span></div>
                        <div class="unit-body"><a href="loginRegister.php">Login/Register</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="rd-navbar-brand"><a class="d-inline-block" href="homePage.php">
                    <div class="unit align-items-sm-center unit-xl flex-column flex-xxl-row unit-spacing-custom">
                      <div class="unit-left"><img width='1000' height='1000' src='images/UNIMATE-4.png' alt=''  class="align-items-center" style="    padding: 7px 11px 4px 0px;
                                                     margin: -129px 0px -47px -2px;">
                      </div>
                       
                    </div></a></div>
              <div class="rd-navbar-menu-wrap clearfix">
                <!--Navbar Brand-->
               
                <div class="rd-navbar-nav-wrap">
                  <div class="rd-navbar-mobile-scroll">
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li><a href="homePage.php">Home</a></li>
                      <li><a href="universitiesPage.php?userID='<?php echo $userID?>'">Universities</a></li>
                      <li><a>Programs</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="programsPage.php?userID='<?php echo $userID?>'">Programs</a></li>
                          <li><a href="exchangePrograms.php?userID='<?php echo $userID?>'">Exchange Programs</a></li>                          
                        </ul>
                      </li>
                      <li><a>Comparison</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="compareUniversities.php?userID='<?php echo $userID?>'">Compare Universities</a>
                          </li>
                          <li><a href="comparePrograms.php?userID='<?php echo $userID?>'">Compare Programs</a>
                          </li>
                        </ul>
                      </li>
                      <li><a href="meritCalculator.php?userID='<?php echo $userID?>'">Merit Calculator</a></li>
                      <li><a href="admissionNews.php">Admissions</a></li>
                      <li><a href="eventsPage.php">Events</a></li>
                      <li><a href="blogs.php">Blogs</a></li>
                      <li><a href="contactUs.php">Contact Us</a></li>
                      <li id="profileImage"><a id="showImage"></a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="dashboard.php">Profile</a></li>
                          <li><a href="chatBox.php">Chat Box</a></li>
                          <li><a href="" onclick="signOut()">Sign out</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <script type="text/javascript">
        $(document).ready(function(){
          firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                $('#profileImage').show();
                var currentUserID = user.uid;
                var firebaseRef=firebase.database().ref().child("Users");
                firebaseRef.child(currentUserID).on("value", snap => { 
                  $('#showImage').html('');
                  var image=snap.child("userImageUrl").val();
                  $('#showImage').prepend('<img src=\"'+image+' \" style="border-radius:50%; height: 35px; width: 35px;">');
                });
              } else {
                $('#profileImage').hide();
              }
          }); 
        })
      </script>