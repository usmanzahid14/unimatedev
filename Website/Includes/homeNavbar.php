<?php 
if (isset($_GET['userID'])) {
  $userID=$_GET['userID'];
}
?>  
<header class="page-head" style="position:absolute; left:0; right:0;top:0">
        <!-- RD Navbar Transparent-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-transparent" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-stick-up-offset="40" data-lg-auto-height="true" data-auto-height="false" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true" data-md-focus-on-hover="false">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap"><span></span></button>
                <h4 class="panel-title d-xl-none">Home</h4>
              </div>
              <div class="rd-navbar-menu-wrap clearfix">
                <!--Navbar Brand-->
                <div class="rd-navbar-brand"><a class="d-inline-block" href="homePage.php">
                    <div class="unit align-items-sm-center unit-lg flex-xl-row unit-spacing-xxs">
                      <div class="unit-left">
                        <div class="wrap"><img width='1000' height='1000' src='images/UNIMATE-4.png' alt=''/>
                        </div>
                      </div>
                      <div class="unit-body text-xl-left">
                        <div class="rd-navbar-brand-title">Unimate</div>
                        <div class="rd-navbar-brand-slogan text-light">The Student Ease</div>
                      </div>
                    </div></a></div>
                <div class="rd-navbar-nav-wrap">
                  <div class="rd-navbar-mobile-scroll">
                    <div class="rd-navbar-mobile-header-wrap">
                      <!--Navbar Brand Mobile-->
                      <div class="rd-navbar-mobile-brand"><a href="homePage.php"><img width="200" height="200" src="images/UNIMATE-4.png" alt=""></a></div>
                    </div>
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li class="active"><a href="homePage.php">Home</a></li>
                      <li><a href="universitiesPage.php?userID='<?php echo $userID?>'">Universities</a></li>
                      <li><a>Programs</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="programsPage.php?userID='<?php echo $userID?>'">Programs</a></li>
                          <li><a href="exchangePrograms.php?userID='<?php echo $userID?>'">Exchange Programs</a></li>                          
                        </ul>
                      </li>
                      <li><a>Comparison</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="compareUniversities.php?userID='<?php echo $userID?>'">Compare Universities</a>
                          </li>
                          <li><a href="comparePrograms.php?userID='<?php echo $userID?>'">Compare Programs</a>
                          </li>
                        </ul>
                      </li>
                      <li><a href="meritCalculator.php?userID='<?php echo $userID?>'">Merit Calculator</a></li>
                      <li><a href="admissionNews.php">Admissions</a></li>
                      <li><a href="eventsPage.php">Events</a></li>
                      <li><a href="blogs.php">Blogs</a></li>
                      <li><a href="contactUs.php">Contact Us</a></li>
                      <li id="profileImage"><a id="showImage"></a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="dashboard.php">Profile</a></li>
                          <li><a href="chatBox.php">Chat Box</a></li>
                          <li><a href="" onclick="signOut()">Sign out</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
          </nav>
        </div>
      </header>
      <script type="text/javascript">
        $(document).ready(function(){
          firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                $('#profileImage').show();
                var currentUserID = user.uid;
                var firebaseRef=firebase.database().ref().child("Users");
                firebaseRef.child(currentUserID).on("value", snap => {
                  $('#showImage').html('');
                  var image=snap.child("userImageUrl").val();
                  $('#showImage').prepend('<img src=\"'+image+' \" style="border-radius:50%; height: 40px; width: 40px;">');
                });
              } else {
                $('#profileImage').hide();
              }
          }); 
        })
      </script>