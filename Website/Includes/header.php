<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="intense web design multipurpose template">
    <meta name="date" content="Dec 26">
    <script src="../cdn-cgi/apps/head/3ts2ksMwXvKRuG480KNifJ2_JNM.js"></script><link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,700%7CMerriweather:400,300,300italic,400italic,700,700italic">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/style.css">
    <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    
    <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-database.js"></script>
    <script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyCd8Ex_O5lRKxTBae1BhaQBVI-4CeGTlgA",
        authDomain: "unimate-d9901.firebaseapp.com",
        databaseURL: "https://unimate-d9901.firebaseio.com",
        projectId: "unimate-d9901",
        storageBucket: "unimate-d9901.appspot.com",
        messagingSenderId: "336982062095",
        appId: "1:336982062095:web:e3220eb439b1c4f7"
        };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    </script>
  </head>