<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Our Team</title>  
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark">
        <div class="container">
          <h1>People</h1>
        </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container">
          <h2 class="font-weight-bold">Our Respected Members</h2>
          <hr class="divider bg-madison">
          <div class="row row-30 text-lg-left offset-top-60">
            <div class="col-md-6 col-lg-3"><img class="img-responsive d-inline-block img-rounded" src="images/users/user-kathy-gibson-270x270.jpg" width="270" height="270" alt="">
              <div class="offset-top-20">
                <h6 class="font-weight-bold text-primary"><a href="teamMemberProfile.php">Kathy Gibson</a></h6>
              </div>
              <div class="offset-top-5">
                <p>Psychology</p>
              </div>
            </div>
            <div class="col-md-6 col-lg-3"><img class="img-responsive d-inline-block img-rounded" src="images/users/user-julie-weaver-270x270.jpg" width="270" height="270" alt="">
              <div class="offset-top-20">
                <h6 class="font-weight-bold text-primary"><a href="teamMemberProfile.php">Julie Weaver</a></h6>
              </div>
              <div class="offset-top-5">
                <p>Economics</p>
              </div>
            </div>
            <div class="col-md-6 col-lg-3"><img class="img-responsive d-inline-block img-rounded" src="images/users/user-peter-wong-270x270.jpg" width="270" height="270" alt="">
              <div class="offset-top-20">
                <h6 class="font-weight-bold text-primary"><a href="teamMemberProfile.php">William Barnett</a></h6>
              </div>
              <div class="offset-top-5">
                <p>Geology & Geophysics</p>
              </div>
            </div>
            <div class="col-md-6 col-lg-3"><img class="img-responsive d-inline-block img-rounded" src="images/users/user-russell-weber-270x270.jpg" width="270" height="270" alt="">
              <div class="offset-top-20">
                <h6 class="font-weight-bold text-primary"><a href="teamMemberProfile.php">Walter Myers</a></h6>
              </div>
              <div class="offset-top-5">
                <p>Graphic Design</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->

</html>