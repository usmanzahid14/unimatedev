<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Contact Us</title>
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div> 
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Contact Us</h1>
        </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="row row-50 justify-content-sm-center">
            <div class="col-sm-10 col-lg-8 text-lg-left">
              <h2 class="font-weight-bold">Get in Touch</h2>
              <hr class="divider bg-madison divider-lg-0">
              <div class="offset-top-30 offset-md-top-60">
                <p>You can contact us any way that is convenient for you. We are available 24/7 via email or contact number. You can also use a quick contact form below or visit our office personally. We would be happy to answer your questions.</p>
              </div>
              <div class="offset-top-30">
                <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post">
                  <div class="row row-12">
                    <div class="col-xl-12">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside" for="name">Name</label>
                        <input class="form-input" id="name" type="text" name="name" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="col-xl-12">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside" for="email">E-mail</label>
                        <input class="form-input" id="email" type="email" name="email" data-constraints="@Required @Email">
                      </div>
                    </div>
                    <div class="col-xl-12">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside" for="message">Message</label>
                        <textarea class="form-input" id="message" name="message" data-constraints="@Required" style="height: 220px"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="text-center text-xl-left offset-top-20">
                    <button class="btn button-primary" type="submit" onclick="saveMessage()">Send Message</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-sm-10 col-lg-4 text-left">
              <div class="inset-lg-left-30">
                <h6 class="font-weight-bold">Socials</h6>
                <div class="hr bg-gray-light offset-top-10"></div>
                <ul class="list-inline list-inline-xs list-inline-madison">
                  <li><a class="icon novi-icon icon-xxs fa fa-facebook icon-circle icon-gray-light-filled" href="https://www.facebook.com/"></a></li>
                  <li><a class="icon novi-icon icon-xxs fa fa-twitter icon-circle icon-gray-light-filled" href="https://twitter.com/login"></a></li>
                  <li><a class="icon novi-icon icon-xxs fa fa-instagram icon-circle icon-gray-light-filled" href="https://www.instagram.com/"></a></li>
                </ul>
                <div class="offset-top-30 offset-md-top-60">
                  <h6 class="font-weight-bold">Phones</h6>
                  <div>
                    <div class="hr bg-gray-light offset-top-10"></div>
                  </div>
                  <div class="offset-top-15">
                    <ul class="list list-unstyled">
                      <li><span class="icon icon-xs text-madison mdi mdi-phone text-middle"></span><a class="text-middle inset-left-10 text-dark">0000-0000000</a></li>
                      <li><span class="icon icon-xs text-madison mdi mdi-phone text-middle"></span><a class="text-middle inset-left-10 text-dark">0000-0000000</a></li>
                    </ul>
                  </div>
                </div>
                <div class="offset-top-30 offset-md-top-60">
                  <h6 class="font-weight-bold">E-mail</h6>
                  <div>
                    <div class="hr bg-gray-light offset-top-10"></div>
                  </div>
                  <div class="offset-top-15">
                    <ul class="list list-unstyled">
                      <li><span class="icon icon-xs text-madison mdi mdi-email-outline text-middle"></span><span>unimate18@gmail.com</span></a></li>
                    </ul>
                  </div>
                </div>
                <div class="offset-top-30 offset-md-top-60">
                  <h6 class="font-weight-bold">Address</h6>
                  <div>
                    <div class="hr bg-gray-light offset-top-10"></div>
                  </div>
                  <div class="offset-top-15">
                    <div class="unit flex-row unit-spacing-xs">
                      <div class="unit-left"><span class="icon icon-xs mdi mdi-map-marker text-madison"></span></div>
                      <div class="unit-body">
                        <p><a class="text-dark" href="#">C-II Johar Town, Lahore, Pakistan.</a></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="offset-top-30 offset-md-top-65">
                  <h6 class="font-weight-bold">Opening Hours</h6>
                  <div>
                    <div class="hr bg-gray-light offset-top-10"></div>
                  </div>
                  <div class="offset-top-15">
                    <div class="unit flex-row unit-spacing-xs">
                      <div class="unit-left"><span class="icon icon-xs mdi mdi-calendar-clock text-madison"></span></div>
                      <div class="unit-body">
                        <div>
                          <p>Mon-Fri: 9:00am-7:00pm</p>
                        </div>
                        <div>
                          <p>Sat: 9:00am-3:00pm</p>
                        </div>
                        <div>
                          <p>Sun: Closed</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/loginRegisterController.js"></script>
  </body>
  <!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>