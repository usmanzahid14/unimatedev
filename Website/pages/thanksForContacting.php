<?php 
if (isset($_GET['name'])) {
  $name = $_GET['name'];
}
?>

<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
    <title>Thank You For Contacting</title>
    <!-- Include Header Here-->
    <?php include '../Includes/header.php';?>
    <!--End-->
  <body>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <?php include '../Includes/pagesNavbar.php';?>
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Thank You</h1>
        </div>
      </section>
      <section class="section section-xl bg-catskill">
        <div class="container">
          <h3 class="font-weight-bold">Thank You for contacting us.</h3>
          <h3 class="font-weight-bold">Mr/Ms <?php echo $name?></h3>
          <hr class="divider bg-madison">
          <div class="offset-top-35 offset-lg-top-60 text-white view-animate fadeInUpSmall delay-06">We will get in touch with you soon.</div>
          <div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="homepage.php">Go to Home</a></div>
        </div>
      </section>
     <!-- Include Footer-->
     <?php include '../Includes/footer.php';?>
     <!--End-->
    </div>

    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>