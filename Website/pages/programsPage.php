<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<!-- Site Title-->
<title>Programs</title> 
<?php include '../Includes/header.php';?>
<style>
  .card1 {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    max-width: 380px;
    margin: auto;
    font-family: arial;
    }
</style>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Programs</h1>
        </div>
      </section>
      <div id="fb-root"></div>
      <section class="section section-xl bg-default">
        <div class="container">
          <h3 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04">Find the best program for you.</h3>
        </div>
      </section>
      <section class="section section-xl bg-default">
            <div class="container">
              <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                            <label>Select Desired University</label>
                            <select class="browser-default custom-select" id="UniversityName" onchange="getProgramsListByUni();">
                              <option selected>Open this select menu</option>
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                            <label>Select Desired Catogery</label>
                            <select class="form-control" id="programCatogery" onchange="getProListByCatogery()">
                              <option selected>Open this select menu</option>
                              <option value="Bacholers">Bacholers/Undergraduate (BS)</option>
                              <option value="Masters">Masters (MS)</option>
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                            <label>Select Desired Prgram</label>
                            <select class="form-control" id="programName">
                              <option selected>Open this select menu</option>
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                            <label></label><br>
                            <button type="button" class="btn button-primary" onclick="searchPrograms();">Search</button>
                        </div>
                    </div>
                </div>
              </div>
            </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container">
          <!-- Populate data form DB-->
          <div class="row row-30 justify-content-sm-center offset-top-60 text-md-left" id="displayPrograms">
            <!-- Data
                From
                DB -->
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/programsPageController.js"></script>
    <script src="js/loginRegisterController.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            loginChecker();
        })
    </script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>