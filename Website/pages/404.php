<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>404</title>
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Content-->
      <section class="section page-content section-cover section-404 section-30 bg-default">
        <div><a href="index-2.html"><img src="images/UNIMATE-4.png" width="400" height="400" alt=""></a></div>
        <div class="section-xxs">
          <div class="container">
            <div>
              <h1 class="font-default"><span class="big text-light font-weight-bold">404</span></h1>
            </div>
            <div class="offset-top-10">
              <h2 class="font-weight-bold">Sorry, the page was not found</h2>
            </div>
            <div class="offset-top-15 offset-lg-top-30">
              <hr class="divider bg-madison">
            </div>
            <div class="offset-top-30 offset-lg-top-60">
              <p>You may have mistyped the address or the page may have removed.</p>
            </div>
            <div class="offset-top-15 offset-lg-top-30">
              <div class="group group-xl"><a class="btn button-primary btn-icon btn-icon btn-icon-left" href="homePage.php"><span class="icon fa fa-arrow-left"></span><span>Back to Home Page</span></a><a class="btn button-default btn-icon btn-icon btn-icon-left" href="contactUs.php"><span class="icon fa fa-envelope"></span><span>Contact Us</span></a></div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>