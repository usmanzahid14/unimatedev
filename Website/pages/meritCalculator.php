<!DOCTYPE html> 
<html class="wide wow-animation scrollTo" lang="en">
<title>Merit Calculator</title>
<?php include '../Includes/header.php';?>
  <style type="text/css">
          .form-container div, .form-container  span{
          font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
      }

      .form-container ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
          color:    #999;
      }

      .form-container :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
         color:    #999;
         opacity:  1;
      }

      .form-container ::-moz-placeholder { /* Mozilla Firefox 19+ */
         color:    #999;
         opacity:  1;
      }

      .form-container :-ms-input-placeholder { /* Internet Explorer 10-11 */
         color:    #999;
      }

      .form-container :placeholder-shown { /* Standard (https://drafts.csswg.org/selectors-4/#placeholder) */
        color:  #999;
      }

      .oauth-buttons {
          text-align:center;
      }
      .row {
        text-align:center;
      }
      #login-form{
       min-width:375px;   
      }
      .login-container{
        width:400px;
        height:330px;
        display:inline-block;
        margin-top: -165px;
          top: 50%;
          left: 50%;
          position: absolute;
          margin-left: -200px;
      }
      .form-container .create-account:hover{
        opacity:.7;
      }
      .form-container .create-account{
        color:inherit;
        margin-top: 15px;
          display: inline-block;
        cursor:pointer;
        text-decoration:none;
      }

      .oauth-buttons .fa{
        cursor:pointer;
        margin-top:10px;
        color:inherit;
        width:30px;
        height:30px;
        text-align:center;
        line-height:30px;
        margin:5px;
        margin-top:15px;
      }
      .oauth-buttons .fa:hover{
        color:white;
      }
      .oauth-buttons .fa-google-plus:hover{
        background: #dd4b39;
      }
       .oauth-buttons .fa-facebook:hover{
        background: #8b9dc3;
      }

      .oauth-buttons .fa-linkedin:hover{
        background: #0077b5;
      }

      .oauth-buttons .fa-twitter:hover{
        background: #55acee;
      }

      .form-container .req-input .input-status {
          display: inline-block;
          height: 40px;
          width: 29px;
          float: left;  
      }
      .form-container .input-status::before{
        height:20px;
        width:20px;
        position:absolute;
        top:10px;
        left:10px;
        color:white;
        border-radius:50%;
        background:white;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
        
      }

      .form-container .input-status::after{
        height:10px;
        width:10px;
        position:absolute;
        top:15px;
        left:15px;
        color:white;
        border-radius:50%;
        background:#00BCD4;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }

      .form-container .req-input{
        width:100%;
          float:left;
        position:relative;
        background:#00BCD4;
        height:40px;
        display:inline-block;
        border-radius:0px;
        margin:5px 0px;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }

      .form-container div .row .invalid:hover{
        background:#EF9A9A;
      }

      .form-container div .row .invalid{
        background:#E57373;
      }

      .form-container .invalid .input-status:before {
        width:20px;
        height:4px;
        top:19px;
        left:10px;
        background:white;/*#F44336;*/
        border-radius:0px;
         -ms-transform: rotate(45deg); /* IE 9 */
          -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
          transform: rotate(45deg);
      }

      .form-container .invalid .input-status:after {
        width:20px;
        height:4px;
        background:white;
        border-radius:0px;
        top:19px;
        left:10px;
         -ms-transform: rotate(-45deg); /* IE 9 */
          -webkit-transform: rotate(-45deg); /* Chrome, Safari, Opera */
          transform: rotate(-45deg);
      }

      .form-container div .row  .valid:hover{
        background: #17252a;
      }

      .form-container div .row .valid {
        background: #2b7a78;
      }

      .form-container .valid .input-status:after {
        border-radius:0px;
          width: 17px;
          height: 4px;
          background: white;
          top: 16px;
          left: 15px;
          -ms-transform: rotate(-45deg);
          -webkit-transform: rotate(-45deg);
          transform: rotate(-45deg);
      }

      .form-container .valid .input-status:before {
        border-radius:0px;
          width: 11px;
          height: 4px;
        background:white;
          top: 19px;
          left: 10px;
          -ms-transform: rotate(45deg);
          -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
      }

      .form-container .input-container{
        padding:0px 20px;
      }

       .form-container .row-input{
        padding:0px 5px;
      }

      .form-container .req-input.input-password{
        margin-bottom:0px;
      }
      .form-container .req-input.confirm-password{
        margin-top:0px;
      }

      .form-container {
        margin:250px;
        padding:15px;
        border-radius:0px;
        background:#B3E5FC;
        color:#00838F;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }

      .form-container .form-title{
        font-size:25px;
        color:inherit;
        text-align:center;
        margin-bottom:10px;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }

      .form-container .submit-row{
        padding:0px 0px;
      }

      .form-container .btn.submit-form{
        margin-top:15px;
        padding:12px;
        background:#00BCD4;
        color:white;
        border-radius:0px;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }

      .form-container .btn.submit-form:focus{
        outline:0px;
        color:white;
      }

      .form-container .btn.submit-form:hover{
        background:#00cde5;
        color:white;
      }

      .form-container .tooltip.top .tooltip-arrow {
        border-top-color:#00BCD4 !important;
      }

      .form-container .tooltip.top.tooltip-invalid .tooltip-arrow {
        border-top-color:#E57373 !important;
      }

      .form-container .tooltip.top.tooltip-invalid .tooltip-inner::before{
        background:#E57373;
      }
      .form-container .tooltip.top.tooltip-invalid .tooltip-inner{
        background:#FFEBEE !important;
        color:#E57373;
      }

      .form-container .tooltip.top.tooltip-valid .tooltip-arrow {
        border-top-color:#81C784 !important;
      }

      .form-container .tooltip.top.tooltip-valid .tooltip-inner::before{
        background:#81C784;
      }
      .form-container .tooltip.top.tooltip-valid .tooltip-inner{
        background:#E8F5E9 !important;
        color:#81C784;
      }

      .form-container .tooltip.top .tooltip-inner::before{
        width:100%;
        height:6px;
        background:#00BCD4;
        position:absolute;
        bottom:5px;
        right:0px;
      }
      .form-container .tooltip.top .tooltip-inner{
        border:0px solid #00BCD4;
        background:#E0F7FA !important;
        color:#00ACC1;
        font-weight:bold;
        font-size:13px;
        border-radius:0px;
        padding:10px 15px;
      }
      .form-container .tooltip {
        max-width:150px;
        opacity:1 !important;
      }

      .form-container .message-box{
        width:100%;
        height:auto;
      }

      .form-container textarea:focus,.form-container textarea:hover{
        background:#fff;
        outline:none;
        border:0px;
      }

      .form-container .req-input textarea {
        max-width:calc(100% - 50px);
          width: 100%;
          height: 80px;
          border: 0px;
          color: #777;
          padding: 10px 9px 0px 9px;
        float:left;
        
      }
      .form-container input[type=text]:focus, .form-container input[type=password]:focus, .form-container input[type=email]:focus, .form-container input[type=tel]:focus, .form-container select{
          background:#fff;
        color:#777;
        border-left:0px;
        outline:none;
      }

      .form-container input[type=text]:hover,.form-container input[type=password]:hover,.form-container input[type=email]:hover,.form-container input[type=tel]:hover, . form-container select{
        background:#fff;
      }

      .form-container input[type=text], .form-container input[type=password], .form-container input[type=email],input[type=tel], form-container select{
        width:calc(100% - 50px);
        float:left;
        border-radius:0px;
        border:0px solid #ddd;
        padding:0px 9px;
        height:40px;
        line-height:40px;
        color:#777;
        background:#fff;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }
  </style>
 
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->      
      <?php include '../Includes/pagesNavbar.php';?>

      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Merit Calculator</h1>
        </div>
      </section>
      <!-- vertical link Tabs-->
      <section class="section section-xl bg-default">
        <div class="container">
          <h2 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04" style="    font-size: initial" style="padding: 10px 2px 6px 0px" >Calulate your Potential 
            </h2>
          <hr class="divider bg-madison">
        </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container">
            <div class="col-lg-12">
              <form style="margin: -330px 0 0 0">
                <div id="contact-form" class="form-container" data-form-container style="color: rgb(46, 125, 50); background: rgb(200, 230, 201);">
                  <div class="input-container">
                    <div class="row">
                       <span class="req-input valid">
                        <span class="input-status"></span>
                         <input class="form-input bg-default" placeholder="Enter Obtained Matric Marks" id="Matricmarks" type="text" name="MatricObtain" data-constraints="@Required">
                      </span>
                    </div>
                    <div class="row" >
                       <span class="req-input valid">
                        <span class="input-status"> </span>
                        <input class="form-input bg-default" placeholder="Enter Total Matric Marks" id="MatricTotal" type="text" name="MatricTotal">
                      </span>
                    </div>
                    <div class="row">
                       <span class="req-input valid">
                        <span class="input-status"> </span>
                        <input class="form-input bg-default" placeholder="Enter Obtained Intermediate Marks" id="InterMarks" type="text" name="InterObtain" data-constraints="@Required">
                      </span>
                    </div>
                    <div class="row">
                       <span class="req-input valid">
                        <span class="input-status"> </span>
                        <input class="form-input bg-default" id="InterTotal" placeholder="Enter Total Intermediate Marks" type="text" name="InterTotal" data-constraints="@Required">
                      </span>
                    </div>
                    <div class="row">
                       <span class="req-input valid">
                        <span class="input-status"> </span>
                        <input class="form-input bg-default" placeholder="Enter Obtained Entry Test Marks" id="EntryTestMarks" type="text" name="registerConfirmPass" data-constraints="@Required">
                      </span>
                    </div>
                    <div class="row">
                       <span class="req-input valid">
                        <span class="input-status"> </span>
                        <input class="form-input bg-default" placeholder="Enter Obtained Entery Test Marks" id="EntryTestTotal" type="text" name="registerConfirmPass" data-constraints="@Required">
                      </span>
                    </div>
                    <div class="row">
                       <span class="req-input valid">
                        <span class="input-status"> </span>
                        <select class="browser-default" id="UniversityName">
                          <option selected>Select the Desired Univerity</option>
                        </select>
                       </span>
                    </div>
                    <div class="row submit-row">
                      <button  type="button" class="btn btn-block submit-form valid"    data-target="#result" onclick="Calculate();"> Calculate 
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          <div class="col-lg-12">
            <section class="section section-xl bg-default">
              <div class="container text-center" id="result">
                <!-- data 
                    comming 
                    from 
                    js 
                    file-->
              </div>
            </section>
          </div>
          <div class="container">
            <div class="row row-30 justify-content-sm-center offset-top-60 text-md-left" id="suggestedPrograms">
                 <!-- data 
                     comming 
                      from 
                          js 
                         file-->
            </div>
          </div>
        </div>
     </section>



      <!-- Page Footer-->
      <!-- Page Footer-->
      <!-- Corporate footer-->
      <footer class="page-footer">
        <div class="hr bg-gray-light"></div>
        <div class="container section-xs block-after-divider">
          <div class="row row-50 justify-content-xl-between justify-content-sm-center">
            <div class="col-lg-3 col-xl-2">
              <!--Footer brand--><a class="d-inline-block" href="homePage.php"><img width='1000' height='1000' src='images/UNIMATE-4.png' alt=''/>
                <div>
                  <h6 class="barnd-name font-weight-bold offset-top-25">UniMate</h6>
                </div>
                <div>
                  <p class="brand-slogan text-gray font-italic font-accent">The Student Ease</p>
                </div></a>
            </div>
            <div class="col-sm-10 col-lg-5 col-xl-4 text-xl-left">
              <h6 class="font-weight-bold">Contact us</h6>
              <div class="text-subline"></div>
              <div class="offset-top-30">
                <ul class="list-unstyled contact-info list">
                  <li>
                    <div class="unit flex-row align-items-center unit-spacing-xs">
                      <div class="unit-left"><span class="icon mdi mdi-phone text-middle icon-xs text-madison"></span></div>
                      <div class="unit-body"><a class="text-dark">0000-0000000</a>
                      </div>
                    </div>
                  </li>
                  <li class="offset-top-15">
                    <div class="unit flex-row align-items-center unit-spacing-xs">
                      <div class="unit-left"><span class="icon mdi mdi-map-marker text-middle icon-xs text-madison"></span></div>
                      <div class="unit-body text-left"><a class="text-dark">C-II, Johar Town</a></div>
                    </div>
                  </li>
                  <li class="offset-top-15">
                    <div class="unit flex-row align-items-center unit-spacing-xs">
                      <div class="unit-left"><span class="icon mdi mdi-email-open text-middle icon-xs text-madison"></span></div>
                      <div class="unit-body">unimate18@gmail.com</div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="offset-top-15 text-left">
                <ul class="list-inline list-inline-xs list-inline-madison">
                  <li><a class="icon novi-icon icon-xxs fa fa-facebook icon-circle icon-gray-light-filled" href="https://www.facebook.com/"></a></li>
                  <li><a class="icon novi-icon icon-xxs fa fa-twitter icon-circle icon-gray-light-filled" href="https://twitter.com/login"></a></li>
                  <li><a class="icon novi-icon icon-xxs fa fa-instagram icon-circle icon-gray-light-filled" href="https://www.instagram.com/"></a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-10 col-lg-8 col-xl-4 text-xl-left">
              <h6 class="font-weight-bold">Newsletter</h6>
              <div class="text-subline"></div>
              <div class="offset-top-30 text-left">
                <p>Enter your email address to get the latest University news, special events and other activities delivered right to your inbox.</p>
              </div>
              <div class="offset-top-10">
                <form class="rd-mailform form-subscribe" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="https://livedemo00.template-help.com/wt_59029_v3/bat/rd-mailform.php">
                  <div class="form-wrap">
                    <div class="input-group input-group-sm">
                      <input class="form-input" placeholder="Your e-mail" type="email" name="email" data-constraints="@Required @Email"><span class="input-group-btn">
                        <button class="btn btn-sm button-primary" type="submit">Subscribe</button></span>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </footer>    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/meritCalculatorController.js"></script>
    <script src="js/loginRegisterController.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            loginChecker();
        })
    </script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>
