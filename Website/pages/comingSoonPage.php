<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Coming Soon</title>
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Content-->
      <section class="section section-cover section-404 section-30 bg-default container">
        <div><a href="homePage.php"><img src="images/UNIMATE-4.png" width="400" height="400" alt=""></a></div>
        <div class="section-50">
          <div class="offset-top-10">
            <h2 class="font-weight-bold">We're getting ready to launch in.</h2>
          </div>
          <div class="offset-top-15 offset-lg-top-30">
            <hr class="divider bg-madison">
          </div>
          <div class="offset-top-30 offset-lg-top-60" style="max-width: 800px; margin-left: auto; margin-right: auto">
            <div class="DateCountdown" data-type="until" data-date="2019-12-31 00:00:00" data-format="wdhms" data-color="#0d2d62" data-bg="rgba(245, 245, 245, 1)" data-width="0.02"></div>
          </div>
          <div class="row justify-content-sm-center offset-top-30 offset-md-top-60">
            <div class="col-md-6">
              <p>This page is under construction, we are working very hard to give you the best experience on our new web site.</p>
              <div class="offset-top-30">
                <p>Stay ready, we`re launching soon</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>