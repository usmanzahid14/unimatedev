<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>About Us</title>
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">About Us</h1>
        </div>
      </section>
      <!--A Meeting of Minds-->
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="row row-50">
            <div class="col-md-4 order-md-2 text-md-left">
              <div class="inset-md-left-30"><img class="img-responsive d-inline-block img-rounded" src="images/UNIMATE-4.png" width="340" height="300" alt="">
                <div class="offset-top-20">
                  <h6 class="text-center text-primary font-weight-bold">Unimate</h6>
                </div>
                <p class="text-center">The Student Ease</p>
                <p class="text-center">Established in 2019</p>
              </div>
            </div>
            <div class="col-md-8 order-md-1 text-md-left">
              <h2 class="font-weight-bold">Introduction</h2>
              <hr class="divider bg-madison divider-md-0">
              <div class="offset-top-30 offset-sm-top-60">
                <p>Did you ever wonder you will have to go outside your home and probably your city to just get some information about where you want to study and which university is the best for you? Stepping outside your house and starting from something you have no idea about can be troubling.</p>
              </div>
              <p>For all the problems we might have a solution. How is it helpful for you, you might ask yourself. This website Uni-Mate is made to help all the students who are waiting for their intermediate/A-Levels results and are looking forward to take admission in university. This is supposed to give you information about all the universities in Pakistan and the criteria for the degree you are looking for.  You can search for the specific universities or you will be suggested with some universities that offer your program. </p>
              <p>You might have questions like how does it work or how can we trust the source? You need not to worry. We will direct you to the page for admission form of the university. It’s easy to use and not too complicated. All you have to do is enter you marks and preferred program and area of residence and the app will automatically suggest you universities offering the program. The app will also give the details of scholarship providing university according to filter set by the user. This app also provides the comparison between universities and the programs offered by universities. </p>
            </div>
          </div>
        </div>
      </section>
      <!-- Images-->
      <section>
        <div class="container container-wide">
          <div class="row row-30">
            <div class="col-md-4"><img class="img-responsive d-inline-block" src="images/ab1.jpg" width="570" height="370" alt=""></div>
            <div class="col-md-4"><img class="img-responsive d-inline-block" src="images/ab2.jpg" width="570" height="370" alt=""></div>
            <div class="col-md-4"><img class="img-responsive d-inline-block" src="images/ab3.jpg" width="570" height="370" alt=""></div>
          </div>
        </div>
      </section>
      <!--A Meeting of Minds-->
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="row justify-content-sm-center justify-content-md-start">
            <div class="col-md-8 text-md-left">
              <h2 class="font-weight-bold">Our Focus</h2>
              <hr class="divider bg-madison divider-md-0">
              <div class="offset-top-30 offset-sm-top-60">
                <p>The main purpose of this website is to help widening the horizon of new students and acts as a bridge between the students who just passed out from Intermediate Examination and want to get admissions in universities. They will get relatable information about the universities. This website is to provide new horizons of knowledge to the youth of this country and also to inject the realization of career selection in appropriate universities. Uni-Mate offers comprehensive career counseling service, aimed at helping you to select the right University and choose the right career path.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>