<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
    <title>Template</title>
    <!-- Include Header Here-->
    <?php include '../Includes/header.php';?>
    <!--End-->
  <body>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <?php include '../Includes/pagesNavbar.php';?>
      <section class="section breadcrumb-classic context-dark">
        <div class="container">
          <h1>Contact Us</h1>
          <div class="offset-top-10 offset-md-top-35">
          </div>
        </div>
      </section>
      <section>
        
      </section>



     <!-- Include Footer-->
     <?php include '../Includes/footer.php';?>
     <!--End-->
    </div>

    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>