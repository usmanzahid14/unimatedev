<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
    <!-- Site Title-->
    <title>Compare Programs</title>
    <?php include '../Includes/header.php';?>
    <style>
        .card1 {
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
          max-width: 450px;
          margin: auto;
          font-family: arial;
          }
        .search-sec{
        padding: 2rem;
        }
        .search-slt{
            display: block;
            width: 100%;
            font-size: 0.875rem;
            line-height: 1.5;
            color: #55595c;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            height: calc(3rem + 2px) !important;
            border-radius:0;
        }
        .wrn-btn{
            width: 100%;
            font-size: 16px;
            font-weight: 400;
            text-transform: capitalize;
            height: calc(3rem + 2px) !important;
            border-radius:0;
        }
        @media (min-width: 992px){
            .search-sec{
                position: relative;
                top: -114px;
                background: white;
            }
        }

        @media (max-width: 992px){
            .search-sec{
                background: #1A4668;
            }
        }
        div.comparisontable{
        display: flex;
        flex-direction: column; /* turn children ul elements into stacked rows */
        }
        div.comparisontable ul.row{
            list-style: none;
            display: flex; /* turn children li elements into flex children */
            flex: 1;
            flex-wrap: wrap;
        }
         
        div.comparisontable ul.row li{
            background: #def2f1;
            flex: 1;
            padding: 10px;
            border-bottom: 1px solid gray;
        }
         
        /* the legend column (first li within each row) */
        div.comparisontable ul.row li.legend{
            background: #2b7a78;
            color: white;
            border: none;
            width: 200px;
            border-bottom: 1px solid white;
        }
         
        /* very first row */
        div.comparisontable ul.row:first-of-type li{
            text-align: center;
        }
         
        /* very last row */
        div.comparisontable ul.row:last-of-type li{
            text-align: center;
            border-bottom: none;
            box-shadow: 0 6px 6px rgba(0,0,0,0.23);
        }
         
         
        /* first and last cell within legend column */
        div.comparisontable ul.row:first-of-type li.legend.legend,
        div.comparisontable ul.row:last-of-type li.legend{
            background: #2b7a78;
            box-shadow: none;
        }
    </style>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Compare Programs</h1>
        </div>
      </section>
      <div id="fb-root"></div>
      <section class="section section-xl bg-default">
        <div class="container">
            <h3 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04">Select & Compare Programs of different Universities Here</h3>
        </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="search-sec">
            <div class="container">
              <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 p-0">
                            <select class="form-control search-slt" id="programSearch">
                                <option>Select Any Program</option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 p-0">
                            <button type="button" class="btn button-primary" onclick="comparePrograms();">Compare</button>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        <div class="comparisontable">
            <ul class="row">
                <li class="legend">University Name</li>
                <li>Program Name</li>
                <li>Duration</li>
                <li>Fee</li>
                <li>Fee Duration</li>
                <li>Merit Based</li>
                <li>Scholarship</li>
                <li>Last year Merit</li>
                <li>Category</li>
                <li>Rating</li>
            </ul>
        </div>
        <br><br>
        <div class="comparisontable" id="compareBody">
            Select and click compare to get the comparison of your desired program.
            <!-- data
                    from 
                    db-->
       </div>
       <h2 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04">Our Recomendation</h2>
       <div class="offset-top-35 offset-lg-top-60 text-white view-animate fadeInUpSmall delay-06">Best university with best rating is</div><br>
       <div class="text-subline"></div>
       <h3 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04" id="suggestion">
           <!-- name from db -->
       </h3>
       <div class="text-subline"></div>
     </div>
   </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/programsPageController.js"></script>
    <script src="js/loginRegisterController.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            loginChecker();
        })
    </script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>