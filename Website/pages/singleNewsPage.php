<?php 
if (isset($_GET['newsID'])) {
  $newsKey = $_GET['newsID'];
?>
 
<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>News</title>
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">News Details</h1>
        </div>
      </section>
      <div id="fb-root"></div>
      <!-- Latest news-->
      <section class="section section-xl">
        <div class="container">
          <div class="row row-85 justify-content-sm-center">
            <div class="col-md-8 col-lg-8 text-lg-left">
              <div id="newsId">
                <!-- Data
                      from
                        database-->
              </div>
            </div>
            <div class="col-lg-4 text-left col-sm-8">
              <aside class="inset-lg-left-30">
                <div class="offset-top-60 text-center"><a class="d-block" href="#"><img class="img-responsive d-inline-block" src="images/blog/banner-340x500.jpg" width="340" height="500" alt=""></a></div>
              </aside>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/loginRegisterController.js"></script>
    <script src="js/newsPageController.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
         loginChecker();
         var id='<?php echo $newsKey?>';
         singleNews(id);
      })
    </script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>

<?php 
}
else{
    echo "No News Found";
}
?>