<?php 
if (isset($_GET['userID'])) {
  $userID=$_GET['userID'];
}
?>

<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
    <title>Unimate</title>
    <!-- Include Header Here-->
    <?php include '../Includes/header.php';?>
    <!--End-->
    <style>
      .card1 {
        background-color: #feffff;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 370px;
        margin: auto;
        font-family: arial;
      }
      .card2 {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 370px;
        margin: auto;
        font-family: arial;
      }
    </style>
  <body>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <?php include '../Includes/homeNavbar.php';?>
      <section>
        <!-- Swiper-->
        <div class="swiper-container swiper-slider swiper-slider-3" data-autoplay="true" data-height="100vh" data-loop="true" data-dragable="false" data-min-height="480px" data-slide-effect="true">
          <div class="swiper-wrapper">
            <div class="swiper-slide" data-slide-bg="images/img-3.jpg" style="background-position: 80% center">
              <div class="swiper-slide-caption header-transparent-slide-caption">
                <div class="container">
                  <div class="row justify-content-sm-center justify-content-xl-start no-gutters">
                    <div class="col-lg-6 text-lg-left col-sm-10">
                      <div data-caption-animate="fadeInUp" data-caption-delay="100" data-caption-duration="1700">
                        <h1 class="font-weight-bold">The Student Ease</h1>
                      </div>
                      <div class="offset-top-20 offset-xs-top-40 offset-xl-top-60" data-caption-animate="fadeInUp" data-caption-delay="150" data-caption-duration="1700">
                        <h5 class="text-regular font-default">Any successful career starts with good education. Together with us you will have deeper knowledge of the subjects that will be especially useful for you when climbing the career ladder.</h5>
                      </div>
                      <div class="offset-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400" data-caption-duration="1700"><a class="btn button-madison btn-ellipse" href="loginRegister.php">Sign In</a>
                        <div class="inset-sm-left-30 d-xl-inline-block"><a class="btn button-primary btn-ellipse d-none d-xl-inline-block" href="aboutUs.php">Learn More</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide" data-slide-bg="images/img-1.jpg" style="background-position: 80% center">
              <div class="swiper-slide-caption header-transparent-slide-caption">
                <div class="container">
                  <div class="row justify-content-sm-center justify-content-xl-start no-gutters">
                    <div class="col-lg-6 text-lg-left col-sm-10">
                      <div data-caption-animate="fadeInUp" data-caption-delay="100" data-caption-duration="1700">
                        <h1 class="font-weight-bold">Investing in Knowledge.</h1>
                      </div>
                      <div class="offset-top-20 offset-xs-top-40 offset-xl-top-60" data-caption-animate="fadeInUp" data-caption-delay="150" data-caption-duration="1700">
                        <h5 class="text-regular font-default">At UniMAte, you can succeed in lots of research areas and benefit from investing in your education and knowledge that will help you in becoming an experienced specialist.</h5>
                      </div>
                      <div class="offset-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400" data-caption-duration="1700"><a class="btn btn-ellipse button-madison" href="loginRegister.php">Sign In</a>
                        <div class="inset-sm-left-30 d-xl-inline-block"><a class="btn btn-ellipse button-primary d-none d-xl-inline-block" href="aboutUs.php">Learn More</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide" data-slide-bg="images/img-3.jpg" style="background-position: 80% center">
              <div class="swiper-slide-caption header-transparent-slide-caption">
                <div class="container">
                  <div class="row justify-content-sm-center justify-content-xl-start no-gutters">
                    <div class="col-lg-6 text-lg-left col-sm-10">
                      <div data-caption-animate="fadeInUp" data-caption-delay="100" data-caption-duration="1700">
                        <h1 class="font-weight-bold">Open Minds. <br class="d-none d-lg-inline-block"> Creating Future.</h1>
                      </div>
                      <div class="offset-top-20 offset-xs-top-40 offset-xl-top-60" data-caption-animate="fadeInUp" data-caption-delay="150" data-caption-duration="1700">
                        <h5 class="text-regular font-default">Build your future with us! The educational programs of our University will give you necessary skills, training, and knowledge to make everything you learned here work for you in the future.</h5>
                      </div>
                      <div class="offset-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400" data-caption-duration="1700"><a class="btn btn-ellipse button-madison" href="loginRegister.php">Sign In</a>
                        <div class="inset-sm-left-30 d-xl-inline-block"><a class="btn btn-ellipse button-primary d-none d-xl-inline-block" href="aboutUs.php">Learn More</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Swiper Pagination-->
          <div class="swiper-pagination"></div>
        </div>
      </section>
      <!-- A Few Words About us-->
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="row row-50 text-lg-left justify-content-md-between">
            <div class="col-lg-7 view-animate fadeInRightSm delay-04">
              <div class="img-wrap-2">
                <figure><img class="img-responsive d-inline-block" src="images/UNIMATE-4.png" width="620" height="350" alt=""></figure>
              </div>
            </div>
            <div class="col-lg-5">
              <h2 class="home-headings-custom font-weight-bold view-animate fadeInLeftSm delay-06"><span class="first-word">About</span> Our Idea</h2>
              <div class="offset-top-35 offset-lg-top-60 view-animate fadeInLeftSm delay-08">
                <p>The main purpose of this website is to help widening the horizon of new students and acts as a bridge between the students who just passed out from Intermediate Examination and want to get admissions in universities. They will get relatable information about the universities. This website is to provide new horizons of knowledge to the youth of this country and also to inject the realization of career selection in appropriate universities. Uni-Mate offers comprehensive career counseling service, aimed at helping you to select the right University and choose the right career path.</p>
              </div>
              <div class="offset-top-30 view-animate fadeInLeftSm delay-1"><a class="btn btn-ellipse btn-icon btn-icon-right button-default" href="aboutUs.php"><span class="icon fa fa-arrow-right"></span><span>Learn More</span></a></div>
            </div>
          </div>
        </div>
      </section>
      <!--Our Featured Univerities-->
      <section class="section bg-madison section-xl text-center">
        <div class="container">
          <h2 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04">Our Univeristies</h2>
          <div class="offset-top-35 offset-lg-top-60 text-white view-animate fadeInUpSmall delay-06" style="font-size: 16px;">Our Featured Universities are selected through a rigorous process and certified as well.</div>

          <!-- Populate data form DB-->
          <div class="row row-30 justify-content-sm-center offset-top-60 text-md-left" id="homeUniPopulate">
            <!-- Data
                  from
                    DB --> 
          </div>
          <div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse btn-icon btn-icon-right button-default" style="color: #def2f1" href="universitiesPage.php?userID='<?php echo $userID?>'"><span class="icon fa fa-arrow-right"></span><span>View All Universities</span></a></div>
        </div>
      </section>
      <!-- Counters-->
      <section class="section text-center bg-default">
        <div class="container">
          <div class="row justify-content-sm-center justify-content-md-start offset-top-0">
            <div class="col-sm-10 col-md-7 section-image-aside section-image-aside-right">
              <div class="section-image-aside-img d-none d-md-block view-animate zoomInSmall delay-04" style="background-image: url(images/home-01-846x1002.jpg)"></div>
              <div class="section-image-aside-body section-xl inset-lg-right-70 inset-xl-right-110">
                <h2 class="home-headings-custom font-weight-bold view-animate fadeInLeftSm delay-04"><span class="first-word">Our</span> Features</h2>
                <div class="offset-top-35 offset-md-top-60 view-animate fadeInLeftSm delay-06">Unimate was founded on the principle that guiding students and sharing what we learn, we make their careers a way better.</div>
                <div class="text-center text-sm-left">
                  <div class="row row-65 justify-content-sm-center justify-content-lg-start offset-top-65 counters">
                    <div class="col-sm-6 view-animate fadeInLeftSm delay-08">
                      <!-- Counter 1-->
                      <div class="unit flex-column flex-sm-row unit-responsive-md counter-type-2">
                        <div class="unit-left"><span class="icon icon-md text-madison mdi mdi-school"></span></div>
                        <div class="unit-body">
                          <div class="h3 font-weight-bold text-primary"><span class="counter" data-speed="1200">15</span><span>+</span></div>
                          <div class="offset-top-3">
                            <h6 class="text-black font-accent font-weight-bold">Certified Universities</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 view-animate fadeInLeftSm delay-04">
                      <!-- Counter 2-->
                      <div class="unit flex-column flex-sm-row unit-responsive-md counter-type-2">
                        <div class="unit-left"><span class="icon icon-md text-madison mdi mdi-wallet-travel"></span></div>
                        <div class="unit-body">
                          <div class="h3 font-weight-bold text-primary"><span class="counter" data-speed="1250">30</span><span>+</span></div>
                          <div class="offset-top-3">
                            <h6 class="text-black font-accent font-weight-bold">Programs</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 view-animate fadeInLeftSm delay-1">
                      <!-- Counter type 1-->
                      <div class="unit flex-column flex-sm-row unit-responsive-md counter-type-2">
                        <div class="unit-left"><span class="icon icon-md text-madison mdi mdi-domain"></span></div>
                        <div class="unit-body">
                          <div class="h3 font-weight-bold text-primary offset-top-5"><span class="counter" data-step="500">10</span></div>
                          <div class="offset-top-3">
                            <h6 class="text-black font-accent font-weight-bold">Blogs</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 view-animate fadeInLeftSm delay-06">
                      <!-- Counter type 1-->
                      <div class="unit flex-column flex-sm-row unit-responsive-md counter-type-2">
                        <div class="unit-left"><span class="icon icon-md text-madison mdi mdi-account-multiple-outline"></span></div>
                        <div class="unit-body">
                          <div class="h3 font-weight-bold text-primary offset-top-5"><span class="counter" data-step="1500">1340</span></div>
                          <div class="offset-top-3">
                            <h6 class="text-black font-accent font-weight-bold">Students</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <br>
      <section class="section bg-catskill section-xl bg-default">
        <div class="container container-wide">
          <h2 class="home-headings-custom font-weight-bold view-animate fadeInUpSmall delay-06">Blogs</h2>
          <hr class="divider bg-madison">
          <!-- Data from DB-->
          <div class="row row-50 offset-top-60 justify-content-sm-center" id="homeBlogsPopulate">
          <!-- Data 
               coming 
               from 
               DB-->  
          </div>
          <div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse btn-icon btn-icon-right button-default" href="blogs.php"><span class="icon fa fa-arrow-right"></span><span>View All Blogs</span></a></div>
        </div>
      </section>
      <!-- Latest news-->
      <section class="section bg-catskill section-xl bg-default">
        <div class="container container-wide">
          <h2 class="home-headings-custom font-weight-bold view-animate fadeInUpSmall delay-06">Latest News</h2>
          <hr class="divider bg-madison">
          <div class="row row-30 offset-top-60 text-left justify-content-sm-center" id="homeNewsDivs">
            <!-- Data
                  from
                    Database-->
          </div>
          <div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse btn-icon btn-icon-right button-default" href="admissionNews.php"><span class="icon fa fa-arrow-right"></span><span>View All News</span></a></div>
        </div>
      </section>
      <footer class="page-footer section-xs section-bottom-20" style="background-color: #3aafa9">
        <div class="row">
          <div class="col-4">
            <div class="row justify-content-sm-center">
              <div class="col-lg-3 col-xl-2">
                <!--Footer brand-->
                <a class="d-inline-block view-animate zoomInSmall delay-06" href="index.html">
                  <div>
                    <h6 class="barnd-name font-weight-bold offset-top-12">UniMate</h6>
                  </div>
                  <div>
                    <p class="brand-slogan text-gray font-italic font-accent" style="color: #def2f1">The Student Ease</p>
                  </div>
                </a>
              </div>
              <div class="col-sm-12 offset-top-15 offset-sm-top-40 text-center">
                <ul class="list-inline list-inline-xs list-inline-madison">
                  <li><a class="icon novi-icon icon-xxs fa fa-facebook icon-circle icon-gray-light-filled view-animate zoomInSmall delay-04" href="#"></a></li>
                  <li><a class="icon novi-icon icon-xxs fa fa-twitter icon-circle icon-gray-light-filled view-animate zoomInSmall delay-06" href="#"></a></li>
                  <li><a class="icon novi-icon icon-xxs fa fa-google icon-circle icon-gray-light-filled view-animate zoomInSmall delay-08" href="#"></a></li>
                  <li><a class="icon novi-icon icon-xxs fa fa-instagram icon-circle icon-gray-light-filled view-animate zoomInSmall delay-1" href="#"></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-8 row justify-content-sm-center">
              <div class="col-md-4">
                <div class="unit flex-column unit-responsive-md section-45">
                  <div class="unit-left"><span class="icon icon-contact-sm text-madison mdi mdi-phone" style="color: #17252a"></span></div>
                  <div class="unit-body"><a class="h6 text-regular" href="#">0000-0000000</a></div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="unit flex-column unit-responsive-md section-45">
                  <div class="unit-left"><span class="icon icon-contact-sm text-madison mdi mdi-map-marker" style="color: #17252a"></span></div>
                  <div class="unit-body"><a class="h6 text-regular" href="#">C-II Johar Town<br class="d-none d-xl-inline">Lahore, Pakistan.</a></div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="unit flex-column unit-responsive-md section-45">
                  <div class="unit-left"><span class="icon icon-contact-sm text-madison mdi mdi-email-open" style="color: #17252a"></span></div>
                  <div class="unit-body"><a class="h6 text-regular" href="#">unimate18@gmail.com</a></div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-sm-12 text-center offset-top-20 offset-sm-top-45">
          <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>Unimate - The Student Ease</span><span> Co.&nbsp;</span></p>
        </div>
      </footer>
    </div>
    

    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/homepageController.js"></script>
    <script src="js/loginRegisterController.js"></script>
  </body>
  <!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>
