<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Gallery</title>  
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark">
        <div class="container">
          <h1>Our Gallery</h1>
        </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container">
          <h2>Highlights</h2>
          <hr class="divider bg-madison">
          <div class="offset-top-60">
            <div class="row row-30 justify-content-sm-center" data-lightgallery="group">
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-01-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #1</h4>
                    </div>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-01-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-01-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-02-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #2</h4>
                    </div>
                    <hr class="divider divider-sm"/>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-02-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-02-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-03-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #3</h4>
                    </div>
                    <hr class="divider divider-sm"/>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-03-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-03-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-04-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #4</h4>
                    </div>
                    <hr class="divider divider-sm"/>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-04-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-04-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-05-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #5</h4>
                    </div>
                    <hr class="divider divider-sm"/>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-05-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-05-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-06-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #6</h4>
                    </div>
                    <hr class="divider divider-sm"/>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-06-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-06-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-07-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #7</h4>
                    </div>
                    <hr class="divider divider-sm"/>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-07-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-07-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4">
                <figure class="thumbnail-classic">
                  <div class="thumbnail-classic-img-wrap"><img src="images/portfolio/gallery-08-370x370.jpg" alt="" width="370" height="370"/>
                  </div>
                  <figcaption class="thumbnail-classic-caption text-center">
                    <div>
                      <h4 class="thumbnail-classic-title">Photo #8</h4>
                    </div>
                    <hr class="divider divider-sm"/>
                    <a class="thumbnail-classic-link icon icon-xxs fa fa-search-plus" href="images/portfolio/gallery-08-original.jpg" data-lightgallery="item"><img src="images/portfolio/gallery-08-370x370.jpg" alt="" width="370" height="370"/></a>
                  </figcaption>
                </figure>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>