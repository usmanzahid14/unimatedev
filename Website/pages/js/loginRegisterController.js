$(document).ready(function(){
	
})

function signIn(email,password){
	var email=$('#loginUsername').val();
	var password=$('#loginPassword').val();

	firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // ...
          alert(errorMessage);
        });

		firebase.auth().onAuthStateChanged(function(user) {
         if (user) {
                   window.location.replace("http://localhost/casperian/website/views/homepage.php");   
         } else {
           // No user is signed in.
         }
        });
}

function signUp(email,password){
	var userName=$('#registerUsername').val();
	var email=$('#registerEmail').val();
	var contact=$('#registerContact').val();
	var password=$('#registerPassword').val();

	firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user){
	  	console.log('uid',user)
	  	var userNode = {
	                    'email': email,
	                    'contact': contact,
	                    'password': password,
	                    'username': userName,
	                    'bio': "Hey! I'm using UniMate...",
	                    'userImageUrl': "https://firebasestorage.googleapis.com/v0/b/unimate-d9901.appspot.com/o/UserImages%2Fuser-2517433_960_720.png?alt=media&token=2d89b345-e75b-4a13-9c22-c4dc9738c2c8",
	                 	}
		firebase.database().ref('Users/'+user.user.uid).set(userNode);

	}).catch(function(error) {
	  // Handle Errors here.
		  var errorCode = error.code;
		  var errorMessage = error.message;
	  	  alert("Error:  "+ errorMessage);
	   });
}

function loginChecker(){
  firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
  			console.log(user.uid)
  } else {
   window.location.replace("http://localhost/casperian/website/views/login.php");
   alert('You are not signed in. Sign In first');
  }
  });
}

function signOut(){
 firebase.auth().signOut();
 alert('You are signed out.');
}

function forgetPassword(){
	var verifyEmail=$('#forgotEmail').val();
	firebase.auth().sendPasswordResetEmail(verifyEmail);
	alert('Check Your Email for new password link.');
}