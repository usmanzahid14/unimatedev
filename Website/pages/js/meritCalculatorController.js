$(document).ready(function(){
getUniversitiesList();
}) 

function getUniversitiesList(){

  //getting data from DB to display in drop down menu
  var firebaseRef=firebase.database().ref().child("Universities");
  firebaseRef.orderByKey().on("value", snap => {

  snap.forEach(function(snapshot) {  
    var Key=snapshot.key;
    var uniName=snapshot.child("name").val();
    $('#UniversityName').append('<option value='+Key+'>'+uniName+'</option>');
    })
  }); 
}

function Calculate(){ 

     var key= $('#UniversityName').val();
     var uniKey=null;
     var matricPercentage=null;
     var interPercentage=null;
     var entryTestPercentage=null;

     var firebaseRef=firebase.database().ref().child('Universities');
     firebaseRef.orderByKey().on("value", snap => {
       snap.forEach(function(snapshot) {
       uniKey=snapshot.key;
       matricPercentage=snapshot.child("matricPercentage").val();
       interPercentage=snapshot.child("interPercentage").val();
       entryTestPercentage=snapshot.child("entryTestPercentage").val();
       })
     })
     
     $('#result').html(' ');
     var keyMOM= $('#Matricmarks').val();
     var keyMTM= $('#MatricTotal').val();
     var keyIOM= $('#InterMarks').val();
     var keyITM= $('#InterTotal').val();
     var keyETM= $('#EntryTestMarks').val();
     var keyETT= $('#EntryTestTotal').val();

     var Result = ((keyETM/ keyETT )* entryTestPercentage)+((keyIOM/keyITM)*interPercentage)+((keyMOM/keyMTM)*matricPercentage);
     $('#result').append('<h2 class="home-headings-custom font-weight-bold view-animate fadeInLeftSm delay-06"><span class="first-word">Your</span> Aggregate:'+Result+'</h2><div class="offset-top-35 offset-lg-top-60 view-animate fadeInLeftSm delay-08"><p> According to your Aggregate we suggest you following Programs of Selected University.</p></div>');
  

     $('#suggestedPrograms').html('');
     var flag=-1;
     var firebaseRef=firebase.database().ref().child('Program');
     firebaseRef.orderByChild("UID").equalTo(key).on("child_added", snap => {
        var key=snap.key;
        var tempProgramName=snap.child("programName").val();
        var tempUniversityName=snap.child("uName").val();
        var tempDuration=snap.child("duration").val();
        var tempFee=snap.child("fee").val();
        var tempFeeDuration=snap.child("feeDuration").val();
        var tempOpenMerit=snap.child("openMerit").val();
        var tempScholarship=snap.child("scholarship").val();
        var tempCatogery=snap.child("Catogery").val();
        var tempLastYearMerit=snap.child("lastYearMerit").val();

        if(Result >= tempLastYearMerit){
            flag=1;
            $('#suggestedPrograms').prepend('<div class="col-md-6 col-lg-4 view-animate fadeInRightSm delay-1 border  border-success" style="margin: 5px"><article class="post-news bg-default"><div class="post-news-body-variant-1"><div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h3 class="font-weight-bold">'+tempProgramName+'</h3><div class="hr divider bg-madison hr-left-0"></div><div class="card" style="width: 18rem;"><ul class="list-group list-group-flush"><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-school" data-toggle="tooltip" data-placement="top" title="Catogery"></span>'+tempCatogery+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-clock" data-toggle="tooltip" data-placement="top" title="Degree Duration"></span>'+tempDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-coin" data-toggle="tooltip" data-placement="top" title="Degree Fee"></span>'+tempFee+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-calendar" data-toggle="tooltip" data-placement="top" title="Fee Pattern"></span>'+tempFeeDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span>'+tempOpenMerit+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span>'+tempScholarship+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-clock" data-toggle="tooltip" data-placement="top" title="Last Year Merit"></span>'+tempLastYearMerit+'</li></ul></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><input type="hidden"  id="uKey"><p class="text-base">'+tempUniversityName+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span></li></ul></div></div></article></div>');
        } 
        else{
            flag=0;
            }
    });
    if (flag=0) {
      $('#suggestedPrograms').prepend('<h2 class="home-headings-custom font-weight-bold view-animate fadeInLeftSm delay-06"><span class="first-word">Your</span> Aggregate is too low, according to eligible creiteria</h2>')
    }
}