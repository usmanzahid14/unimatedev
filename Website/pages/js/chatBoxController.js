$(document).ready(function(){
getChatHeader();
getMaters();
})  
  
function getChatHeader(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("Users");
        firebaseRef.child(currentUserID).on("value", function(snap) {
          $('#chatHeaderDiv').html('');    
          var key=snap.key;
          var currentUsername=snap.child("username").val();
          var tempImage=snap.child("userImageUrl").val();
          
          $('#chatHeaderDiv').prepend('<img id="profile-img" src=\"'+tempImage+'\"  class="online" alt="" /><p>'+currentUsername+'</p>'); 
        }); 
      }
    });
}

function currentTimeStamp(){
  var currentTime = new Date();
  var fYear=currentTime.getFullYear();
  var fmonth=currentTime.getMonth()+1;
  var fday=currentTime.getDate();
  var fhh=currentTime.getHours();
  var fmm=currentTime.getMinutes();
  var fss=currentTime.getSeconds();
  return fday;
}

function convertTimeToHours(time){
  var filedate=new Date(time);
  var fYear=filedate.getFullYear();
  var fmonth=filedate.getMonth()+1;
  var fday=filedate.getDate();
  var fhh=filedate.getHours();
  var fmm=filedate.getMinutes();
  var fss=filedate.getSeconds();
  var today = currentTimeStamp();

  if(fday == today){
    if(fhh>12){
      fhh= fhh-12;
      filedate=fhh+':'+fmm+ ' pm';
    }
    else{
      filedate=fhh+':'+fmm+ ' am';
    }
  }
  else{
    filedate=fday+'/'+fmonth+'/'+fYear;
  }
  return filedate;
}

function getMaters(){
  var currentUserID;
  var userArray = [];
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      currentUserID = user.uid         
      var firebaseRef=firebase.database().ref().child("socialNetwork");
      firebaseRef.child(currentUserID).child("approved").child("maters").on("value", snap=> {
        snap.forEach(function(snapshot){
          var key =snapshot.key;
          var userID=snapshot.child("userID").val();
          
            //getting mater name & image
          var name, image, materKey;
          var firebaseRef2=firebase.database().ref().child("Users");
          firebaseRef2.child(userID).on("value",function(snap){
            materKey=snap.key;
            name=snap.child("username").val();
            image=snap.child("userImageUrl").val();

            //getting last message of the mater
            var receiver, message, imageMessage, time, timeInHours, seen;
            var unseenCount=0;
            var firebaseRef3=firebase.database().ref().child("Chat");
            firebaseRef3.child(currentUserID).child(userID).on("value",snap=>{
              snap.forEach(function(snapshot){
                message = null;
                imageMessage = null;
                receiver=snapshot.child("for").val();
                message=snapshot.child("message").val();            
                imageMessage=snapshot.child("imageMessageURL").val();
                time=snapshot.child("timeStamp").val();
                timeInHours = convertTimeToHours(time);
                seen=snapshot.child("seen").val();

                if(receiver==currentUserID){
                  if(!seen){
                    unseenCount++
                  } 
                }
                if(unseenCount==0){
                  $('#unseen_'+userID+'').hide();
                }
              });

              var checkUser = jQuery.inArray(userID, userArray);
              if(checkUser<0){
                userArray.push(userID);

                if(message || message==" "){ 
                  if (receiver==currentUserID){                    
                    $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span> <img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'">'+message+'</p></div></div></li>');
                  }                    
                  else{
                    $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span><img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'"><span>You:</span> '+message+' </p></div></div></li>');
                  }
                }
                else if(imageMessage){
                  if (receiver==currentUserID){
                    $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span> <img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'"> Image</p></div></div></li>');
                  }                    
                  else{
                    $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span><img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'"><span>You:</span> Image</p></div></div></li>');
                  }
                }
                else{
                  $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span><img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="preview" id="msg_'+userID+'"> Tap to Start Chat...</p></div></div></li>');
                  }
                }
                else{
                  if(message){
                    if (receiver==currentUserID){
                      $('#time_'+userID+'').html(timeInHours);
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#msg_'+userID+'').html(message);
                    }                    
                    else{
                      $('#time_'+userID+'').html(timeInHours);
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#msg_'+userID+'').html('<span>You:</span> '+message);
                    }
                  }
                  else if(imageMessage){
                    if (receiver==currentUserID){
                      $('#time_'+userID+'').html(timeInHours);
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#msg_'+userID+'').html(' Image');
                    }                    
                    else{
                      $('#time_'+userID+'').html(timeInHours);
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#msg_'+userID+'').html('<span>You:</span> Image');
                    }
                  }
                  else{
                    $('#msg_'+userID+'').html(' Tap to Start Chat...');
                  }
                }
            });
          });  
        });    
      });

      var firebaseRef=firebase.database().ref().child("socialNetwork");
      firebaseRef.child(currentUserID).child("approved").child("matings").on("value", snap=> {
        $('#getMatersDiv').html(' ');
        snap.forEach(function(snapshot){
          var key =snapshot.key;
          var userID=snapshot.child("userID").val();
          
            var name, image, materKey;
            var firebaseRef2=firebase.database().ref().child("Users");
            firebaseRef2.child(userID).on("value",function(snap){
              materKey=snap.key;
              name=snap.child("username").val();
              image=snap.child("userImageUrl").val();

              //getting last message of the mater
              var receiver, message, imageMessage, time, timeInHours, seen;
              var unseenCount=0;
              var firebaseRef3=firebase.database().ref().child("Chat");
              firebaseRef3.child(currentUserID).child(userID).on("value",snap=>{
                snap.forEach(function(snapshot){
                  message = null;
                  imageMessage = null;
                  receiver=snapshot.child("for").val();
                  message=snapshot.child("message").val();            
                  imageMessage=snapshot.child("imageMessageURL").val(); 
                  time=snapshot.child("timeStamp").val();
                  seen=snapshot.child("seen").val();
                  timeInHours = convertTimeToHours(time);

                  if(receiver==currentUserID){
                    if(!seen){
                      unseenCount++
                    } 
                  }
                  if(unseenCount==0){
                    $('#unseen_'+userID+'').hide();
                  }
                });

                var checkUser = jQuery.inArray(userID, userArray);
                if(checkUser<0){
                  userArray.push(userID);
                  if(message){
                    if (receiver==currentUserID){
                      $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span> <img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'">'+message+'</p></div></div></li>');
                    }                    
                    else{
                      $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span><img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'"><span>You:</span> '+message+' </p></div></div></li>');
                    }
                  }
                  else if(imageMessage){
                    if (receiver==currentUserID){
                      $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span> <img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'"> Image</p></div></div></li>');
                    }                    
                    else{
                      $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span><img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="text-right " style="margin: -19px 95px 0 0;" id="unseen_'+userID+'">'+unseenCount+'</p><p class="text-right" style="margin: -19px 0 0 0;" id="time_'+userID+'"> '+timeInHours+'</p><p class="preview" id="msg_'+userID+'"><span>You:</span> Image</p></div></div></li>');
                    }
                  }
                  else{
                    $('#getMatersDiv').prepend('<li class="contact" onclick="getMessageBox(\'' + materKey + '\',\'' + name + '\',\'' + image + '\');"><div class="wrap"><span class="contact-status online"></span><img src=\"'+image+'\" alt="" /><div class="meta"><p class="name">'+name+'</p><p class="preview" id="msg_'+userID+'"> Tap to Start Chat...</p></div></div></li>');
                  }
                }
                else{
                  if(message){
                    if (receiver==currentUserID){
                      $('#time_'+userID+'').html(timeInHours);
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#msg_'+userID+'').html(message);
                    }                    
                    else{
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#time_'+userID+'').html(timeInHours);
                      $('#msg_'+userID+'').html('<span>You:</span> '+message);
                    }
                  }
                  else if(imageMessage){
                    if (receiver==currentUserID){
                      $('#time_'+userID+'').html(timeInHours);
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#msg_'+userID+'').html(' Image');
                    }                    
                    else{
                      $('#time_'+userID+'').html(timeInHours);
                      $('#unseen_'+userID+'').html(unseenCount);
                      $('#msg_'+userID+'').html('<span>You:</span> Image');
                    }
                  }
                  else{
                    $('#msg_'+userID+'').html(' Tap to Start Chat...');
                  }
                }
              });
            });
              
        });    
      });
    }
  });
}

function getMessageBox(chatterID, chatterName, chatterImage){
  $('#messagesHeader').html('');
  $('#messagesHeader').prepend('<img src=\"'+chatterImage+' \" alt=""><p><strong>'+chatterName+'</strong></p><div class="social-media"><i class="fa fa-trash" aria-hidden="true" data-toggle="modal" data-target="#deleteChat"></i><i class="fa fa-facebook" aria-hidden="true"></i><i class="fa fa-twitter" aria-hidden="true"></i><i class="fa fa-instagram" aria-hidden="true"></i></div>');

  var currentUserID, currentUserImage;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      currentUserID = user.uid

      var firebaseRef=firebase.database().ref().child("Users");
      firebaseRef.child(currentUserID).on("value",function(snap){
        currentUserImage=snap.child("userImageUrl").val();
      });

      var receiver, message, time;
      var firebaseRef2=firebase.database().ref().child("Chat");
      firebaseRef2.child(currentUserID).child(chatterID).on("value",snap=>{
        $('#messagesDiv').html('');
        snap.forEach(function(snapshot){
          receiver=snapshot.child("for").val();
          message=snapshot.child("message").val();
          image=snapshot.child("imageMessageURL").val();
          time=snapshot.child("timeStamp").val();
          var timeInHours = convertTimeToHours(time);

          if (message){
            if (receiver==currentUserID){
              $('#messagesDiv').append('<li class="sent"><img src=\"'+chatterImage+' \" alt=""><p>'+message+'<br><span class="text-right" style="margin: 0px 0px 0px 210px;"> '+timeInHours+'</span></p></li>');
            }                    
            else{
              $('#messagesDiv').append('<li class="replies"><img src=\"'+currentUserImage+' \" alt=""><p>'+message+'<br><span class="text-right" style="margin: 0px 0px 0px 210px;"> '+timeInHours+'</span></p></li>');
            }
          }
          else{
            if (receiver==currentUserID){
              $('#messagesDiv').append('<li class="sent"><img src=\"'+chatterImage+' \" alt=""><p><img src=\"'+image+' \" alt="" style="height: 250px; width: 250px"><br><span class="text-right" style="margin: 0px 0px 0px 210px;"> '+timeInHours+'</span></p></li>');
            }                    
            else{
              $('#messagesDiv').append('<li class="replies"><img src=\"'+currentUserImage+' \" alt=""><p><img src=\"'+image+' \" alt="" style="height: 250px; width: 250px"><br><span class="text-right" style="margin: 0px 0px 0px 210px;"> '+timeInHours+'</span></p></li>');
            }
          }
        }); 
      });
    }
  });
  $('#chatterID').val(chatterID);
  $('#chatterID2').val(chatterID);
}

function sendMessage(){
  var chatterID=$('#chatterID').val();
  var message=$('#typedMessage').val();

  if(chatterID){
    var currentUserID;
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        currentUserID = user.uid

        var messageNode = {
                          'for':chatterID,
                          'message': message,
                          'timeStamp': firebase.database.ServerValue.TIMESTAMP,
                          'seen': false
                        }
        var keyRef = firebase.database().ref('Chat').push().key;
        firebase.database().ref('Chat/'+currentUserID+'/'+ chatterID +'/'+ keyRef).update(messageNode);
        firebase.database().ref('Chat/'+chatterID+'/'+ currentUserID +'/'+ keyRef).update(messageNode);
      }
    });
  }
  $('#typedMessage').val('');
}

function sendImage(downloadURL){
  var chatterID=$('#chatterID2').val();

  if(chatterID){
    var currentUserID;
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        currentUserID = user.uid

        var messageNode = {
                          'for':chatterID,
                          'imageMessageURL': downloadURL,
                          'timeStamp': firebase.database.ServerValue.TIMESTAMP,
                          'seen': false
                        }
        var keyRef = firebase.database().ref('Chat').push().key;
        firebase.database().ref('Chat/'+currentUserID+'/'+ chatterID +'/'+ keyRef).update(messageNode);
        firebase.database().ref('Chat/'+chatterID+'/'+ currentUserID +'/'+ keyRef).update(messageNode);
      }
    });
  }
  $('#fileToUpload').val('');
}

function previewImage(event) {
  var reader = new FileReader();
  reader.onload = function(){
    var output = document.getElementById('preview');
    output.src = reader.result;
    }
  reader.readAsDataURL(event.target.files[0]);
}

function deleteConversation(){
  var chatterID=$('#chatterID').val();
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      currentUserID = user.uid

      firebase.database().ref('/Chat/'+ currentUserID +'/'+ chatterID).remove();
      firebase.database().ref('/Chat/'+ chatterID +'/'+ currentUserID).remove();
    }
  });
}

$(document).on('change', '#fileToUpload', function(event) {
    selectedFile = event.target.files[0];
    //uploadImage();
    previewImage(event);
  });

  var uploadImage= function() {
  var filename=selectedFile.name;
  var storageRef = firebase.storage().ref();
  var uploadTask = storageRef.child('/MessagesImages/'+filename).put(selectedFile);

  uploadTask.on('state_changed', function(snapshot){
   
   var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
   console.log('Upload is ' + progress + '% done');
   switch (snapshot.state) {
     case firebase.storage.TaskState.PAUSED: // or 'paused'
       console.log('Upload is paused');
       break;
     case firebase.storage.TaskState.RUNNING: // or 'running'
       console.log('Upload is running');
       break;
   }
  }, function(error) {
   // Handle unsuccessful uploads
  }, function() {
   // Handle successful uploads on complete
   // For instance, get the download URL: https://firebasestorage.googleapis.com/...
   uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
     sendImage(downloadURL)
   });
  });
}