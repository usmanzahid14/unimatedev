var selectedFile; 
 
$(document).ready(function(){
populateUserDetails();
getOldPassword();
getSubscribedUnis();
getUser();
getPendingSendRequests();
getPendingMateRequests();
getMatersList();
getMatingsList();
getBlockedList();
getNotifications();
})
 
function populateUserDetails(){
	var userkey;
	firebase.auth().onAuthStateChanged(function(user) {
  	if (user) { 
  			userkey = user.uid
  			
  			var firebaseRef=firebase.database().ref().child("Users");
    		firebaseRef.child(userkey).on("value", function(snap) {
              $('#userDetail').html('');   
              $('#userImage').html('');   
		          var key=snap.key;
		          var contact=snap.child("contact").val();
		          var email=snap.child("email").val();
		          var pass=snap.child("password").val();
              var name=snap.child("username").val();
              var bio=snap.child("bio").val();
		          var tempImage=snap.child("userImageUrl").val();

              $('#userImage').prepend('<div class="row"><div class="col-md-12"><div class="bio-image"><img src=\"'+tempImage+' \" alt="image" /></div><a><span class="btn-file icon icon-xs mdi mdi-pencil"><input class="form-input bg-default" id="iKey" type="hidden" name="username" data-constraints="@Required"><input type="file" name="fileToUpload" id="fileToUpload" data-constraints="@Required" style="width: 45%"></span></a></div></div>');
		          $('#userDetail').prepend('<div class="bio-content"><h1 class="card-text"><strong>'+name+'</strong></h1><div class="hr bg-gray-light offset-top-10"></div><span class="icon icon-xs mdi mdi-phone">  '+contact+'</span><br><br><span class="icon icon-xs mdi mdi-email">  '+email+'</span><h4 class="card-text"><strong>Bio: </strong> '+bio+'</h4><br><div class="profile-userbuttons"><button type="button" class="btn button-primary btn-md" onclick="getUserEditDetails(\'' + key + '\');" data-toggle="modal" data-target="#EditProfile">Edit Profile</button> </div></div>'); 
		          $('#iKey').val(key);
        }); 
  		}
  	});
}

function getUserEditDetails(userID){
	var key = userID;
	var firebaseRef=firebase.database().ref().child("Users");
    firebaseRef.child(key).on("value", snap => {   
          var key=snap.key;
          var contact=snap.child("contact").val();
          var email=snap.child("email").val();
          var pass=snap.child("password").val();
          var name=snap.child("username").val();
          var bio=snap.child("bio").val();
  
  		  $('#eusername').val(name);
    	  $('#email').val(email);	
    	  $('#contact').val(contact);	
    	  $('#bio').val(bio);	
        $('#eKey').val(key);   	
        });
}

function saveChanges(){
  var tempKey=$('#eKey').val();
  var name=$('#eusername').val();
  var email=$('#email').val();
  var phone=$('#contact').val();
  var bio=$('#bio').val();

  //convert into json 
  var userNode = {
                  'email': email,
                  'contact': phone,
                  'username': name,
                  'bio':bio,
                 }
  firebase.database().ref('Users/'+tempKey).update(userNode);
}

function changeImage(downloadURL){
  var tempKey=$('#iKey').val();
  var imageNode = {
                  'userImageUrl':downloadURL,
                 }
  firebase.database().ref('Users/'+tempKey).update(imageNode);            
}

function getOldPassword(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("Users");
        firebaseRef.child(currentUserID).on("value", snap => {   
            var key=snap.key;
            var pass=snap.child("password").val();
  
            $('#cKey').val(key);  
            $('#oldPassword').val(pass);  
          });
        
        }  
  })
}

function changePassword(){
  var tempKey=$('#cKey').val();
  var pass=$('#newPassword').val();

  var passwordNode = {
                      'password': pass,
                     }
  firebase.database().ref('Users/'+tempKey).update(passwordNode);
  alert('Password Changed successfully!');
}

function getUser(){
  $('#getUsers').html();
  var firebaseRef=firebase.database().ref().child('Users');
  firebaseRef.orderByKey().on("value", snap => {
    snap.forEach(function(snapshot) {
    
    var key=snapshot.key;
    var bio=snapshot.child("bio").val();
    var name=snapshot.child("username").val();
    var image=snapshot.child("userImageUrl").val();
    
    $('#getUsers').prepend('<div class="col-md-4"><img src=\"'+image+' \" alt="Display Picture" style="width:100%; height: 350px; width: 300;"><h1>'+name+'</h1><p class="title"><strong>Bio:</strong> '+bio+'</p><p><button style="background-color: #3aafa9" onclick="sendMateRequest(\'' + key + '\')">Mate</button></p></div>');   
    })
  });
}

function sendMateRequest(key){
  var name;
  var firebaseRef=firebase.database().ref().child("Users");
    firebaseRef.child(key).on("value", snap => {
          name=snap.child("username").val();
      })

  //current user
  var currentUserID;
  var currentUsername
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("Users");
        firebaseRef.child(currentUserID).on("value", snap => {   
            currentUsername=snap.child("username").val();
        })
        
        var matingNode = {
                 'userID': key,
                 'name': name,
                 'seen': false,
                 };

        var materNode = {
                 'userID': currentUserID,
                 'name': currentUsername,
                 'seen': false,       
                }

        var notificatioNode = {
                  'userID': currentUserID,
                  'name': currentUsername,
                  'type': "send",
                  'seen': false,
                  }

        var keyRef = firebase.database().ref('socialNetwork').push().key;
        firebase.database().ref('socialNetwork/'+currentUserID+'/pendings/sentRequests/'+ keyRef).update(matingNode);
        firebase.database().ref('socialNetwork/'+key+'/pendings/mateRequests/'+ keyRef).update(materNode);
        
        var keyRef = firebase.database().ref('Notifications').push().key;
        firebase.database().ref('Notifications/'+key+'/'+ keyRef).update(notificatioNode);
        alert("Request sent for mating!");
      }
    });
}

function getSubscribedUnis(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("Subscriptions");
        firebaseRef.child(currentUserID).on("value", snap => {   
            $('#subscriptions').html('');
            var lineCounter=1;
            snap.forEach(function(snapshot) {
              var subKey=snapshot.key;
              var uniID=snapshot.child("universityID").val();
              var rowDiv='<div class="row">';
              //get data from university table
              var firebaseRef=firebase.database().ref().child("Universities");
                firebaseRef.child(uniID).on("value", snap2 => {                  

                  var key=snap2.key;
                  var image=snap2.child("universityImageUrl").val();
                  var location=snap2.child("location").val();
                  var name=snap2.child("name").val();
                  //For handling margin and padding in future
                  //var checker=lineCounter/3;
                  // if(checker % 1 == 0){
                  //   alert(lineCounter);
                  // }
                  $('#subscriptions').prepend('<div class="card1 col-md-3"><img src=\"'+image+' \" alt="UniversityImage" style="width:100%; height: 240px; width: 370;"><h3>'+name+'</h3><p class="title"><strong>Location:</strong> '+location+'</p><div style="margin: 24px 0;"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-certificate" data-toggle="tooltip" data-placement="top" title="Degree Programs"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Certified"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-calculator" data-toggle="tooltip" data-placement="top" title="Merit Calculator"></span></li></ul></div><p><button style="background-color: #3aafa9" onclick="unsubscribe(\'' + subKey + '\',\'' + currentUserID + '\',\'' + name + '\');">Unsubscribe</button></p></div>');
                   lineCounter++;
                });
              }); 
          });
        }  
  })
}

function unsubscribe(unsubKey, currentUserKey, uniName){
  firebase.database().ref('/Subscriptions/'+ currentUserKey +'/'+ unsubKey).remove();
  alert(uniName +' is unsubscribed...');
}

function getPendingSendRequests(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("socialNetwork");
        firebaseRef.child(currentUserID).child("pendings").child("sentRequests").on("value", snap => {   
            $('#userSendRequestDiv').html('');
            snap.forEach(function(snapshot) {
              var reqKey=snapshot.key;
              var userID=snapshot.child("userID").val();

              var firebaseRef1=firebase.database().ref().child("Users");
              firebaseRef1.child(userID).on("value", snap => { 

                  var userKey=snap.key;
                  var image=snap.child("userImageUrl").val();
                  var name=snap.child("username").val();
                  var bio=snap.child("bio").val();

                  $('#userSendRequestDiv').append('<div class="row"><div class="col-12"><div class="card"><div class="card-body"><div class="row"><div class="col-12 col-lg-8 col-md-6"><h3 class="card-text"><strong>Name: </strong> '+name+'</h3><div class="hr bg-gray-light offset-top-10"></div><h4 class="card-text"><strong>Bio: </strong> '+bio+'</h4><br><p><span class="btn button-primary" onclick="cancelMateRequest(\'' + reqKey + '\',\'' + currentUserID + '\',\'' + userID + '\');">Cancel Request</span></p></div><div class="col-12 col-lg-4 col-md-6 text-center"><img class="btn-md" src=\"'+image+' \" alt="" style="border-radius:50%; height: 230px; width: 200px;"></div></div></div></div></div></div>');
                });
            }) 
        });
      }
  });
}

function cancelMateRequest(key, currentUser, userID){
  firebase.database().ref('/socialNetwork/'+ currentUser +'/pendings/sentRequests/'+ key).remove();
  firebase.database().ref('/socialNetwork/'+ userID +'/pendings/mateRequests/'+ key).remove();
}

function getPendingMateRequests(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("socialNetwork");
        firebaseRef.child(currentUserID).child("pendings").child("mateRequests").on("value", snap => {   
            $('#userMateRequestDiv').html('');
            snap.forEach(function(snapshot) {
              var reqKey=snapshot.key;
              var userID=snapshot.child("userID").val();

              var firebaseRef1=firebase.database().ref().child("Users");
              firebaseRef1.child(userID).on("value", snap => {  
                  var userKey=snap.key;
                  var image=snap.child("userImageUrl").val();
                  var name=snap.child("username").val();
                  var bio=snap.child("bio").val();

                  $('#userMateRequestDiv').append('<div class="row"><div class="col-12"><div class="card"><div class="card-body"><div class="row"><div class="col-12 col-lg-8 col-md-6"><h3 class="card-text"><strong>Name: </strong> '+name+'</h3><div class="hr bg-gray-light offset-top-10"></div><h4 class="card-text"><strong>Bio: </strong> '+bio+'</h4><br><p><span class="btn button-primary" onclick="approveRequest(\'' + userID + '\',\'' + reqKey + '\',\'' + name + '\');">Approve</span> <span class="btn button-primary" onclick="deleteRequest(\'' + currentUserID + '\',\'' + userID + '\',\'' + reqKey + '\');">Decline</span></p></div><div class="col-12 col-lg-4 col-md-6 text-center"><img class="btn-md" src=\"'+image+' \" alt="" style="border-radius:50%; height: 230px; width: 200px;"></div></div></div></div></div></div>');
                });
            }) 
        });
      }
  });
}

function approveRequest(userID, reqkey, name){
  var currentUserID;
  var currentUsername
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("Users");
        firebaseRef.child(currentUserID).on("value", snap => {   
            currentUsername=snap.child("username").val();
        })
        alert(currentUsername);
        
        var materNode = {
                 'userID': userID,
                 'name': name,
                 };

        var matingNode = {
                 'userID': currentUserID,
                 'name': currentUsername,      
                } 

        var notificatioNode = {
                  'userID': currentUserID,
                  'name': name,
                  'type': "approved",
                  'seen': false,
                  }

        var keyRef = firebase.database().ref('socialNetwork').push().key;
        firebase.database().ref('socialNetwork/'+currentUserID+'/approved/maters/'+ keyRef).update(materNode);
        firebase.database().ref('socialNetwork/'+userID+'/approved/matings/'+ keyRef).update(matingNode); 
        
        firebase.database().ref('/socialNetwork/'+ currentUserID +'/pendings/mateRequests/'+ reqkey).remove();
        firebase.database().ref('/socialNetwork/'+ userID +'/pendings/sentRequests/'+ reqkey).remove();
      
        var keyRef1 = firebase.database().ref('Notifications').push().key;
        firebase.database().ref('Notifications/'+userID+'/'+ keyRef1).update(notificatioNode);
      }
    });
}

function deleteRequest(current, userID, key){
  firebase.database().ref('/socialNetwork/'+ current +'/pendings/mateRequests/'+ key).remove();
  firebase.database().ref('/socialNetwork/'+ userID +'/pendings/sentRequests/'+ key).remove();
}

function getMatersList(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("socialNetwork");
        firebaseRef.child(currentUserID).child("approved").child("maters").on("value", snap => {   
            $('#mattersDiv').html('');
            snap.forEach(function(snapshot) {
              var materKey=snapshot.key;
              var userID=snapshot.child("userID").val();

              var firebaseRef1=firebase.database().ref().child("Users");
              firebaseRef1.child(userID).on("value", snap => { 

                  var userKey=snap.key;
                  var image=snap.child("userImageUrl").val();
                  var name=snap.child("username").val();
                  var email=snap.child("email").val();
                  var bio=snap.child("bio").val();

                  $('#mattersDiv').append('<div class="row"><div class="col-12"><div class="card"><div class="card-body"><div class="row"><div class="col-12 col-lg-8 col-md-6"><h2 class="card-text"><strong>Name: </strong> '+name+'</h2><div class="hr bg-gray-light offset-top-10"></div> <p class="card-text"><span class="icon icon-xs mdi mdi-email">  '+email+'</span></p><h4 class="card-text"><strong>Bio: </strong> '+bio+'</h4><p><span class="btn button-primary" onclick="removeMater(\'' + currentUserID + '\',\'' + userID + '\',\'' + materKey + '\')">Remove</span> <span class="btn button-primary" onclick="blockMate(\'' + userID + '\',\'' + name + '\',\'' + materKey + '\');">Block</span></p></div><div class="col-12 col-lg-4 col-md-6 text-center"><img class="btn-md" src=\"'+image+' \" alt="" style="border-radius:50%; height: 230px; width: 200px;"></div></div></div></div></div></div>');
                });
            }) 
        });
      }
  });
}

function removeMater(current, userID, key){
  firebase.database().ref('/socialNetwork/'+ current +'/approved/maters/'+ key).remove();
  firebase.database().ref('/socialNetwork/'+ userID +'/approved/matings/'+ key).remove();
}

function getMatingsList(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("socialNetwork");
        firebaseRef.child(currentUserID).child("approved").child("matings").on("value", snap => {   
            $('#matingsListDiv').html('');
            snap.forEach(function(snapshot) {
              var matingKey=snapshot.key;
              var userID=snapshot.child("userID").val();

              var firebaseRef1=firebase.database().ref().child("Users");
              firebaseRef1.child(userID).on("value", snap => { 

                  var userKey=snap.key;
                  var image=snap.child("userImageUrl").val();
                  var name=snap.child("username").val();
                  var email=snap.child("email").val();
                  var bio=snap.child("bio").val();

                  $('#matingsListDiv').append('<div class="row"><div class="col-12"><div class="card"><div class="card-body"><div class="row"><div class="col-12 col-lg-8 col-md-6"><h2 class="card-text"><strong>Name: </strong> '+name+'</h2><div class="hr bg-gray-light offset-top-10"></div> <p class="card-text"><span class="icon icon-xs mdi mdi-email">  '+email+'</span></p><h4 class="card-text"><strong>Bio: </strong> '+bio+'</h4><p><span class="btn button-primary" onclick="removeMating(\'' + currentUserID + '\',\'' + userID + '\',\'' + matingKey + '\');">Unmate</span> <span class="btn button-primary"onclick="blockMate(\'' + userID + '\',\'' + name + '\',\'' + matingKey + '\');">Block</span></p></div><div class="col-12 col-lg-4 col-md-6 text-center"><img class="btn-md" src=\"'+image+' \" alt="" style="border-radius:50%; height: 230px; width: 200px;"></div></div></div></div></div></div>');
                });
            }) 
        });
      }
  });
}

function removeMating(current, userID, key){
  firebase.database().ref('/socialNetwork/'+ current +'/approved/matings/'+ key).remove();
  firebase.database().ref('/socialNetwork/'+ userID +'/approved/maters/'+ key).remove();
}

function getBlockedList(){
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("socialNetwork");
        firebaseRef.child(currentUserID).child("blocked").on("value", snap => {   
            $('#blockedListDiv').html('');
            snap.forEach(function(snapshot) {
              var blockKey=snapshot.key;
              var userID=snapshot.child("userID").val();

              var firebaseRef1=firebase.database().ref().child("Users");
              firebaseRef1.child(userID).on("value", snap => {
                  var userKey=snap.key;
                  var image=snap.child("userImageUrl").val();
                  var name=snap.child("username").val();

                  $('#blockedListDiv').append('<div class="container"><div class="row"><div class="col-12"><div class="card"><div class="card-body"><div class="row"><div class="col-12 col-lg-2 col-md-6 text-center"><img class="btn-md" src=\"'+image+' \" alt="" style="border-radius:50%; height: 120px; width: 110px;"></div><div class="col-12 col-lg-10 col-md-6"><div class="row"><div class="col-9"><br><h2 class="card-text"><strong>Name: </strong> '+name+'</h2></div><div class="col-3 text-center"><br><br><span class="btn button-primary" onclick="unblock(\'' + currentUserID + '\',\'' + blockKey + '\');">Unblock</span></div></div></div></div></div></div></div></div></div>');
                });
            }) 
        });
      }
  });
}

function blockMate(userID, name, blockKey){
  var currentUserID;
  var currentUsername;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        currentUserID = user.uid
        
        var firebaseRef=firebase.database().ref().child("Users");
        firebaseRef.child(currentUserID).on("value", snap => {   
            currentUsername=snap.child("username").val();
        })
        
        var blockNode = {
                 'userID': userID,
                 'name': name,
                 };

        var keyRef = firebase.database().ref('socialNetwork').push().key;
        firebase.database().ref('socialNetwork/'+currentUserID+'/blocked/'+ keyRef).update(blockNode); 
        
        firebase.database().ref('/socialNetwork/'+ currentUserID +'/approved/maters/'+ blockKey).remove();
        firebase.database().ref('/socialNetwork/'+ userID +'/approved/matings/'+ blockKey).remove();
      }
    });
}

function unblock(current, blockKey){
  firebase.database().ref('/socialNetwork/'+ current +'/blocked/'+ blockKey).remove();
}

function getNotifications(){
  var currentUserID;
  var counter=0;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      currentUserID = user.uid
      
      var type, seen, username;
      var firebaseRef=firebase.database().ref().child("Notifications");
      $('#notificationDiv').html('');
      firebaseRef.child(currentUserID).on("value", snap => {
        snap.forEach(function(snapshot) {
          type=snapshot.child('type').val();
          seen=snapshot.child('seen').val();
          username=snapshot.child('name').val();

          if(type=="send"){
            if(seen){
              $('#notificationDiv').prepend('<div class="alert alert-success" role="alert"><p><strong>'+username+'</strong> send you the mate request.</p></div> ');
            }
            else{
              counter++;
              $('#notificationDiv').prepend('<div class="alert alert-info" role="alert"><p><strong>'+username+'</strong> send you the mate request.</p></div> ');
            }
          }
          else{
            if(seen){
              $('#notificationDiv').prepend('<div class="alert alert-success" role="alert"><p><strong>'+username+'</strong> approved your the mate request.</p></div> ');
            }
            else{
              counter++;
              $('#notificationDiv').prepend('<div class="alert alert-info" role="alert"><p><strong>'+username+'</strong> approved your the mate request.</p></div> ');
            }
          }
          if(counter>0){
            $('#unseenNotifications').html(counter);
          }
          else{
            $('unseenNotifications').hide();
          }
        })
      });
    }
  });
  
} 

$(document).on('change', '#fileToUpload', function(event) {
    selectedFile = event.target.files[0];
    uploadImage();
  });
  var uploadImage= function() {
  var filename=selectedFile.name;
  var storageRef = firebase.storage().ref();
  var uploadTask = storageRef.child('/UserImages/'+filename).put(selectedFile);

  uploadTask.on('state_changed', function(snapshot){
   
   var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
   console.log('Upload is ' + progress + '% done');
   switch (snapshot.state) {
     case firebase.storage.TaskState.PAUSED: // or 'paused'
       console.log('Upload is paused');
       break;
     case firebase.storage.TaskState.RUNNING: // or 'running'
       console.log('Upload is running');
       break;
   }
  }, function(error) {
   // Handle unsuccessful uploads
  }, function() {
   // Handle successful uploads on complete
   // For instance, get the download URL: https://firebasestorage.googleapis.com/...
   uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
     changeImage(downloadURL)
   });
  });
}