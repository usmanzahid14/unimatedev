$(document).ready(function(){
populateBlogsDivs()
})
 
function populateBlogsDivs()
{
  var firebaseRef=firebase.database().ref().child('Blog');
   
  firebaseRef.orderByKey().on("value", snap => {
      //Get Fresh copy of blog data
      snap.forEach(function(snapshot) {
      
       var key=snapshot.key;
       var tempTitle=snapshot.child("title").val();
       var tempDis=snapshot.child("discription").val();
       var tempTag=snapshot.child("tag").val();
       var tempStatus=snapshot.child("status").val();
       var tempimage=snapshot.child("blogImageUrl").val();
       
       //append data into blog div
       $('#blogsPagePopulate').prepend('<div class="col-md-6 col-lg-5 col-xxl-3 border border-success" style="margin: 5px;"><article class="post-event"><div class="post-event-img-overlay"><a href="singleBlogPage.php?blogID='+key+'"><img class="img-responsive" src=\"'+tempimage+' \" width="700" height="455" alt=""></a></div><div class="unit unit-lg flex-column flex-xl-row"><div class="unit-left"><div class="post-event-meta text-center"><p class="d-inline-block d-xl-block">--</p><p class="d-inline-block d-xl-block">--</p><div class="h3 font-weight-bold d-inline-block d-xl-block">B</div><span class="font-weight-bold d-inline-block d-xl-block inset-left-10 inset-xl-left-0">--</span><span class="font-weight-bold d-inline-block d-xl-block inset-left-10 inset-xl-left-0">--</span></div></div><div class="unit-body"><div class="post-event-body text-xl-left"><h6><a href="singleBlogPage.php?blogID='+key+'">'+tempTitle+'</a></h6><div class="hr divider bg-madison hr-left-0"></div><ul class="list-inline list-inline-xs"><li><a href="blogs.php?tag='+tempTag+'"><span class="icon icon-xxs mdi mdi-tag text-middle"></span><span class="inset-left-10 text-dark text-middle">'+tempTag+'</span></a></li></ul></div></div></div></article></div>')
      })
    });
}  

function singleBlogDisplay(key){
    $('#blogid').html('');
    var search=key;
    var firebaseRef=firebase.database().ref().child('Blog');
    firebaseRef.child(search).on("value", snap => {
     
        var key=snap.key;
        var tempTitle=snap.child("title").val();
        var tempDis=snap.child("discription").val();
        var tempTag=snap.child("tag").val();
        var tempStatus=snap.child("status").val();
        var tempimage=snap.child("blogImageUrl").val();
        $('#blogid').prepend(' <div class="container"><div class="row row-50 justify-content-sm-center"><div class="col-md-6 text-left"><div class="inset-md-right-30"><img class="img-responsive d-inline-block" src=\"'+tempimage+' \" width="540" height="540" alt=""></div></div><div class="col-md-6 text-left"><h2 class="font-weight-bold">'+tempTitle+'</h2><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-20"><p>'+tempDis+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-20"><ul class="post-news-meta list list-inline list-inline-xl"><li class="pull-lg-right d-block offset-top-20 offset-lg-top-10"><div class="tags-list group group-sm"><a class="btn btn-xs button-primary font-italic" href="tagBlogPage.php?tag='+tempTag+'">'+tempTag+'</a></div></li></ul></div></div></div></div>');
 });
}

function displayTagBlogs(tag){
  var tagKey=tag;
  $('#tBlogsPagePopulate').html('');
  var firebaseRef=firebase.database().ref().child('Blog');
   
  firebaseRef.orderByChild("tag").equalTo(tagKey).on("child_added", function(snap) {
      
       var key=snap.key;
       var tempTitle=snap.child("title").val();
       var tempDis=snap.child("discription").val();
       var tempTag=snap.child("tag").val();
       var tempStatus=snap.child("status").val();
       var tempimage=snap.child("blogImageUrl").val();
       
       //append data into blog div
       $('#tBlogsPagePopulate').prepend('<div class="col-md-6 col-lg-5 col-xxl-3 border border-success" style="margin: 5px;"><article class="post-event"><div class="post-event-img-overlay"><a href="singleBlogPage.php?newsID='+key+'"><img class="img-responsive" src=\"'+tempimage+' \" width="700" height="455" alt=""></a></div><div class="unit unit-lg flex-column flex-xl-row"><div class="unit-left"><div class="post-event-meta text-center"><p class="d-inline-block d-xl-block">--</p><p class="d-inline-block d-xl-block">--</p><div class="h3 font-weight-bold d-inline-block d-xl-block">B</div><span class="font-weight-bold d-inline-block d-xl-block inset-left-10 inset-xl-left-0">--</span><span class="font-weight-bold d-inline-block d-xl-block inset-left-10 inset-xl-left-0">--</span></div></div><div class="unit-body"><div class="post-event-body text-xl-left"><h6><a href="singleBlogPage.php?blogID='+key+'">'+tempTitle+'</a></h6><div class="hr divider bg-madison hr-left-0"></div><ul class="list-inline list-inline-xs"><li><a href="blogs.php?tag='+tempTag+'"><span class="icon icon-xxs mdi mdi-tag text-middle"></span><span class="inset-left-10 text-dark text-middle">'+tempTag+'</span></a></li></ul></div></div></div></article></div>')
    });
}

function fetchComments(id){
    $('#displayComments').html('');
    
    var search=id;
    var firebaseRef=firebase.database().ref().child('Comments');
    firebaseRef.orderByChild("key").equalTo(search).on("child_added", function(snap) {
        var key=snap.key;
        var tempName=snap.child("userName").val();
        var tempComment=snap.child("comment").val();
        
        $('#displayComments').prepend('<div class="box-comment text-left box-comment-classic"><div class="unit flex-column flex-sm-row"><div class="unit-left"><img class="img-rounded box-comment-img" src="images/user.png" alt="" width="80" height="80"/></div><div class="unit-body"><div><p class="box-comment-title"><span class="h6 text-primary font-weight-bold">'+tempName+'</span></p></div><div class="box-comment-body offset-top-10"><p>'+tempComment+'</p></div></div></div></div><br><div class="text-subline"></div><br>');
 });
}

function saveComment(){

  var name=$('#name').val();
  var bKey=$('#bKey').val();
  var comment=$('#comment').val(); 

  var commentNode = {
          'userName': name,
          'comment' : comment,
          'key' : bKey,
  }

  var Ref = firebase.database().ref('Comments').push().key;
  firebase.database().ref('Comments/'+ Ref).update(commentNode);
}