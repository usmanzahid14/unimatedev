$(document).ready(function(){
populateNewsDivs()
})

function populateNewsDivs(){
  var firebaseRef=firebase.database().ref().child('News');
 
  firebaseRef.orderByKey().on("value", snap => {

      snap.forEach(function(snapshot) {
     
       var key=snapshot.key;
       var tempDate=snapshot.child("lastdate").val();
       var tempDis=snapshot.child("discription").val();
       var tempPostingDate=snapshot.child("postingTime").val();
       var tempStatus=snapshot.child("status").val();
       var tempTitle=snapshot.child("title").val();
       var image=snapshot.child("newsImageUrl").val();
      
       $('#populateNews').prepend('<div class="col-md-6 col-lg-4 border border-success" style="margin: 5px"><article class="post-news post-news-wide"><a href="singleNewsPage.php?newsID='+key+'"><img class="img-responsive" src=\"'+image+' \" width="700" height="455" alt=""></a><div class="post-news-body"><h6><a href="singleNewsPage.php?newsID='+key+'">'+tempTitle+'</a></h6><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-20"><p>'+tempDis+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="post-news-meta offset-top-20"><span class="icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span class="text-middle inset-left-10 font-italic text-black">'+tempPostingDate+'</span></div></div></article></div>');
      })
    });
}

function singleNews(key){

   var search= key;
   var firebaseRef=firebase.database().ref().child('News');
    firebaseRef.child(search).on("value", snap => {     
       var key=snap.key;
       var tempDate=snap.child("lastdate").val();
       var tempDis=snap.child("discription").val();
       var tempPostingDate=snap.child("postingTime").val();
       var tempStatus=snap.child("status").val();
       var tempTitle=snap.child("title").val();
       var image=snap.child("newsImageUrl").val();
        
       $('#newsId').prepend('<h2 class="font-weight-bold">'+tempTitle+'</h2><hr class="divider bg-madison divider-lg-0"><div class="offset-lg-top-47 offset-top-20"><ul class="post-news-meta list list-inline list-inline-xl"><li><span class="icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span class="text-middle inset-left-10 font-italic text-black">Posted On: '+tempPostingDate+'</span></li></ul></div><div class="offset-top-30"><img class="img-responsive" src=\"'+image+' \" width="770" height="500" alt=""><div class="offset-top-30"><h6 class="font-weight-bold">Details:</h6><div class="text-subline"></div><p>'+tempDis+'</p><h6 class="font-weight-bold">Last Date to Apply:</h6><div class="text-subline"></div><p> '+tempDate+'</p></div></div>');
 });

}