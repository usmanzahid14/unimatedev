 $(document).ready(function(){
populateEventsDivs()
})

function populateEventsDivs()
{
  var firebaseRef=firebase.database().ref().child('Workshops');
 
  firebaseRef.orderByKey().on("value", snap => {
    //Counter
      var i=1;
      //Get Fresh copy of workshop data
      snap.forEach(function(snapshot) {
     
       var key=snapshot.key;
       var tempDate=snapshot.child("date").val();
       var tempDis=snapshot.child("discription").val();
       var tempEnd=snapshot.child("endTime").val();
       var tempStart=snapshot.child("startTime").val();
       var tempFee=snapshot.child("fee").val();
       var tempStatus=snapshot.child("status").val();
       var tempTitle=snapshot.child("title").val();
       var tempVenue=snapshot.child("venue").val();
       var tempimage=snapshot.child("workshopImageUrl").val();
       
       $('#eventPopulate').prepend('<div class="col-md-6 col-lg-5 col-xxl-4 border border-success" style="margin: 5px"><article class="post-event"><div class="post-event-img-overlay"><img class="img-responsive" src=\"'+tempimage+' \" width="420" height="420" alt=""><div class="post-event-overlay context-dark"><div class="offset-top-20"><a class="btn button-default" href="singleEventPage.php?eventId='+key+'">More Details</a></div></div></div><div class="unit unit-lg flex-column flex-xl-row"><div class="unit-left"><div class="post-event-meta text-center"><div class="h3 font-weight-bold d-inline-block d-xl-block">'+tempDate+'</div><div class="text-center">--</div><p class="d-inline-block d-xl-block">'+tempStart+'</p><div class="text-center">--</div><span class="font-weight-bold d-inline-block d-xl-block inset-left-10 inset-xl-left-0">'+tempEnd+'</span></div></div><div class="unit-body"><div class="post-event-body text-xl-left"><h6><a href="singleEventView.php">'+tempTitle+'</a></h6><div class="hr divider bg-madison hr-left-0"></div><ul class="list-inline list-inline-xs"><li><span class="icon icon-xxs mdi mdi-map-marker text-middle"></span><span class="inset-left-10 text-dark text-middle">'+tempVenue+'</span></li></ul></div></div></div></article></div>')
       i++;
      })
    });
}

function singleEventDisplay(key){
  
    $('#singleEvent').html('');
    var searchEvent=key;
    var firebaseRef=firebase.database().ref().child('Workshops');
     firebaseRef.child(searchEvent).on("value", function(snap) {
    

           var key=snap.key;
           var tempDate=snap.child("date").val();
           var tempDis=snap.child("discription").val();
           var tempEnd=snap.child("endTime").val();
           var tempStart=snap.child("startTime").val();
           var tempFee=snap.child("fee").val();
           var tempStatus=snap.child("status").val();
           var tempTitle=snap.child("title").val();
           var tempVenue=snap.child("venue").val();
           var tempimage=snap.child("workshopImageUrl").val();
          

           $('#singleEvent').prepend('<div class="container"><div class="row row-50 justify-content-sm-center"><div class="col-md-6 text-left"><div class="inset-md-right-30"><img class="img-responsive d-inline-block" src=\"'+tempimage+' \ width="540" height="540" alt=""></div></div><div class="col-md-6 text-left"><h2 class="font-weight-bold">'+tempTitle+'</h2><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Discription</h6><div class="text-subline"></div><p>'+tempDis+'</p></div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">When is the workshop?</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>Details of the event are below:</p><ul class="list list-unstyled"><li><span class="text-black">Date:</span> <span>'+tempDate+'</span></li><li><span class="text-black">Time:</span> <span>'+tempStart+' - '+tempEnd+'</span></li><li><span class="text-black">Fee:</span><span>'+tempFee+'</span></li><li><span class="text-black">Location:</span><span>'+tempVenue+'</span></li></ul><div class="offset-top-20"></div><div class="offset-top-30"><a class="btn button-primary" href="eventsPage.php">Go Back</a></div></div></div></div>');

       });

     }