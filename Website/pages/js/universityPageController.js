$(document).ready(function(){
populateDivs()
getUniversitiesList1()
getUniversitiesList2()
}) 

function populateDivs(){
  var firebaseRef=firebase.database().ref().child('Universities');
 	firebaseRef.orderByKey().on("value", snap => {
 	//Counter
   	var i=1;
   	snap.forEach(function(snapshot) {
    
      var key=snapshot.key;
      var des=snapshot.child("discription").val();
      var email=snapshot.child("email").val();
      var rank=snapshot.child("rank").val();
      var name=snapshot.child("name").val();
      var phone=snapshot.child("phone").val();
      var location=snapshot.child("location").val();
      var sector=snapshot.child("sector").val();
      var image=snapshot.child("universityImageUrl").val();
      var isEntryTestInstitution=snapshot.child("isEntryTestInstitution").val();
      
      //append data into university table
      $('#populateDivs').prepend('<li class="card1 col-md-6 col-lg-4 view-animate fadeInRightSm delay-1" style="margin: 5px;"> <article class="post-news bg-default"><a href="singleUniversityView.php?uniID='+key+'"><img class="img-responsive" src=\"'+image+' \" width="370" height="240" alt=""></a> <div class="post-news-body-variant-1"> <div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h6><a href="singleUniversityView.php?uniID='+key+'">'+name+'</a></h6><div class="hr divider bg-madison hr-left-0"></div> <div class="offset-top-9"><p class="text-base">'+location+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-certificate" data-toggle="tooltip" data-placement="top" title="Degree Programs"><a href="proramsPage.php"></a></span></li> <li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Certified"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-calculator" data-toggle="tooltip" data-placement="top" title="Merit Calculator"></span></li> </ul> </div> </div> </article> </li> ');  i++;
   	})
    
    $('#populateDivs').paginate({
      // how many items per page
      perPage:                5,     
      // boolean: scroll to top of the container if a user clicks on a pagination link       
      autoScroll:             true,          
     
      // which elements to target
      scope:                  '',        
     
      // defines where the pagination will be displayed   
      paginatePosition:       ['bottom'],    
     
      // Pagination selectors
      containerTag:           'nav',
      paginationTag:          'ul',
      itemTag:                'li',
      linkTag:                'a',
     
      // Determines whether or not the plugin makes use of hash locations
      useHashLocation:        true,          
      // Triggered when a pagination link is clicked
      onPageClick:            function() {}  
   

    });
  });
}

function searchUni(){
  $('#populateDivs').html(' ');
  var searchKey=$('#searchUniversity').val();
  
  var firebaseRef=firebase.database().ref().child("Universities");
     firebaseRef.orderByChild("name").equalTo(searchKey).on("child_added", function(snap) {
        var key=snap.key;
        var des=snap.child("discription").val();
        var email=snap.child("email").val();
        var rank=snap.child("rank").val();
        var name=snap.child("name").val();
        var phone=snap.child("phone").val();
        var location=snap.child("location").val();
        var sector=snap.child("sector").val();
        var image=snap.child("universityImageUrl").val();
        var isEntryTestInstitution=snap.child("isEntryTestInstitution").val();

        $('#populateDivs').prepend(' <div class="col-md-6 col-lg-4 view-animate fadeInRightSm delay-1 border border-success" style="margin: 5px;"> <article class="post-news bg-default"><a href="singleUniversityView.php?uniID='+key+'"><img class="img-responsive" src=\"'+image+' \" width="370" height="240" alt=""></a> <div class="post-news-body-variant-1"> <div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h6><a href="singleUniversityView.php?uniID='+key+'">'+name+'</a></h6><div class="hr divider bg-madison hr-left-0"></div> <div class="offset-top-9"><p class="text-base">'+location+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-certificate" data-toggle="tooltip" data-placement="top" title="Degree Programs"><a href="proramsPage.php"></a></span></li> <li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Certified"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-calculator" data-toggle="tooltip" data-placement="top" title="Merit Calculator"></span></li> </ul> </div> </div> </article> </div> ');     
  });      
}

function getUniversitiesList1(){
  //getting data from DB to display in drop down menu
  var firebaseRef=firebase.database().ref().child("Universities");
  firebaseRef.orderByKey().on("value", snap => {
    snap.forEach(function(snapshot) {      
       var Key=snapshot.key;
       var uniName=snapshot.child("name").val();
       $('#uni1Name').append('<option value='+Key+'>'+uniName+'</option>');
      })
  });
}

function getUniversitiesList2(){
  //getting data from DB to display in drop down menu
  var firebaseRef=firebase.database().ref().child("Universities");
  firebaseRef.orderByKey().on("value", snap => {
  
    snap.forEach(function(snapshot) {      
       var Key=snapshot.key;
       var uniName=snapshot.child("name").val();
       $('#uni2Name').append('<option value='+Key+'>'+uniName+'</option>');
      })
  });
}

function compareTwoUni(){
  $('#compareUni1').html(' ');
  $('#compareUni2').html(' ');
  var uni1Key=$('#uni1Name').val();
  var uni2Key=$('#uni2Name').val();
  var firebaseRef=firebase.database().ref().child("Universities");
    firebaseRef.child(uni1Key).on("value", function(snap) {   
          var key=snap.key;
          var des=snap.child("discription").val();
          var email=snap.child("email").val();
          var rank=snap.child("rank").val();
          var name=snap.child("name").val();
          var phone=snap.child("phone").val();
          var location=snap.child("location").val();
          var sector=snap.child("sector").val();
          var image=snap.child("universityImageUrl").val();
          var isEntryTestInstitution=snap.child("isEntryTestInstitution").val();
          
          $('#compareUni1').prepend('<span class="card1 h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">University 1</span><div class="bg-transparent card-header pt-4 border-0"><h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15"><span class="h4 text-muted ml-2">'+name+'</span></h1></div><div class="card-body pt-0"><ul class="list-unstyled mb-4"><li>Sector: '+sector+'</li><hr class="divider bg-madison"><li>Rank: '+rank+'</li><hr class="divider bg-madison"><li>Location: '+location+'</li><hr class="divider bg-madison"><li>Discription: '+des+'</li><hr class="divider bg-madison"></ul><img class="img-responsive" src=\"'+image+' \" width="370" height="240" alt=""><br><a href="singleUniversityView.php?uniID='+key+'"><button type="button" class="btn btn-outline-secondary mb-3">Details</button></a></div>'); 
    });

    firebaseRef.child(uni2Key).on("value", function(snap) {   
          var key=snap.key;
          var des=snap.child("discription").val();
          var email=snap.child("email").val();
          var rank=snap.child("rank").val();
          var name=snap.child("name").val();
          var phone=snap.child("phone").val();
          var location=snap.child("location").val();
          var sector=snap.child("sector").val();
          var image=snap.child("universityImageUrl").val();
          var isEntryTestInstitution=snap.child("isEntryTestInstitution").val();
          
          $('#compareUni2').prepend('<span class="card1 h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">University 2</span><div class="bg-transparent card-header pt-4 border-0"><h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15"><span class="h4 text-muted ml-2">'+name+'</span></h1></div><div class="card-body pt-0"><ul class="list-unstyled mb-4"><li>Sector: '+sector+'</li><hr class="divider bg-madison"><li>Rank: '+rank+'</li><hr class="divider bg-madison"><li>Loaction: '+location+'</li><hr class="divider bg-madison"><li>Discription: '+des+'</li><hr class="divider bg-madison"></ul><a></a><img class="img-responsive" src=\"'+image+' \" width="370" height="240" alt=""><br><a href="singleUniversityView.php?uniID='+key+'"><button type="button" class="btn btn-outline-secondary mb-3">Details</button></a></div>'); 
    });
}

function singleUniDisplay(key){
    var searchUni = key;
    var firebaseRef=firebase.database().ref().child("Universities");
    firebaseRef.child(searchUni).on("value", snap => {   
      var key=snap.key;
      var des=snap.child("discription").val();
      var email=snap.child("email").val();
      var rank=snap.child("rank").val();
      var name=snap.child("name").val();
      var phone=snap.child("phone").val();
      var location=snap.child("location").val();
      var sector=snap.child("sector").val();
      var image=snap.child("universityImageUrl").val();
      var isEntryTestInstitution=snap.child("isEntryTestInstitution").val();
      var matricPercentage=snap.child("matricPercentage").val();
      var interPercentage=snap.child("interPercentage").val();
      var entryTestPercentage=snap.child("entryTestPercentage").val();

      var currentUserID;
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          currentUserID = user.uid

          var uniID, flag, subKey;
          var firebaseRef=firebase.database().ref().child("Subscriptions");
          firebaseRef.child(currentUserID).on("value", snap=> {
            snap.forEach(function(snapshot){
              uniID=snapshot.child("universityID").val();

              if(uniID==searchUni){
                flag = true;
                subKey=snapshot.key;
              }
            });
              
            if(flag){
              $('#singleUni').html('');
              $('#singleUni').prepend('<div class="row row-50 justify-content-sm-center"><div class="col-md-6 text-left"><div class="inset-md-right-30"><img class="img-responsive d-inline-block" src=\"'+image+' \" width="540" height="540" alt=""></div></div><div class="col-md-6 text-left"><h2 class="font-weight-bold">'+name+'</h2><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-30 offset-lg-top-60"><input type="hidden" name="universityKey" value="variable"><p>Sector: '+sector+'</p><p>Rank: '+rank+'</p></div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Contact Info</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>Details of the contact are below:</p><ul class="list list-unstyled"><li><span class="text-black mdi mdi-phone" data-toggle="tooltip" data-placement="top" title="Contact"></span> <span>'+phone+'</span></li><li><span class="text-black mdi mdi-email" data-toggle="tooltip" data-placement="top" title="Email"></span> <span>'+email+'</span></li><li><span class="text-black mdi mdi-map-marker" data-toggle="tooltip" data-placement="top" title="Location"></span> <span>'+location+'</span></li></ul></div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Discription/Details</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>'+des+'</p><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Merit/Aggregate Criteria</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>Matric Percentage: '+matricPercentage+'%</p><p>Intermediate Percentage: '+interPercentage+'%</p><p>Entry Test Percentage: '+entryTestPercentage+'%</p></div></div></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><button class="btn btn-ellipse button-primary" onclick="unsubscribe(\'' + currentUserID + '\',\'' + subKey + '\',\'' + searchUni + '\');">Unubscribe</button></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="programPageOnClickingAUniversity.php?UID='+key+'">Programs</a></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="singleScholarshipPage.php?UID='+key+'">Scholarships</a></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="meritCalculator.php?meritUID='+key+'">Merit Calculator</a></div><div class="container"><h6 class="font-weight-bold">Give it a review</h6><div class="text-subline"></div><br><a href="ratingPage.php?value='+1+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+2+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+3+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+4+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+5+'& key='+key+'"><span class="fa fa-star checked"></span></a><br><br><div class="text-subline"></div></div></div>'); 
            }
            else{
              $('#singleUni').html('');
              $('#singleUni').prepend('<div class="row row-50 justify-content-sm-center"><div class="col-md-6 text-left"><div class="inset-md-right-30"><img class="img-responsive d-inline-block" src=\"'+image+' \" width="540" height="540" alt=""></div></div><div class="col-md-6 text-left"><h2 class="font-weight-bold">'+name+'</h2><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-30 offset-lg-top-60"><input type="hidden" name="universityKey" value="variable"><p>Sector: '+sector+'</p><p>Rank: '+rank+'</p></div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Contact Info</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>Details of the contact are below:</p><ul class="list list-unstyled"><li><span class="text-black mdi mdi-phone" data-toggle="tooltip" data-placement="top" title="Contact"></span> <span>'+phone+'</span></li><li><span class="text-black mdi mdi-email" data-toggle="tooltip" data-placement="top" title="Email"></span> <span>'+email+'</span></li><li><span class="text-black mdi mdi-map-marker" data-toggle="tooltip" data-placement="top" title="Location"></span> <span>'+location+'</span></li></ul></div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Discription/Details</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>'+des+'</p><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Merit/Aggregate Criteria</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>Matric Percentage: '+matricPercentage+'%</p><p>Intermediate Percentage: '+interPercentage+'%</p><p>Entry Test Percentage: '+entryTestPercentage+'%</p></div></div></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><button class="btn btn-ellipse button-primary" onclick="subscribe(\'' + key + '\',\'' + searchUni + '\');">Subscribe</button></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="programPageOnClickingAUniversity.php?UID='+key+'">Programs</a></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="singleScholarshipPage.php?UID='+key+'">Scholarships</a></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="meritCalculator.php?meritUID='+key+'">Merit Calculator</a></div><div class="container"><h6 class="font-weight-bold">Give it a review</h6><div class="text-subline"></div><br><a href="ratingPage.php?value='+1+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+2+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+3+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+4+'& key='+key+'"><span class="fa fa-star checked"></span></a> <a href="ratingPage.php?value='+5+'& key='+key+'"><span class="fa fa-star checked"></span></a><br><br><div class="text-subline"></div></div></div>'); 
            }
          });
        }
      });       
    });
}

function subscribe(key,searchUni) {
  var uniKey = key;
  var uniName;

  var firebaseRef=firebase.database().ref().child("Universities");
  firebaseRef.child(uniKey).on("value", snap => {    
     uniName=snap.child("name").val();
    })

  //current user
  var currentUserID;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
            currentUserID = user.uid
 
            var subscribeNode = {
                      'universityName': uniName,
                      'universityID': uniKey,                 
                      }

            var keyRef = firebase.database().ref('Subscriptions').push().key;
            firebase.database().ref('Subscriptions/'+currentUserID+'/'+keyRef).update(subscribeNode);
          } 
    });
  singleUniDisplay(searchUni);
}

function unsubscribe(currentUserKey,unsubKey,uniKey){
  firebase.database().ref('/Subscriptions/'+ currentUserKey +'/'+ unsubKey).remove();
  singleUniDisplay(uniKey);
}

function addRating(value, key){
  var uniName= null;
  var uniKey=key;
  var rating=value;
  var firebaseRef=firebase.database().ref().child("Universities");
  firebaseRef.child(uniKey).on("value", snap => {    
    uniName=snap.child("name").val();
     
    var ratingNode = {
                 'universityName': uniName,
                 'UID': uniKey,
                 'rating': rating,
               }
    var keyRef = firebase.database().ref('Review').push().key;
    firebase.database().ref('Review/'+keyRef).update(ratingNode);

    $('#rating').prepend('<h2>Thank-You for rating '+uniName+'</h2><div class="text-subline"></div><br><span class="fa fa-star checked bg-default"></span><span class="fa fa-star checked bg-default"></span><span class="fa fa-star checked bg-default"></span><span class="fa fa-star checked bg-default"></span><span class="fa fa-star checked bg-default"></span><br><br><div class="text-subline"></div><div class="offset-top-35 offset-xl-top-70 view-animate fadeInUpSmall"><a class="btn btn-ellipse button-primary" href="singleUniversityView.php?uniID='+uniKey+'">Go Back</a></div>');
  });  
}

function singleScholarship(key){
  var image= null;
  var search=key;
  var firebaseRef=firebase.database().ref().child("Universities");
  firebaseRef.child(search).on("value", snap => {

        image=snap.child("universityImageUrl").val();
       });

  var firebaseRef2=firebase.database().ref().child("Scholarships");
  firebaseRef2.orderByChild("UID").equalTo(search).on("child_added", function(snap) {
      
      var key=snap.key;
      var sponsNam=snap.child("uName").val();
      var scolType=snap.child("type").val();
      var dis=snap.child("discription").val();

      $('#scholarshipI').prepend('<div class="row row-50 justify-content-sm-center"><div class="col-md-6 text-left"><div class="inset-md-right-30"><img class="img-responsive d-inline-block"src=\"'+image+' \"width="540"height="540"alt=""></div></div><div class="col-md-6 text-left"><h2 class="font-weight-bold">'+sponsNam+'</h2><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-30 offset-lg-top-60"><input type="hidden" name="universityKey" value="variable"></div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Scholarship Types</h6><div class="text-subline"></div>'+scolType+'</div><div class="offset-top-30 offset-lg-top-60"><h6 class="font-weight-bold">Discription</h6><div class="text-subline"></div></div><div class="offset-top-17"><p>'+dis+'</p></div></div></div>');


  });
} 