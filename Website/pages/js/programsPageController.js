$(document).ready(function(){
	getUniversitiesList();
	showPrograms();
	getProgramsListForCompare();
	getExchangeProgramsList();
	showExchangePrograms();
})   

function getUniversitiesList(){

	//getting data from DB to display in drop down menu
	var firebaseRef=firebase.database().ref().child("Universities");
	firebaseRef.orderByKey().on("value", snap => {
	
		snap.forEach(function(snapshot) {		   
		   var Key=snapshot.key;
		   var uniName=snapshot.child("name").val();
		   $('#UniversityName').append('<option value='+Key+'>'+uniName+'</option>');
	  	})
	});
}

function getExchangeProgramsList(){
	var firebaseRef=firebase.database().ref().child("Universities");
	firebaseRef.orderByKey().on("value", snap => {
	
		snap.forEach(function(snapshot) {		   
		   var Key=snapshot.key;
		   var uniName=snapshot.child("name").val();
		   $('#exchangeProUni').append('<option value='+Key+'>'+uniName+'</option>');
	  	})
	});
}

function searchExPrograms(){
	var uniKey =$('#exchangeProUni').val();
	
	var firebaseRef=firebase.database().ref().child("Exchangeprograms");
	firebaseRef.orderByChild("universityID").equalTo(uniKey).on("value", function(snap) {
		snap.forEach(function(snapshot) {		   
		   var pkey=snapshot.key;
		   var pName=snapshot.child("programName").val();
		   var pUniName=snapshot.child("universityName").val();
		   var pDis=snapshot.child("discription").val();
		   var pDuration=snapshot.child("duration").val();
		   var pFee=snapshot.child("charges").val();

		  $('#displayExPrograms').prepend('<div class="card1 col-md-6 col-lg-4 view-animate fadeInRightSm delay-1" style="margin: 5px"><article class="post-news bg-default"><div class="post-news-body-variant-1"><div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h3 class="font-weight-bold">'+pName+'</h3><div class="hr divider bg-madison hr-left-0"></div><div class="card" style="width: 18rem;"><ul class="list-group list-group-flush"><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-clock" data-toggle="tooltip" data-placement="top" title="Degree Duration"></span> Duration: '+pDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-coin" data-toggle="tooltip" data-placement="top" title="Degree Fee"></span> Fee: '+pFee+'</li><li class="list-group-item"><span>Discription</span> '+pDis+'</li></ul></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><p class="text-base">'+pUniName+'</p></div><div class="hr divider bg-madison hr-left-0"></div></div></article></div>');
	  	})    
    });
}

function showExchangePrograms(){
	var firebaseRef=firebase.database().ref().child('Exchangeprograms');
 
 	firebaseRef.orderByKey().on("value", snap => {
	  	snap.forEach(function(snapshot) {
	   
		   var pkey=snapshot.key;
		   var pName=snapshot.child("programName").val();
		   var pUniName=snapshot.child("universityName").val();
		   var pDis=snapshot.child("discription").val();
		   var pDuration=snapshot.child("duration").val();
		   var pFee=snapshot.child("charges").val();

		  $('#displayExPrograms').prepend('<div class="card1 col-md-6 col-lg-4 view-animate fadeInRightSm delay-1" style="margin: 5px"><article class="post-news bg-default"><div class="post-news-body-variant-1"><div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h3 class="font-weight-bold">'+pName+'</h3><div class="hr divider bg-madison hr-left-0"></div><div class="card" style="width: 18rem;"><ul class="list-group list-group-flush"><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-clock" data-toggle="tooltip" data-placement="top" title="Degree Duration"></span> Duration: '+pDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-coin" data-toggle="tooltip" data-placement="top" title="Degree Fee"></span> Fee: '+pFee+'</li><li class="list-group-item"><span>Discription</span> '+pDis+'</li></ul></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><p class="text-base">'+pUniName+'</p></div><div class="hr divider bg-madison hr-left-0"></div></div></article></div>');
	  	})    
    });
}


function getProgramsListByUni(){
	  $('#programName').html(' ');
      var key=$('#UniversityName').val();
      var firebaseRef=firebase.database().ref().child("Program");
      firebaseRef.orderByChild("UID").equalTo(key).on("child_added", function(snap) {
		   var programKey=snap.key;
		    var proName=snap.child("programName").val();
		    $('#programName').append('<option value='+programKey+'>'+proName+'</option>');		    
    });
}

function getProListByCatogery(){
	  $('#programName').html(' ');
      var cat=$('#programCatogery').val();
      var firebaseRef=firebase.database().ref().child("Program");
      firebaseRef.orderByChild("Catogery").equalTo(cat).on("value", function(snap) {
      	snap.forEach(function(snapshot) {		   
		   	var programKey=snapshot.key;
		    var proName=snapshot.child("programName").val();
		    $('#programName').append('<option value='+programKey+'>'+proName+'</option>');
	  	})		   
		    
    });
}


function searchPrograms(){
	$('#displayPrograms').html(' ');
	var uniKey =$('#UniversityName').val();
	var catKey=$('#programCatogery').val();
	var proKey=$('#programName').val();

	var firebaseRef=firebase.database().ref().child("Program");
	firebaseRef.orderByChild("UID").equalTo(uniKey).on("value", function(snap) {
		snap.forEach(function(snapshot) {		   
		   var pkey=snapshot.key;
		   var pName=snapshot.child("programName").val();
		   var pUniName=snapshot.child("uName").val();
		   var pUniId=snapshot.child("UID").val();
		   var pDuration=snapshot.child("duration").val();
		   var pFee=snapshot.child("fee").val();
		   var pFeeDuration=snapshot.child("feeDuration").val();
		   var pOpenMerit=snapshot.child("openMerit").val();
		   var pDepartmentName=snapshot.child("departmentName").val();
		   var pScholarship=snapshot.child("scholarship").val();
		   var pCatogery=snapshot.child("Catogery").val();
		   var pLast=snapshot.child("lastYearMerit").val();

		    $('#displayPrograms').prepend('<div class="card1 col-md-6 col-lg-4 view-animate fadeInRightSm delay-1" style="margin: 5px"><article class="post-news bg-default"><div class="post-news-body-variant-1"><div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h3 class="font-weight-bold">'+pName+'</h3><div class="hr divider bg-madison hr-left-0"></div><div class="card" style="width: 18rem;"><ul class="list-group list-group-flush"><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-school" data-toggle="tooltip" data-placement="top" title="Catogery"></span> Catogery: '+pCatogery+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-clock" data-toggle="tooltip" data-placement="top" title="Degree Duration"></span> Duration: '+pDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-coin" data-toggle="tooltip" data-placement="top" title="Degree Fee"></span> Fee: '+pFee+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-calendar" data-toggle="tooltip" data-placement="top" title="Fee Pattern"></span>Fee Pattern: '+pFeeDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span> Merit Based: '+pOpenMerit+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span> Scholarship: '+pScholarship+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Last Year Merit"></span> Last Year Merit: '+pLast+'</li></ul></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><input type="hidden" id="'+pUniId+'"><p class="text-base">'+pUniName+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span></li></ul></div></div></article></div>');
	  	})    
    });
}

function showPrograms()
{
	var firebaseRef=firebase.database().ref().child('Program');
 
 	firebaseRef.orderByKey().on("value", snap => {
	  	snap.forEach(function(snapshot) {
	   
		   var key=snapshot.key;
		   var tempProgramName=snapshot.child("programName").val();
		   var tempUniversityName=snapshot.child("uName").val();
		   var tempUniversityId=snapshot.child("UID").val();
		   var tempDuration=snapshot.child("duration").val();
		   var tempFee=snapshot.child("fee").val();
		   var tempFeeDuration=snapshot.child("feeDuration").val();
		   var tempOpenMerit=snapshot.child("openMerit").val();
		   var tempDepartmentName=snapshot.child("departmentName").val();
		   var tempScholarship=snapshot.child("scholarship").val();
		   var tempCatogery=snapshot.child("Catogery").val();
		   var tempLast=snapshot.child("lastYearMerit").val();
		   
		   //append data into program divs
		   $('#displayPrograms').prepend('<div class="card1 col-md-6 col-lg-4 view-animate fadeInRightSm delay-1" style="margin: 5px"><article class="post-news bg-default"><div class="post-news-body-variant-1"><div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h3 class="font-weight-bold">'+tempProgramName+'</h3><div class="hr divider bg-madison hr-left-0"></div><div class="card" style="width: 18rem;"><ul class="list-group list-group-flush"><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-school" data-toggle="tooltip" data-placement="top" title="Catogery"></span> Catogery: '+tempCatogery+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-clock" data-toggle="tooltip" data-placement="top" title="Degree Duration"></span> Duration: '+tempDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-coin" data-toggle="tooltip" data-placement="top" title="Degree Fee"></span> Fee: '+tempFee+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-calendar" data-toggle="tooltip" data-placement="top" title="Fee Pattern"></span>Fee Pattern: '+tempFeeDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span> Merit Based: '+tempOpenMerit+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span> Scholarship: '+tempScholarship+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Last Year Merit"></span> Last Year Merit: '+tempLast+'</li></ul></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><input type="hidden" id="'+tempUniversityId+'"><p class="text-base">'+tempUniversityName+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span></li></ul></div></div></article></div>');
	  	})
    });
}


function getProgramsListForCompare(){

  //getting data from DB to display in drop down menu
	var firebaseRef=firebase.database().ref().child("Program");
	firebaseRef.orderByKey().on("value", snap => {
	
		snap.forEach(function(snapshot) {		   
		   var key=snapshot.key;
		   var proName=snapshot.child("programName").val();
		   $('#programSearch').append('<option value='+proName+'>'+proName+'</option>');
	  	})
	});
}

function comparePrograms(){
  $('#compareBody').html(' ');
  var searchKey=$('#programSearch').val();
  var firebaseRef=firebase.database().ref().child("Program");
    firebaseRef.orderByChild("programName").equalTo(searchKey).on("value", function(snap) {
      	snap.forEach(function(snapshot) {
  		  var key=snap.key;
	      var tempProName=snapshot.child("programName").val();
	      var tempUniName=snapshot.child("uName").val();
	      var tempUniId=snapshot.child("UID").val();
	      var tempDuration=snapshot.child("duration").val();
	      var tempFee=snapshot.child("fee").val();
	      var tempFeeDuration=snapshot.child("feeDuration").val();
	      var tempOpenMerit=snapshot.child("openMerit").val();
	      var tempScholarship=snapshot.child("scholarship").val();
	      var tempCatogery=snapshot.child("Catogery").val();
	      var tempLastYear=snapshot.child("lastYearMerit").val();
	      
	      var sum=parseInt(0);
	      var counter=parseInt(0);
	      var avg =parseInt(0);
	      var tempAvg =parseInt(0);
  		  var firebaseRef1=firebase.database().ref().child("Review");
  		  firebaseRef1.orderByChild("UID").equalTo(tempUniId).on("value", function(snap) {
      			snap.forEach(function(snapshot) {
      				var tempRate=snapshot.child("rating").val();
      				var tempuni=snapshot.child("universityName").val();
      				sum = parseInt(sum) + parseInt(tempRate);
      				counter++
      				})
      		});
  		  avg = parseInt(sum)/parseInt(counter);

          $('#suggestion').html('');
          $('#compareBody').prepend('<ul class="row"><li class="legend">'+tempUniName+'</li><li>'+tempProName+'</li><li>'+tempDuration+'</li><li>'+tempFee+'</li><li>'+tempFeeDuration+'</li><li>'+tempOpenMerit+'</li><li>'+tempScholarship+'</li><li>'+tempLastYear+'</li><li>'+tempCatogery+'</li><li>'+avg+'</li></ul>');   	
	  	  tempAvg = Math.max(avg, tempAvg)
	  	  $('#suggestion').prepend(''+tempUniName+'');
	  	})		           
  }); 
}

function populateUniDropDownBySelectedUni(key){
	var Key = key;
	var firebaseRef=firebase.database().ref().child("Program");
	firebaseRef.orderByChild("UID").equalTo(Key).on("child_added", function(snap) {

	   	   $('#universityName').html('');
		   var key=snap.key;
		   var pName=snap.child("programName").val();
		   var pUniversityName=snap.child("uName").val();
		   var pUniversityId=snap.child("UID").val();
		   var pDuration=snap.child("duration").val();
		   var pFee=snap.child("fee").val();
		   var pFeeDuration=snap.child("feeDuration").val();
		   var pOpenMerit=snap.child("openMerit").val();
		   var pDepartmentName=snap.child("departmentName").val();
		   var pScholarship=snap.child("scholarship").val();
		   var pCatogery=snap.child("Catogery").val();
		   var pLast=snap.child("lastYearMerit").val();
		   
		   $('#universityName').prepend('<h3 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04">Here are the programs of "'+pUniversityName+'" and their details.</h3>');
		   $('#specificProgramOfUni').prepend('<div class="col-md-6 col-lg-4 view-animate fadeInRightSm delay-1 border border-success" style="margin: 5px"><article class="post-news bg-default"><div class="post-news-body-variant-1"><div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h3 class="font-weight-bold">'+pName+'</h3><div class="hr divider bg-madison hr-left-0"></div><div class="card" style="width: 18rem;"><ul class="list-group list-group-flush"><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-school" data-toggle="tooltip" data-placement="top" title="Catogery"></span>'+pCatogery+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-clock" data-toggle="tooltip" data-placement="top" title="Degree Duration"></span>'+pDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-coin" data-toggle="tooltip" data-placement="top" title="Degree Fee"></span>'+pFee+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-calendar" data-toggle="tooltip" data-placement="top" title="Fee Pattern"></span>'+pFeeDuration+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span>'+pOpenMerit+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span>'+pScholarship+'</li><li class="list-group-item"><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Last Year Merit"></span> Last Year Merit: '+pLast+'</li></ul></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><input type="hidden" id="'+pUniversityId+'"><p class="text-base">'+pUniversityName+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-currency-usd" data-toggle="tooltip" data-placement="top" title="Scholarships"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Merit"></span></li></ul></div></div></article></div>');
	  	
    });
}