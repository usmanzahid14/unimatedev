$(document).ready(function(){
populateUniversityDivs()
populateBlogsDivs()
populateNewsDivs()
})

function populateUniversityDivs(){
  var firebaseRef=firebase.database().ref().child('Universities');
 	firebaseRef.orderByKey().limitToFirst(3).on("value", snap => {
 	  //Counter
 	  var i=1;
 	  snap.forEach(function(snapshot) {
  
        var key=snapshot.key;
        var des=snapshot.child("discription").val();
        var email=snapshot.child("email").val();
        var rank=snapshot.child("rank").val();
        var name=snapshot.child("name").val();
        var phone=snapshot.child("phone").val();
        var location=snapshot.child("location").val();
        var sector=snapshot.child("sector").val();
        var image=snapshot.child("universityImageUrl").val();
        var isEntryTestInstitution=snapshot.child("isEntryTestInstitution").val();
        //append data into university div
        $('#homeUniPopulate').prepend('<div class="card1 col-md-6 col-lg-4 view-animate fadeInRightSm delay-1" style="margin: 5px;"> <article class="post-news bg-default"><a href="singleUniversityView.php"><img class="img-responsive" src=\"'+image+' \" width="370" height="240" alt=""></a> <div class="post-news-body-variant-1"> <div class="post-news-meta"><time class="text-middle font-italic" datetime="2019"></time></div><h6><a href="singleUniversityView.php">'+name+'</a></h6><div class="hr divider bg-madison hr-left-0"></div> <div class="offset-top-9"><p class="text-base">'+location+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-9"><ul class="list-inline list-unstyled list-inline-primary"><li><span class="text-hover-custom icon icon-xxs mdi mdi-certificate" data-toggle="tooltip" data-placement="top" title="Degree Programs"><a href="proramsPage.php"></a></span></li> <li><span class="text-hover-custom icon icon-xxs mdi mdi-star" data-toggle="tooltip" data-placement="top" title="Certified"></span></li><li><span class="text-hover-custom icon icon-xxs mdi mdi-calculator" data-toggle="tooltip" data-placement="top" title="Merit Calculator"></span></li> </ul> </div> </div> </article> </div> ');
        i++;
       })
  });
}

function populateBlogsDivs(){
  var firebaseRef=firebase.database().ref().child('Blog');
  //Counter
  var i=1;
  firebaseRef.orderByKey().limitToFirst(3).on("value", snap => {
      //Get Fresh copy of blog data
      snap.forEach(function(snapshot) {
     
       var key=snapshot.key;
       var tempTitle=snapshot.child("title").val();
       var tempDis=snapshot.child("discription").val();
       var tempTag=snapshot.child("tag").val();
       var tempStatus=snapshot.child("status").val();
       var tempimage=snapshot.child("blogImageUrl").val();
       
       //append data into blog div
       $('#homeBlogsPopulate').prepend('<div class="card2 col-md-6 col-lg-5 col-xxl-3" style="margin: 5px;"><article class="post-event"><div class="post-event-img-overlay"><img class="img-responsive" src=\"'+tempimage+' \" width="420" height="420" href="singleBlog.php" alt=""></div><div class="unit unit-lg flex-column flex-xl-row"><div class="unit-left"><div class="post-event-meta text-center"><p class="d-inline-block d-xl-block">--</p><p class="d-inline-block d-xl-block">--</p><div class="h3 font-weight-bold d-inline-block d-xl-block">B</div><span class="font-weight-bold d-inline-block d-xl-block inset-left-10 inset-xl-left-0">--</span><span class="font-weight-bold d-inline-block d-xl-block inset-left-10 inset-xl-left-0">--</span></div></div><div class="unit-body"><div class="post-event-body text-xl-left"><h6><a href="singleBlog.php">'+tempTitle+'</a></h6><div class="hr divider bg-madison hr-left-0"></div><ul class="list-inline list-inline-xs"><li><span class="icon icon-xxs mdi mdi-tag text-middle" style="color: #2b7a78"></span><span class="inset-left-10 text-dark text-middle">'+tempTag+'</span></li></ul></div></div></div></article></div>')
       i++;
      })
    });
}

function populateNewsDivs(){
  var firebaseRef=firebase.database().ref().child('News');
  firebaseRef.orderByKey().limitToFirst(3).on("value", snap => {
    //Counter
      var i=1;

      snap.forEach(function(snapshot) {
     
       var key=snapshot.key;
       var tempDate=snapshot.child("lastdate").val();
       var tempDis=snapshot.child("discription").val();
       var tempPostingDate=snapshot.child("postingTime").val();
       var tempStatus=snapshot.child("status").val();
       var tempTitle=snapshot.child("title").val();
       var image=snapshot.child("newsImageUrl").val();
        
       $('#homeNewsDivs').prepend('<div class="card2 col-md-6 col-lg-4" style="margin: 5px"><article class="post-news post-news-wide"><a href="singleNewsPage.php"><img class="img-responsive" src=\"'+image+' \" width="700" height="455" alt=""></a><div class="post-news-body"><h6><a href="singleNewsPage.php">'+tempTitle+'</a></h6><div class="hr divider bg-madison hr-left-0"></div><div class="offset-top-20"><p>'+tempDis+'</p></div><div class="hr divider bg-madison hr-left-0"></div><div class="post-news-meta offset-top-20"><span class="icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span class="text-middle inset-left-10 font-italic text-black">'+tempPostingDate+'</span></div></div></article></div>');
       i++;
      })
    });
}