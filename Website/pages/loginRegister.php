<!DOCTYPE html> 
<html class="wide wow-animation scrollTo" lang="en">
<title>Login/Register</title>
    <!-- Include Header Here-->
    <?php include '../Includes/header.php';?>
    <!--End-->
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <!-- Include Header Here-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!--End-->
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Login/Register</h1>
        </div>
      </section>
      <!--Section Login Register-->
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="row justify-content-sm-center section-34">
            <div class="col-sm-8 col-md-6 col-lg-5">
              <h2 class="font-weight-bold">Sign In or Sign Up</h2>
              <hr class="divider bg-madison">
              <div class="offset-sm-top-45 text-center">
                <!--Bootstrap tabs-->
                <div class="tabs-custom tabs-horizontal tabs-line" id="tabs-1">
                  <!--Nav tabs-->
                  <ul class="nav nav-tabs">
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-login-1" data-toggle="tab">Login</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-login-2" data-toggle="tab">Registration</a></li>
                  </ul>
                  <!--Tab panes-->
                  <div class="tab-content">
                    <div class="tab-pane fade" id="tabs-login-1">
                      <!-- RD Mailform-->
                      <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" >
                        <div class="form-wrap">
                          <label class="form-label form-label-outside" for="loginUsername">E-mail:</label>
                          <input class="form-input bg-default" id="loginUsername" type="text" name="loginUsername" data-constraints="@Required">
                        </div>
                        <div class="form-wrap offset-top-15">
                          <label class="form-label form-label-outside" for="loginPassword">Password:</label>
                          <input class="form-input bg-default" id="loginPassword" type="password" name="loginPassword" data-constraints="@Required">
                        </div>
                        <div class="offset-top-20">
                          <a href="forgotPassword.php"><span>Forgot Password?</span></a>
                        </div>
                        <div class="offset-top-20">
                          <button class="btn button-primary" type="submit" onclick="signIn();">Login</button>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane fade" id="tabs-login-2">
                      <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" >
                        <div class="form-wrap">
                          <label class="form-label form-label-outside" for="registerUsername">Name:</label>
                          <input class="form-input bg-default" id="registerUsername" type="text" name="registerUsername" data-constraints="@Required">
                        </div>
                        <div class="form-wrap offset-top-15">
                          <label class="form-label form-label-outside" for="registerEmail">Email:</label>
                          <input class="form-input bg-default" id="registerEmail" type="text" name="registerEmail" data-constraints="@Required @Email">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside" for="registerContact">Contact:</label>
                          <input class="form-input bg-default" id="registerContact" type="text" name="registerContact" data-constraints="@Required">
                        </div>
                        <div class="form-wrap offset-top-15">
                          <label class="form-label form-label-outside" for="registerPassword">Password:</label>
                          <input class="form-input bg-default" id="registerPassword" type="password" name="registerPassword" data-constraints="@Required">
                        </div>
                        <div class="offset-top-20">
                          <button class="btn button-primary" type="submit" onclick="signUp();">Register</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/loginRegisterController.js"></script>
  </body>
  <!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>