 <!DOCTYPE html>   
<html class="wide wow-animation scrollTo" lang="en">
<title>Universities</title>
<?php include '../Includes/header.php';?>
  <link rel="stylesheet" href="css/jquery.paginate.css">
 
<style>
  .card1 {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
      max-width: 300px;
      margin: auto;
      font-family: arial;
      }
</style>
 
 
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Universities</h1>
        </div>
      </section>
      <!-- vertical link Tabs-->
      <section class="section section-xl bg-default">
        <div class="container" style="margin: -100px 0px 0px 245px;">
          <div class="offset-top-35 offset-lg-top-60 text-white view-animate fadeInUpSmall delay-06" style="font-size: 17px">Our Featured Universities are approved and certified. Find the university of your desire.</div>
          <section class="section section-xl bg-default" style="margin: -64px;">
            <div class="container">
              <div class="row justify-content-sm-center">
                <div class="col-md-10">
                  <!-- RD Search Form-->
                  <div class="form-wrap row text-center">
                    <div class="col-7">
                      <input class="form-input" type="text" placeholder="Search.." name="search" id="searchUniversity" autocomplete="off">
                    </div>
                    <div class="col-5">
                      <button class="btn button-primary" type="submit" onclick="searchUni();"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>                
            </div>
          </section>
          <!-- Populate data form DB-->
          <ul class="row row-30 justify-content-sm-center offset-top-60 text-md-left" id="populateDivs">
            <!-- Populate
                Data
                form
                Database -->
                     
            </ul>


           
        </div>
                
      </section>
                
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="//code.jquery.com/jquery.min.js"></script>
    <script src="js/jquery.paginate.js"></script>
    <script src="js/universityPageController.js"></script>
    <script src="js/loginRegisterController.js"></script>  
    <script type="text/javascript">
        $(document).ready(function(){
            loginChecker();
            
        })
    </script>
  

  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>