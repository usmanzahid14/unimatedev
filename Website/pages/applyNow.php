<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Apply Now</title>  
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark">
        <div class="container">
          <h1>Apply Now</h1>
        </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="row row-50">
            <div class="col-lg-4 order-lg-2 text-lg-left"><img class="img-responsive d-inline-block" src="images/apply-01-370x370.jpg" width="370" height="370" alt=""></div>
            <div class="col-lg-8 order-lg-1 text-lg-left">
              <div class="inset-lg-right-30">
                <h2 class="font-weight-bold">How to Apply</h2>
                <hr class="divider bg-madison divider-lg-0">
                <div class="offset-top-30 offset-sm-top-60">
                  <p>You are invited to apply for any open positions listed for which you believe you are qualified. Internal candidates (applicants who are currently employed by the Jonathan Carroll University) will be given first consideration for all positions. Employees of the Jonathan Carroll University (JCU employees) are considered external candidates.</p>
                </div>
                <p>If the Jonathan Carroll University is not your current employer, we invite you to view our available positions in the Job Opportunities section and follow the instructions provided to create your profile and to submit an online application.</p>
                <p>Please be prepared to enter your e-mail address and password. You will be able to use that same information to access your profile and online application in the future.</p>
                <p>Applicants must submit a job application online. However, if you have difficulty submitting your resume online, please contact the Nurse Recruitment office for nursing positions at (800) 702-1734. All other applicants, please contact (800) 702-2355.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Fill in the Form-->
      <section class="section bg-madison">
        <div class="container">
          <div class="row justify-content-sm-center justify-content-md-end offset-top-0">
            <div class="col-sm-10 col-md-8 col-lg-6 section-image-aside section-image-aside-left text-md-left">
              <div class="section-image-aside-img d-none d-md-block section-image-aside-2-img" style="background-image: url(images/apply-02-948x987.jpg)"></div>
              <div class="section-image-aside-body section-xl inset-md-left-70 inset-xl-left-100">
                <h2 class="font-weight-bold text-white">Fill in the Form</h2>
                <hr class="divider divider-md-0 bg-default">
                <div class="offset-top-60">
                  <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="https://livedemo00.template-help.com/wt_59029_v3/bat/rd-mailform.php">
                    <div class="offset-top-17">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside text-white" for="fill-form-name">First name</label>
                        <input class="form-input" id="fill-form-name" type="text" name="name" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="offset-top-17">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside text-white" for="fill-form-last-name">Last name</label>
                        <input class="form-input" id="fill-form-last-name" type="text" name="last-name" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="offset-top-17">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside text-white" for="fill-form-phone">Phone</label>
                        <input class="form-input" id="fill-form-phone" type="text" name="phone" data-constraints="@Required @IsNumeric">
                      </div>
                    </div>
                    <div class="offset-top-17">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside text-white" for="fill-form-email">E-mail</label>
                        <input class="form-input" id="fill-form-email" type="email" name="email" data-constraints="@Required @Email">
                      </div>
                    </div>
                    <div class="offset-top-17">
                      <div class="form-wrap">
                        <label class="form-label form-label-outside" for="fill-form-department" style="color: #fff">Department</label>
                        <select class="form-input select-filter" id="fill-form-department" data-placeholder="Law" data-minimum-results-for-search="Infinity">
                          <option>Law</option>
                          <option value="2">Medium</option>
                          <option value="3">High</option>
                        </select>
                      </div>
                    </div>
                    <div class="offset-top-35">
                      <div class="form-wrap">
                        <!-- RD Filepicker-->
                        <div class="rd-file-picker" id="fill-form-file">
                          <input name="file" type="file" multiple>
                          <div class="rd-file-picker-meta"></div>
                          <div class="rd-file-picker-btn btn text-left"><span>Select a File</span></div>
                        </div>
                      </div>
                    </div>
                    <div class="text-center text-xl-left offset-top-30 context-dark">
                      <button class="btn button-primary" type="submit">Apply Now</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>