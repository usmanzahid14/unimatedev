<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Events</title> 
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Event Calender</h1>
        </div>
      </section>
      <section class="section section-xl bg-default" style="margin: -67px 0 -56px -23px;">
        <div class="container container-wide">
          <h3 class="font-weight-bold text-white view-animate fadeInUpSmall delay-04">Find the latest events</h3>
        </div>
      </section>
      <section class="section section-xl bg-default">
        <div class="container container-wide">
          <div class="row row-70 justify-content-sm-center">
            <div class="col-xl-9">
              <div class="offset-top-60"></div>
                  <div class="row row-50 justify-content-sm-center justify-content-xxl-start" id="eventPopulate">
                    <!-- Populate
                          Data
                            From
                              DB-->
                  </div>
            </div>
            <div class="col-xl-3 text-md-left">
              <div class="row row-50">
                <div class="col-12 d-none d-xl-block"><a class="d-block" href="#"><img class="img-responsive d-inline-block" src="images/Ad1.jpg" width="416" height="500" alt="" style="padding: 66px 0px 0px;"></a></div>
              </div>
            </div>
             
            <div class="col-xl-5 text-md-left">
              <div class="row row-50">
                <div class="col-12 d-none d-xl-block"><a class="d-block" href="#"><img class="img-responsive d-inline-block" src="images/gif1.gif" width="416" height="500" alt=""></a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Corporate footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/eventsPageController.js"></script>
    <script src="js/loginRegisterController.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            loginChecker();
        })
    </script>
  </body>
  <!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script>
</html>