<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
  <title>Dashboard</title>
  <!-- Include Header Here-->
  <?php include '../Includes/header.php';?>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <!--End--> 

   <style>
      .notification {
         background-color: #555;
         color: white;
         text-decoration: none;
         padding: 15px 26px;
         position: relative;
         display: inline-block;
         border-radius: 2px;
      }
      .notification .badge {
         position: absolute;
         top: -10px;
         right: -10px;
         padding: 5px 10px;
         border-radius: 50%;
         background-color: #e0352f;
         color: white;
      }
      .btn-file {
        position: relative;
        overflow: hidden;
      }
      .btn-file input[type=file] {
          position: absolute;
          top: 0;
          right: 0;
          min-width: 100%;
          min-height: 100%;
          font-size: 100px;
          text-align: right;
          filter: alpha(opacity=0);
          opacity: 0;
          outline: none;
          background: white;
          cursor: inherit;
          display: block;
      }
      .portfolio{
        padding:6%;
        text-align:center;
      }
      .heading{
        background: #fff;
        padding: 1%;
        text-align: left;
        box-shadow: 0px 0px 4px 0px #545b62;
      }
      .heading img{
        width: 10%;
      }
      .bio-info{
        padding: 5%;
        background:#fff;
        box-shadow: 0px 0px 4px 0px #b0b3b7;
      }
      .bio-image{
        text-align:center;
      }
      .bio-image img{
        border-radius:50%;
        height: 300px;
        width: 250px;
      }
      .bio-content{
        text-align:left;
      }

      .card1 {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
      max-width: 300px;
      margin: auto;
      text-align: center;
      font-family: arial;
      }

      .title {
        color: grey;
        font-size: 18px;
      }

      button {
        border: none;
        outline: 0;
        display: inline-block;
        padding: 8px;
        color: white;
        background-color: #000;
        text-align: center;
        cursor: pointer;
        width: 100%;
        font-size: 18px;
      }

      a {
        text-decoration: none;
        font-size: 22px;
        color: black;
      }

      button:hover, a:hover {
        opacity: 0.7;
      }
  </style>

  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <?php include '../Includes/pagesNavbar.php';?>
      
      <section class="section section-xl bg-default" style="padding: 47px 0px 90px 90px!important;">
        <div class="container portfolio" style="padding: 0 0 0 0;">
          <div class="bio-info">
            <div class="row" >
              <div class="col-md-4" id="userImage">
                <!-- Image from db-->
              </div>
              <div class="col-md-8" id="userDetail">
                <!-- Details from db-->
              </div>
            </div>  
          </div>
        </div>
        <div class="container">
          <div class="row justify-content-sm-center">
            <div class="col-sm-12 col-xl-12" style="padding: 53px 28px 0px 0px;">
              <h2 class="font-weight-bold">Welcome User</h2>
              <hr class="divider bg-madison">
              <!--Bootstrap tabs-->
              <div class="tabs-custom tabs-horizontal tabs-line" id="tabs-1">
                <!--Nav tabs-->
                <ul class="nav nav-tabs">
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-1" data-toggle="tab">Subscribed Universities</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link notification" href="#tabs-profile-2" data-toggle="tab">Notification<span class="badge" id="unseenNotifications"></span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-3" data-toggle="tab">Maters</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-4" data-toggle="tab">Matings</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-5" data-toggle="tab">Pending Requests</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-6" data-toggle="tab">Blocked</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-7" data-toggle="tab">Change Password</a></li>
                  </ul>
                <!--Tab panes-->
                <div class="tab-content">
                  <!--Subscribed Universities-->
                  <div class="tab-pane fade" id="tabs-profile-1">
                    <h4>Your Subscribed Universities</h4>
                    <div id="subscriptions" class="row">
                      <!-- Subscribed 
                            universities 
                                from 
                                    DB-->
                    </div>
                  </div>
                  <!--Notification-->
                  <div class="tab-pane fade" id="tabs-profile-2">
                    <div  class="container" id="notificationDiv">
                      <div class="alert alert-info" role="alert">
                        <p>notify</p>
                      </div> 
                    </div>                      
                  </div>
                  <!--Matters-->
                  <div class="tab-pane fade" id="tabs-profile-3">
                    <div class="container" id="mattersDiv">
                      <!-- maters list
                            from
                              database-->
                    </div><br>
                  </div>
                  <!--Matings-->
                  <div class="tab-pane fade" id="tabs-profile-4">
                    <div class="container" id="matingsListDiv">
                      <!-- matings
                              list
                                from
                                  db-->
                    </div><br>
                  </div>
                  <!--Pending-->
                  <div class="tab-pane fade" id="tabs-profile-5">
                    <h3 class="font-weight-bold text-center">Pending Requests</h3>
                    <hr class="divider bg-madison">
                    <div class="offset-sm-top-45 text-center">
                      <!--Bootstrap tabs-->
                      <div class="tabs-custom tabs-horizontal tabs-line" id="tabs-1">
                        <!--Nav tabs-->
                        <ul class="nav nav-tabs">
                          <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-login-1" data-toggle="tab">Mate Requests</a></li>
                          <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-login-2" data-toggle="tab">Sent Requests</a></li>
                        </ul>
                        <!--Tab panes-->
                        <div class="tab-content">
                          <div class="tab-pane fade" id="tabs-login-1">
                            <div class="container" id="userMateRequestDiv">
                               <!-- Pending
                                      Mate
                                      Requests 
                                        from 
                                          DB-->
                            </div>
                          </div>
                          <div class="tab-pane fade" id="tabs-login-2">
                            <div class="container" id="userSendRequestDiv">
                              <!-- Pending
                                      Send 
                                      Requests 
                                        from 
                                          DB-->                             
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--Blocked-->
                  <div class="tab-pane fade" id="tabs-profile-6">
                    <div class="container" id="blockedListDiv">
                      <!-- Blocked 
                              list
                                from 
                                  db-->
                    </div><br>
                  </div>
                  <!--Change Profile-->
                  <div class="tab-pane fade" id="tabs-profile-7">
                    <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" >
                      <div class="form-wrap">
                        <input type="hidden" id="cKey">
                        <label class="form-label form-label-outside" for="oldPassword">Old Password:</label>
                        <input class="form-input bg-default" id="oldPassword" type="text" name="oldPassword" data-constraints="@Required">
                      </div>
                      <div class="form-wrap offset-top-15">
                        <label class="form-label form-label-outside" for="newPassword">New Password:</label>
                        <input class="form-input bg-default" placeholder="Enter New Password" id="newPassword" type="text" name="newPassword" data-constraints="@Required">
                      </div>
                      <div class="offset-top-20">
                        <button class="btn button-primary" type="submit" onclick="changePassword();">Change Password</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

     <!-- Include Footer-->
     <?php include '../Includes/footer.php';?>
     <!--End-->
    </div>

    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/dashboardController.js"></script>
    <script src="js/loginRegisterController.js"></script>  
    <script type="text/javascript">
        $(document).ready(function(){
            loginChecker(); 
        });
    </script>

        <div class="modal fade" id="EditProfile" tabindex="-1" role="">
          <div class="modal-dialog EditProfile" role="document">
            <div class="modal-content">
              <div class="card card-signup card-plain">
                <div class="modal-header card-header-info">
                  <h3 class="card-title">Edit Profile</h3>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true">
                      <i class="material-icons">X</i>
                    </button>
                </div>
                <div class="modal-body">
                  <div class="card ">
                    <div class="card-body ">
                        <div class="container">
                          <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post">
                            <div class="form-wrap">
                              <input class="form-input bg-default" id="eKey" type="hidden" name="username" data-constraints="@Required">
                              <label class="form-label form-label-outside" for="username">Username:</label>
                              <input class="form-input bg-default form-control" id="eusername" type="text" name="username" data-constraints="@Required" style="width: 45%">
                            </div>
                            <div class="form-wrap">
                              <label class="form-label form-label-outside" for="email">E-mail:</label>
                              <input class="form-input bg-default" id="email" type="text" name="email" data-constraints="@Required">
                            </div>
                            <div class="form-wrap offset-top-15">
                              <label class="form-label form-label-outside" for="contact">Contact:</label>
                              <input class="form-input bg-default" id="contact" type="text" name="contact" data-constraints="@Required">
                            </div>
                            <div class="form-wrap offset-top-15">
                              <label class="form-label form-label-outside" for="bio">Tell About Yourself:</label>
                              <textarea class="form-input form-validation-inside" id="bio" name="bio"></textarea>
                            </div>
                            <div class="offset-top-20">
                              <button class="btn button-primary" class="close" data-dismiss="modal" type="submit" onclick="saveChanges();">Save Changes</button>
                            </div>
                          </form>
                        </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
  </body>
</html>         