<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Search result</title>
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark">
        <div class="container">
          <h1>Search Here</h1>
        </div>
      </section>
      <section class="section section-lg bg-default">
        <div class="container">
          <div class="row justify-content-sm-center">
            <div class="col-md-8">
              <!-- RD Search Form-->
              <form class="form-search rd-search" action="https://livedemo00.template-help.com/wt_59029_v3/search-results.html" method="GET">
                <div class="form-wrap">
                  <input class="form-search-input form-input" type="text" name="s" autocomplete="on">
                </div>
                <button class="form-search-submit" type="submit"><span class="icon fa fa-search"></span></button>
              </form>
              <div class="rd-search-results"></div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footers-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->

<!-- Mirrored from livedemo00.template-help.com/wt_59029_v3/search-results.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Jun 2019 08:02:53 GMT -->
</html>