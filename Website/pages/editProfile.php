<?php 
if (isset($_GET['userID'])) {
  $userID = $_GET['userID'];
?>

<!DOCTYPE html> 
<html class="wide wow-animation scrollTo" lang="en">
<title>Edit Profile</title>
    <!-- Include Header Here-->
    <?php include '../Includes/header.php';?>
    <!--End-->
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <!-- Include Header Here-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!--End-->
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark">
        <div class="container">
          <h1>Edit Profile</h1>
        </div>
      </section>
      <!--Section Login Register-->
      <section class="section section-xl bg-default">
        <div class="container">
          <div class="row justify-content-sm-center section-34">
            <div class="col-sm-8 col-md-6 col-lg-5">
              <h2 class="font-weight-bold">Make Changes Here</h2>
              <hr class="divider bg-madison">
              <div class="offset-sm-top-45 text-center">
                <!--Bootstrap tabs-->
                <div class="tabs-custom tabs-horizontal tabs-line" id="tabs-1">
                  <!--Nav tabs-->
                  <ul class="nav nav-tabs">
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-1" data-toggle="tab">Edit Profile</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-profile-2" data-toggle="tab">Change Password</a></li>
                  </ul>
                  <!--Tab panes-->
                  <div class="tab-content">
                    <div class="tab-pane fade" id="tabs-profile-1">
                      <!-- RD Mailform-->
                      <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post">
                        <div class="form-wrap">
                          <input class="form-input bg-default" id="eKey" type="text" name="username" data-constraints="@Required">
                          <label class="form-label form-label-outside" for="username">Username:</label>
                          <input class="form-input bg-default" id="eusername" type="text" name="username" data-constraints="@Required">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside" for="email">E-mail:</label>
                          <input class="form-input bg-default" id="email" type="text" name="email" data-constraints="@Required">
                        </div>
                        <div class="form-wrap offset-top-15">
                          <label class="form-label form-label-outside" for="contact">Contact:</label>
                          <input class="form-input bg-default" id="contact" type="text" name="contact" data-constraints="@Required">
                        </div>
                        <div class="form-wrap offset-top-15">
                          <label class="form-label form-label-outside" for="bio">Tell About Yourself:</label>
                          <textarea class="form-input form-validation-inside" id="bio" name="bio" data-constraints="@Required"></textarea>
                        </div>
                        <div class="form-wrap offset-top-15">
                          <label class="form-label form-label-outside" for="bio">Upload Profile Picture:</label><br>
                          <form action="upload.php" method="post" enctype="multipart/form-data">
                            <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">      
                          </form>
                        </div>
                        <div class="offset-top-20">
                          <button class="btn button-primary" type="submit" onclick="uploadImage_editUser();">Save Changes</button>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane fade" id="tabs-profile-2">
                      <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" >
                        <div class="form-wrap">
                          <input class="form-input bg-default" id="cKey" type="text" name="username" data-constraints="@Required">
                          <label class="form-label form-label-outside" for="oldPassword">Old Password:</label>
                          <input class="form-input bg-default" id="oldPassword" type="text" name="oldPassword" data-constraints="@Required">
                        </div>
                        <div class="form-wrap offset-top-15">
                          <label class="form-label form-label-outside" for="newPassword">New Password:</label>
                          <input class="form-input bg-default" placeholder="Enter New Password" id="newPassword" type="text" name="newPassword" data-constraints="@Required">
                        </div>
                        <div class="offset-top-20">
                          <button class="btn button-primary" type="submit" onclick="changePassword();">Change Password</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/dashboardController.js"></script>
    <script src="js/loginRegisterController.js"></script>  
    <script type="text/javascript">
        $(document).ready(function(){
            loginChecker();
            var userID='<?php echo $userID?>';
            getUserEditDetails(userID);
        })
    </script>
  </body>
  <!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>
<?php 
}
else{
    echo "No User details Found. Login again";
}
?>