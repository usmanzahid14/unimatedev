<!DOCTYPE html>
 

<html>
   
<head>
<title>DashBoard Test</title>
 <?php include '../Includes/header.php';?>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
 <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
 
 <!--Yellow larki wala card-->
 <style>
    body{
        background: linear-gradient(90deg, #e8e8e8 50%, #3d009e 50%);
      }
      .portfolio{
        padding:6%;
        text-align:center;
      }
      .heading{
        background: #fff;
        padding: 1%;
        text-align: left;
        box-shadow: 0px 0px 4px 0px #545b62;
      }
      .heading img{
        width: 10%;
      }
      .bio-info{
        padding: 5%;
        background:#fff;
        box-shadow: 0px 0px 4px 0px #b0b3b7;
      }
      .bio-image{
        text-align:center;
      }
      .bio-image img{
        border-radius:50%;
        height: 300px;
        width: 250px;
      }
      .bio-content{
        text-align:left;
      }
</style>

<!--w3 School card-->
<style>
    .card1 {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
      max-width: 300px;
      margin: auto;
      text-align: center;
      font-family: arial;
    }

    .title {
      color: grey;
      font-size: 18px;
    }

    button {
      border: none;
      outline: 0;
      display: inline-block;
      padding: 8px;
      color: white;
      background-color: #000;
      text-align: center;
      cursor: pointer;
      width: 100%;
      font-size: 18px;
    }

    a {
      text-decoration: none;
      font-size: 22px;
      color: black;
    }

    button:hover, a:hover {
      opacity: 0.7;
    }
</style>

<!--Preview image before upload-->
<style>
  #wrapper
  {
   text-align:center;
   margin:0 auto;
   padding:0px;
   width:995px;
  }
  #output_image
  {
   max-width:300px;
  }
</style>
 
<body>

 <section class="section section-xl bg-default">
    <div class="container">
      <div class="row justify-content-sm-center section-34">
        <div class="col-sm-12 col-md-12 ">


          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-lg-8 col-md-6">
                        <h2 class="card-text"><strong>Name: </strong> Web Developer</h2>
                        <div class="hr bg-gray-light offset-top-10"></div> 
                        <p class="card-text"><span class="icon icon-xs mdi mdi-email">  some@gamil.com</span></p>
                        <h4 class="card-text"><strong>Bio: </strong> I am Bio.</h4>
                        <p>
                          <span class="btn button-primary">Action</span> 
                          <span class="btn button-primary">View Profile</span>
                        </p>
                      </div>
                      <div class="col-12 col-lg-4 col-md-6 text-center">
                        <img class="btn-md" src="images/123.jpg" alt="" style="border-radius:50%; height: 300px; width: 250px;">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!--Yellow larki-->
          <div class="container portfolio">
            <div class="bio-info">
              <div class="row">
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="bio-image">
                        <img src="images/123.jpg" alt="image" />
                      </div>      
                    </div>
                  </div>  
                </div>
                <div class="col-md-8">
                  <div class="bio-content">
                    <h1 class="card-text"><strong>Name: </strong> Web Developer</h1>
                    <div class="hr bg-gray-light offset-top-10"></div>                    
                    <span class="icon icon-xs mdi mdi-phone">  0000-0000000</span><br><br>
                    <span class="icon icon-xs mdi mdi-email">  some@gamil.com</span>
                    <h4 class="card-text"><strong>Bio: </strong> I am Bio.</h4><br>
                    <div class="profile-userbuttons"> 
                      <button type="button" class="btn button-primary btn-md">Edit Profile</button> 
                    </div> 
                  </div>
                </div>
              </div>  
            </div>
          </div>


          <!--w3 School card-->
          <div class="row" id="getUsers" >
            <div class="card1 col-md-4">
              <img src="images/123.jpg" alt="John" style="width:100%; height: 350px; width: 300;">
              <h1>Name</h1>
              <p class="title"><strong>Bio:</strong> i am Bio</p>
              <p><button style="background-color: #3aafa9">Mate</button></p>
            </div>
          </div>
            
          <a href="#" id="chatBox" class="ui-to-top icon icon-circle mdi mdi-forum active" style="background-color: #3aafa9"></a>

          <div id="wrapper">
           <input type="file" accept="image/*" onchange="preview_image(event)">
           <img id="output_image"/>
           
          </div>


        </div>
      </div>
    </div>
  </div>
</section>
   <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/dashboardController.js"></script>
    <script type='text/javascript'>
      function preview_image(event) 
      {
       var reader = new FileReader();
       reader.onload = function()
       {
        var output = document.getElementById('output_image');
        output.src = reader.result;
       }
       reader.readAsDataURL(event.target.files[0]);
      }
    </script>

    
</body>

</html>
