  <?php 
if (isset($_GET['blogID'])) {
  $blogKey = $_GET['blogID'];
?>


<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
<title>Blog Page</title>  
<?php include '../Includes/header.php';?>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader"> 
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div> 
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <?php include '../Includes/pagesNavbar.php';?>
      <!-- Classic Breadcrumbs-->
      <section class="section breadcrumb-classic context-dark" style="height: 10px">
        <div class="container">
          <h1 style="margin: -43px 0px 0px 0px;">Blog Details</h1>
        </div>
      </section>
      <section class="section section-xl bg-default" id="blogid">
        <!--Data
              From 
                Database--> 
      </section>
      <section class="section section-xl">
        <div class="container">
          <div class="row row-85 justify-content-sm-center">
            <div class="col-md-8 col-lg-8 text-lg-left">
              <div class="offset-top-50">
                <div class="offset-top-60">
                  <h6 class="font-weight-bold">Comments</h6>
                  <div class="text-subline"></div>
                  <div class="offset-top-30">
                    <div id="displayComments">
                      <!-- Comments
                                  form 
                                    DB-->
                    </div>                        
                    <div class="offset-top-60">
                      <h6 class="font-weight-bold">Send a Comment</h6>
                      <div class="text-subline"></div>
                      <div class="offset-top-20">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post">
                          <div class="row">
                            <input class="form-input bg-default" id="bKey" type="hidden" value="<?php echo $blogKey?>">
                            <div class="col-xl-12 offset-top-12">
                              <div class="form-wrap">
                                <label class="form-label form-label-outside" for="name">Name:</label>
                                <input class="form-input bg-default" id="name" type="text" name="name">
                              </div>
                            </div>
                            <div class="col-xl-12 offset-top-12">
                              <div class="form-wrap">
                                <label class="form-label form-label-outside" for="comment-form-message">Comment:</label>
                                <textarea class="form-input form-validation-inside" id="comment" name="message" data-constraints="@Required"></textarea>
                              </div>
                            </div>
                          </div>
                          <div class="offset-top-20 text-center text-md-left">
                            <button class="btn button-primary" type="submit" onclick="saveComment();">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 text-left col-sm-8">
              <aside class="inset-lg-left-30">
                <div class="offset-top-60 text-center"><a class="d-block" href="#"><img class="img-responsive d-inline-block" src="images/blog/banner-340x500.jpg" width="340" height="500" alt=""></a></div>
              </aside>
            </div>
          </div>
        </div>
      </section>
      <!-- Corporate footer-->
      <?php include '../Includes/footer.php';?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/loginRegisterController.js"></script>
    <script src="js/blogsPageController.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
         loginChecker();
         var id='<?php echo $blogKey?>';
         singleBlogDisplay(id);
         fetchComments(id);
      })
    </script>
  </body><!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>


<?php 
}
else{
    echo "No Blog Found";
}
?>